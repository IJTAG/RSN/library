/*
* Copyright Testonica Lab (C) 2018
* Author: Anton Tsertov, Testonica Lab
*
* Uses modules from files: E30.icl
*
*/



Module FCX_SIB {
    Attribute lic = 'h eb2de30a;
    ScanInPort SI;
    CaptureEnPort CE;
    ShiftEnPort SE;
    UpdateEnPort UE;
    SelectPort SEL;
    ResetPort RST;
    TCKPort TCK;
    ScanOutPort SO {
        Source F[0];
    }
	
	DataInPort fromSIBF;
	DataInPort fromSIBC;
	
	DataInPort fromF;
	DataInPort fromC;
	DataOutPort toF {
		Source toF_logSig;
	}
	DataOutPort toC {
		Source toC_logSig;
	}
	
	
    ScanInterface client {
        Port SI; Port CE; Port SE; Port UE;
        Port RST; Port SEL; Port TCK; Port SO;
    }
    
    LogicSignal toSel_SR_SEL { 
       SR[0] & SEL;
    }
	
	LogicSignal toF_logSig { 
       fromF | (X[0] & fromSIBF) ;
    }
	
	LogicSignal toC_logSig { 
       fromC | (~X[0] & fromSIBC);
    }
	
    ScanInPort fromSO;
    ToCaptureEnPort toCE;
	
    ToShiftEnPort toSE;
    ToUpdateEnPort toUE;
    ToSelectPort toSEL {
       Source toSel_SR_SEL; 
    }
    ToResetPort toRST;
    ToTCKPort toTCK;
    ScanOutPort toSI {
        Source SI;
    }
    ScanInterface host {
        Port fromSO; Port toCE; Port toSE; Port toUE;
        Port toSEL; Port toRST; Port toTCK; Port toSI;
    }
    ScanRegister SR[0:0] {
        ScanInSource SIBmux;
        CaptureSource SR[0];
        ResetValue 1'b0;
    }
	ScanRegister X[0:0] {
        ScanInSource SR[0];
        CaptureSource X;
        ResetValue 1'b0;
    }
	ScanRegister C[0:0] {
        ScanInSource X[0];
        CaptureSource sync_C.DO;
        ResetValue 1'b0;
    }
	ScanRegister F[0:0] {
        ScanInSource C[0];
        CaptureSource sync_F.DO;
        ResetValue 1'b0;
    }
    ScanMux SIBmux SelectedBy SR {
        1'b0 : SI;
        1'b1 : fromSO;
    }
	
	Instance sync_C Of Sync { InputPort DI = fromSIBC;}
    Instance sync_F Of Sync { InputPort DI = fromSIBF;}

	
}
/*
* Dummy SYNC module imitates synchronizer circuit for the signal-crossing
* clock domains.
* NB! rewrite for simulation purposes
*/
Module Sync {
    Attribute lic = 'h fde3bc37;
	DataInPort DI[0];
	DataOutPort DO[0] {
		Source DR[0];
	}
	
	DataRegister DR[0:0] {
		WriteDataSource DI[0];
	}
	
}
