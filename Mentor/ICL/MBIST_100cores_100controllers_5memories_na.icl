// Copyright 2018: Mentor, a Siemens Business
// Benchmark for a chip containing 100 cores
// Each core contains 100 memory BIST controllers
// Each memory BIST controller tests 5 memories



Module memory_1 {
   DataInPort AR[3:0];
   DataInPort AW[3:0];
   ClockPort CLKR;
   ClockPort CLKW;
}


Module mbist_interface {
   ClockPort BIST_CLK;
   DataInPort BIST_EN;
   DataInPort BIST_ASYNC_RESETN;
   ScanInPort BIST_SI;
   ScanOutPort BIST_SO {
      Source reg3[0];
   }
   ShiftEnPort BIST_SHIFT_COLLAR;
   DataInPort BIST_CONTROL;
   DataOutPort AR[3:0];
   DataOutPort AW[3:0];

   ScanRegister reg1[0:0] {
      ScanInSource BIST_SI;
   }
   ScanRegister reg2[0:3] {
      ScanInSource reg1[0];
   }
   ScanRegister reg3[7:0] {
      ScanInSource reg2[3];
   }
}

Module sib_1 {
   ResetPort ijtag_reset {
      ActivePolarity 0;
   }
   SelectPort ijtag_sel;
   ScanInPort ijtag_si;
   CaptureEnPort ijtag_ce;
   ShiftEnPort ijtag_se;
   UpdateEnPort ijtag_ue;
   TCKPort ijtag_tck;
   ScanOutPort ijtag_so {
      Source sib;
   }
   ToSelectPort ijtag_to_sel;
   ScanInPort ijtag_from_so;
   ScanInterface client {
      Port ijtag_si;
      Port ijtag_so;
      Port ijtag_sel;
   }
   ScanInterface host {
      Port ijtag_from_so;
      Port ijtag_to_sel;
   }
   ScanRegister sib {
      ScanInSource scan_in_mux;
      CaptureSource 1'b0;
      ResetValue 1'b0;
   }
   ScanMux scan_in_mux SelectedBy sib {
      1'b0 : ijtag_si;
      1'b1 : ijtag_from_so;
   }
}
Module sib_2 {
   ResetPort ijtag_reset {
      ActivePolarity 0;
   }
   SelectPort ijtag_sel;
   ScanInPort ijtag_si;
   CaptureEnPort ijtag_ce;
   ShiftEnPort ijtag_se;
   UpdateEnPort ijtag_ue;
   TCKPort ijtag_tck;
   
   ToResetPort to_ijtag_reset {
      ActivePolarity 0;
      Source ijtag_reset;
   }
   ScanOutPort to_ijtag_si {
      Source ijtag_si;
   }
   ToCaptureEnPort to_ijtag_ce {
      Source ijtag_ce;
   }
   ToShiftEnPort to_ijtag_se {
      Source ijtag_se;
   }
   ToUpdateEnPort to_ijtag_ue {
      Source ijtag_ue;
   }
   ToTCKPort to_ijtag_tck;
   
   ScanOutPort ijtag_so {
      Source sib;
   }
   ToSelectPort ijtag_to_sel;
   ScanInPort ijtag_from_so;

   ScanInterface client {
      Port ijtag_si;
      Port ijtag_so;
      Port ijtag_sel;
   }
   ScanInterface host {
      Port ijtag_from_so;
      Port ijtag_to_sel;
      Port to_ijtag_si;
   }
   ScanRegister sib {
      ScanInSource scan_in_mux;
      CaptureSource 1'b0;
      ResetValue 1'b0;
   }
   ScanMux scan_in_mux SelectedBy sib {
      1'b0 : ijtag_si;
      1'b1 : ijtag_from_so;
   }
}
Module  mbist_bap {
   ResetPort reset {
      ActivePolarity 0;
   }
   SelectPort ijtag_select;
   ScanInPort si;
   CaptureEnPort capture_en;
   ShiftEnPort shift_en;
   ToShiftEnPort shift_en_R;
   UpdateEnPort update_en;
   TCKPort tck;

   DataOutPort setting1 {
      Source BIST_setting1_tdr;
      RefEnum OnOff;
   }
   DataOutPort setting2 {
      Source BIST_setting2_tdr;
      RefEnum OnOff;
   }
   DataOutPort setting3 {
      Source BIST_setting3_tdr;
      RefEnum OnOff;
   }

   ScanOutPort so {
      Source sib_0;
   }
   
   ScanOutPort toBist_99 {
      Source BIST_setting3_tdr;    }
   ScanOutPort toBist_0 {
      Source sib_1;
   }
   ScanOutPort toBist_1 {
      Source sib_2;
   }
   ScanOutPort toBist_2 {
      Source sib_3;
   }
   ScanOutPort toBist_3 {
      Source sib_4;
   }
   ScanOutPort toBist_4 {
      Source sib_5;
   }
   ScanOutPort toBist_5 {
      Source sib_6;
   }
   ScanOutPort toBist_6 {
      Source sib_7;
   }
   ScanOutPort toBist_7 {
      Source sib_8;
   }
   ScanOutPort toBist_8 {
      Source sib_9;
   }
   ScanOutPort toBist_9 {
      Source sib_10;
   }
   ScanOutPort toBist_10 {
      Source sib_11;
   }
   ScanOutPort toBist_11 {
      Source sib_12;
   }
   ScanOutPort toBist_12 {
      Source sib_13;
   }
   ScanOutPort toBist_13 {
      Source sib_14;
   }
   ScanOutPort toBist_14 {
      Source sib_15;
   }
   ScanOutPort toBist_15 {
      Source sib_16;
   }
   ScanOutPort toBist_16 {
      Source sib_17;
   }
   ScanOutPort toBist_17 {
      Source sib_18;
   }
   ScanOutPort toBist_18 {
      Source sib_19;
   }
   ScanOutPort toBist_19 {
      Source sib_20;
   }
   ScanOutPort toBist_20 {
      Source sib_21;
   }
   ScanOutPort toBist_21 {
      Source sib_22;
   }
   ScanOutPort toBist_22 {
      Source sib_23;
   }
   ScanOutPort toBist_23 {
      Source sib_24;
   }
   ScanOutPort toBist_24 {
      Source sib_25;
   }
   ScanOutPort toBist_25 {
      Source sib_26;
   }
   ScanOutPort toBist_26 {
      Source sib_27;
   }
   ScanOutPort toBist_27 {
      Source sib_28;
   }
   ScanOutPort toBist_28 {
      Source sib_29;
   }
   ScanOutPort toBist_29 {
      Source sib_30;
   }
   ScanOutPort toBist_30 {
      Source sib_31;
   }
   ScanOutPort toBist_31 {
      Source sib_32;
   }
   ScanOutPort toBist_32 {
      Source sib_33;
   }
   ScanOutPort toBist_33 {
      Source sib_34;
   }
   ScanOutPort toBist_34 {
      Source sib_35;
   }
   ScanOutPort toBist_35 {
      Source sib_36;
   }
   ScanOutPort toBist_36 {
      Source sib_37;
   }
   ScanOutPort toBist_37 {
      Source sib_38;
   }
   ScanOutPort toBist_38 {
      Source sib_39;
   }
   ScanOutPort toBist_39 {
      Source sib_40;
   }
   ScanOutPort toBist_40 {
      Source sib_41;
   }
   ScanOutPort toBist_41 {
      Source sib_42;
   }
   ScanOutPort toBist_42 {
      Source sib_43;
   }
   ScanOutPort toBist_43 {
      Source sib_44;
   }
   ScanOutPort toBist_44 {
      Source sib_45;
   }
   ScanOutPort toBist_45 {
      Source sib_46;
   }
   ScanOutPort toBist_46 {
      Source sib_47;
   }
   ScanOutPort toBist_47 {
      Source sib_48;
   }
   ScanOutPort toBist_48 {
      Source sib_49;
   }
   ScanOutPort toBist_49 {
      Source sib_50;
   }
   ScanOutPort toBist_50 {
      Source sib_51;
   }
   ScanOutPort toBist_51 {
      Source sib_52;
   }
   ScanOutPort toBist_52 {
      Source sib_53;
   }
   ScanOutPort toBist_53 {
      Source sib_54;
   }
   ScanOutPort toBist_54 {
      Source sib_55;
   }
   ScanOutPort toBist_55 {
      Source sib_56;
   }
   ScanOutPort toBist_56 {
      Source sib_57;
   }
   ScanOutPort toBist_57 {
      Source sib_58;
   }
   ScanOutPort toBist_58 {
      Source sib_59;
   }
   ScanOutPort toBist_59 {
      Source sib_60;
   }
   ScanOutPort toBist_60 {
      Source sib_61;
   }
   ScanOutPort toBist_61 {
      Source sib_62;
   }
   ScanOutPort toBist_62 {
      Source sib_63;
   }
   ScanOutPort toBist_63 {
      Source sib_64;
   }
   ScanOutPort toBist_64 {
      Source sib_65;
   }
   ScanOutPort toBist_65 {
      Source sib_66;
   }
   ScanOutPort toBist_66 {
      Source sib_67;
   }
   ScanOutPort toBist_67 {
      Source sib_68;
   }
   ScanOutPort toBist_68 {
      Source sib_69;
   }
   ScanOutPort toBist_69 {
      Source sib_70;
   }
   ScanOutPort toBist_70 {
      Source sib_71;
   }
   ScanOutPort toBist_71 {
      Source sib_72;
   }
   ScanOutPort toBist_72 {
      Source sib_73;
   }
   ScanOutPort toBist_73 {
      Source sib_74;
   }
   ScanOutPort toBist_74 {
      Source sib_75;
   }
   ScanOutPort toBist_75 {
      Source sib_76;
   }
   ScanOutPort toBist_76 {
      Source sib_77;
   }
   ScanOutPort toBist_77 {
      Source sib_78;
   }
   ScanOutPort toBist_78 {
      Source sib_79;
   }
   ScanOutPort toBist_79 {
      Source sib_80;
   }
   ScanOutPort toBist_80 {
      Source sib_81;
   }
   ScanOutPort toBist_81 {
      Source sib_82;
   }
   ScanOutPort toBist_82 {
      Source sib_83;
   }
   ScanOutPort toBist_83 {
      Source sib_84;
   }
   ScanOutPort toBist_84 {
      Source sib_85;
   }
   ScanOutPort toBist_85 {
      Source sib_86;
   }
   ScanOutPort toBist_86 {
      Source sib_87;
   }
   ScanOutPort toBist_87 {
      Source sib_88;
   }
   ScanOutPort toBist_88 {
      Source sib_89;
   }
   ScanOutPort toBist_89 {
      Source sib_90;
   }
   ScanOutPort toBist_90 {
      Source sib_91;
   }
   ScanOutPort toBist_91 {
      Source sib_92;
   }
   ScanOutPort toBist_92 {
      Source sib_93;
   }
   ScanOutPort toBist_93 {
      Source sib_94;
   }
   ScanOutPort toBist_94 {
      Source sib_95;
   }
   ScanOutPort toBist_95 {
      Source sib_96;
   }
   ScanOutPort toBist_96 {
      Source sib_97;
   }
   ScanOutPort toBist_97 {
      Source sib_98;
   }
   ScanOutPort toBist_98 {
      Source sib_99;
   }
   ScanInPort fromBist_0;
   DataOutPort bistEn_0 {
      Source sib_0;
    }
   ScanInPort fromBist_1;
   DataOutPort bistEn_1 {
      Source sib_1;
    }
   ScanInPort fromBist_2;
   DataOutPort bistEn_2 {
      Source sib_2;
    }
   ScanInPort fromBist_3;
   DataOutPort bistEn_3 {
      Source sib_3;
    }
   ScanInPort fromBist_4;
   DataOutPort bistEn_4 {
      Source sib_4;
    }
   ScanInPort fromBist_5;
   DataOutPort bistEn_5 {
      Source sib_5;
    }
   ScanInPort fromBist_6;
   DataOutPort bistEn_6 {
      Source sib_6;
    }
   ScanInPort fromBist_7;
   DataOutPort bistEn_7 {
      Source sib_7;
    }
   ScanInPort fromBist_8;
   DataOutPort bistEn_8 {
      Source sib_8;
    }
   ScanInPort fromBist_9;
   DataOutPort bistEn_9 {
      Source sib_9;
    }
   ScanInPort fromBist_10;
   DataOutPort bistEn_10 {
      Source sib_10;
    }
   ScanInPort fromBist_11;
   DataOutPort bistEn_11 {
      Source sib_11;
    }
   ScanInPort fromBist_12;
   DataOutPort bistEn_12 {
      Source sib_12;
    }
   ScanInPort fromBist_13;
   DataOutPort bistEn_13 {
      Source sib_13;
    }
   ScanInPort fromBist_14;
   DataOutPort bistEn_14 {
      Source sib_14;
    }
   ScanInPort fromBist_15;
   DataOutPort bistEn_15 {
      Source sib_15;
    }
   ScanInPort fromBist_16;
   DataOutPort bistEn_16 {
      Source sib_16;
    }
   ScanInPort fromBist_17;
   DataOutPort bistEn_17 {
      Source sib_17;
    }
   ScanInPort fromBist_18;
   DataOutPort bistEn_18 {
      Source sib_18;
    }
   ScanInPort fromBist_19;
   DataOutPort bistEn_19 {
      Source sib_19;
    }
   ScanInPort fromBist_20;
   DataOutPort bistEn_20 {
      Source sib_20;
    }
   ScanInPort fromBist_21;
   DataOutPort bistEn_21 {
      Source sib_21;
    }
   ScanInPort fromBist_22;
   DataOutPort bistEn_22 {
      Source sib_22;
    }
   ScanInPort fromBist_23;
   DataOutPort bistEn_23 {
      Source sib_23;
    }
   ScanInPort fromBist_24;
   DataOutPort bistEn_24 {
      Source sib_24;
    }
   ScanInPort fromBist_25;
   DataOutPort bistEn_25 {
      Source sib_25;
    }
   ScanInPort fromBist_26;
   DataOutPort bistEn_26 {
      Source sib_26;
    }
   ScanInPort fromBist_27;
   DataOutPort bistEn_27 {
      Source sib_27;
    }
   ScanInPort fromBist_28;
   DataOutPort bistEn_28 {
      Source sib_28;
    }
   ScanInPort fromBist_29;
   DataOutPort bistEn_29 {
      Source sib_29;
    }
   ScanInPort fromBist_30;
   DataOutPort bistEn_30 {
      Source sib_30;
    }
   ScanInPort fromBist_31;
   DataOutPort bistEn_31 {
      Source sib_31;
    }
   ScanInPort fromBist_32;
   DataOutPort bistEn_32 {
      Source sib_32;
    }
   ScanInPort fromBist_33;
   DataOutPort bistEn_33 {
      Source sib_33;
    }
   ScanInPort fromBist_34;
   DataOutPort bistEn_34 {
      Source sib_34;
    }
   ScanInPort fromBist_35;
   DataOutPort bistEn_35 {
      Source sib_35;
    }
   ScanInPort fromBist_36;
   DataOutPort bistEn_36 {
      Source sib_36;
    }
   ScanInPort fromBist_37;
   DataOutPort bistEn_37 {
      Source sib_37;
    }
   ScanInPort fromBist_38;
   DataOutPort bistEn_38 {
      Source sib_38;
    }
   ScanInPort fromBist_39;
   DataOutPort bistEn_39 {
      Source sib_39;
    }
   ScanInPort fromBist_40;
   DataOutPort bistEn_40 {
      Source sib_40;
    }
   ScanInPort fromBist_41;
   DataOutPort bistEn_41 {
      Source sib_41;
    }
   ScanInPort fromBist_42;
   DataOutPort bistEn_42 {
      Source sib_42;
    }
   ScanInPort fromBist_43;
   DataOutPort bistEn_43 {
      Source sib_43;
    }
   ScanInPort fromBist_44;
   DataOutPort bistEn_44 {
      Source sib_44;
    }
   ScanInPort fromBist_45;
   DataOutPort bistEn_45 {
      Source sib_45;
    }
   ScanInPort fromBist_46;
   DataOutPort bistEn_46 {
      Source sib_46;
    }
   ScanInPort fromBist_47;
   DataOutPort bistEn_47 {
      Source sib_47;
    }
   ScanInPort fromBist_48;
   DataOutPort bistEn_48 {
      Source sib_48;
    }
   ScanInPort fromBist_49;
   DataOutPort bistEn_49 {
      Source sib_49;
    }
   ScanInPort fromBist_50;
   DataOutPort bistEn_50 {
      Source sib_50;
    }
   ScanInPort fromBist_51;
   DataOutPort bistEn_51 {
      Source sib_51;
    }
   ScanInPort fromBist_52;
   DataOutPort bistEn_52 {
      Source sib_52;
    }
   ScanInPort fromBist_53;
   DataOutPort bistEn_53 {
      Source sib_53;
    }
   ScanInPort fromBist_54;
   DataOutPort bistEn_54 {
      Source sib_54;
    }
   ScanInPort fromBist_55;
   DataOutPort bistEn_55 {
      Source sib_55;
    }
   ScanInPort fromBist_56;
   DataOutPort bistEn_56 {
      Source sib_56;
    }
   ScanInPort fromBist_57;
   DataOutPort bistEn_57 {
      Source sib_57;
    }
   ScanInPort fromBist_58;
   DataOutPort bistEn_58 {
      Source sib_58;
    }
   ScanInPort fromBist_59;
   DataOutPort bistEn_59 {
      Source sib_59;
    }
   ScanInPort fromBist_60;
   DataOutPort bistEn_60 {
      Source sib_60;
    }
   ScanInPort fromBist_61;
   DataOutPort bistEn_61 {
      Source sib_61;
    }
   ScanInPort fromBist_62;
   DataOutPort bistEn_62 {
      Source sib_62;
    }
   ScanInPort fromBist_63;
   DataOutPort bistEn_63 {
      Source sib_63;
    }
   ScanInPort fromBist_64;
   DataOutPort bistEn_64 {
      Source sib_64;
    }
   ScanInPort fromBist_65;
   DataOutPort bistEn_65 {
      Source sib_65;
    }
   ScanInPort fromBist_66;
   DataOutPort bistEn_66 {
      Source sib_66;
    }
   ScanInPort fromBist_67;
   DataOutPort bistEn_67 {
      Source sib_67;
    }
   ScanInPort fromBist_68;
   DataOutPort bistEn_68 {
      Source sib_68;
    }
   ScanInPort fromBist_69;
   DataOutPort bistEn_69 {
      Source sib_69;
    }
   ScanInPort fromBist_70;
   DataOutPort bistEn_70 {
      Source sib_70;
    }
   ScanInPort fromBist_71;
   DataOutPort bistEn_71 {
      Source sib_71;
    }
   ScanInPort fromBist_72;
   DataOutPort bistEn_72 {
      Source sib_72;
    }
   ScanInPort fromBist_73;
   DataOutPort bistEn_73 {
      Source sib_73;
    }
   ScanInPort fromBist_74;
   DataOutPort bistEn_74 {
      Source sib_74;
    }
   ScanInPort fromBist_75;
   DataOutPort bistEn_75 {
      Source sib_75;
    }
   ScanInPort fromBist_76;
   DataOutPort bistEn_76 {
      Source sib_76;
    }
   ScanInPort fromBist_77;
   DataOutPort bistEn_77 {
      Source sib_77;
    }
   ScanInPort fromBist_78;
   DataOutPort bistEn_78 {
      Source sib_78;
    }
   ScanInPort fromBist_79;
   DataOutPort bistEn_79 {
      Source sib_79;
    }
   ScanInPort fromBist_80;
   DataOutPort bistEn_80 {
      Source sib_80;
    }
   ScanInPort fromBist_81;
   DataOutPort bistEn_81 {
      Source sib_81;
    }
   ScanInPort fromBist_82;
   DataOutPort bistEn_82 {
      Source sib_82;
    }
   ScanInPort fromBist_83;
   DataOutPort bistEn_83 {
      Source sib_83;
    }
   ScanInPort fromBist_84;
   DataOutPort bistEn_84 {
      Source sib_84;
    }
   ScanInPort fromBist_85;
   DataOutPort bistEn_85 {
      Source sib_85;
    }
   ScanInPort fromBist_86;
   DataOutPort bistEn_86 {
      Source sib_86;
    }
   ScanInPort fromBist_87;
   DataOutPort bistEn_87 {
      Source sib_87;
    }
   ScanInPort fromBist_88;
   DataOutPort bistEn_88 {
      Source sib_88;
    }
   ScanInPort fromBist_89;
   DataOutPort bistEn_89 {
      Source sib_89;
    }
   ScanInPort fromBist_90;
   DataOutPort bistEn_90 {
      Source sib_90;
    }
   ScanInPort fromBist_91;
   DataOutPort bistEn_91 {
      Source sib_91;
    }
   ScanInPort fromBist_92;
   DataOutPort bistEn_92 {
      Source sib_92;
    }
   ScanInPort fromBist_93;
   DataOutPort bistEn_93 {
      Source sib_93;
    }
   ScanInPort fromBist_94;
   DataOutPort bistEn_94 {
      Source sib_94;
    }
   ScanInPort fromBist_95;
   DataOutPort bistEn_95 {
      Source sib_95;
    }
   ScanInPort fromBist_96;
   DataOutPort bistEn_96 {
      Source sib_96;
    }
   ScanInPort fromBist_97;
   DataOutPort bistEn_97 {
      Source sib_97;
    }
   ScanInPort fromBist_98;
   DataOutPort bistEn_98 {
      Source sib_98;
    }
   ScanInPort fromBist_99;
   DataOutPort bistEn_99 {
      Source sib_99;
    }
   DataInPort MBISTPG_GO_0;
   DataInPort MBISTPG_DONE_0;
   DataInPort MBISTPG_GO_1;
   DataInPort MBISTPG_DONE_1;
   DataInPort MBISTPG_GO_2;
   DataInPort MBISTPG_DONE_2;
   DataInPort MBISTPG_GO_3;
   DataInPort MBISTPG_DONE_3;
   DataInPort MBISTPG_GO_4;
   DataInPort MBISTPG_DONE_4;
   DataInPort MBISTPG_GO_5;
   DataInPort MBISTPG_DONE_5;
   DataInPort MBISTPG_GO_6;
   DataInPort MBISTPG_DONE_6;
   DataInPort MBISTPG_GO_7;
   DataInPort MBISTPG_DONE_7;
   DataInPort MBISTPG_GO_8;
   DataInPort MBISTPG_DONE_8;
   DataInPort MBISTPG_GO_9;
   DataInPort MBISTPG_DONE_9;
   DataInPort MBISTPG_GO_10;
   DataInPort MBISTPG_DONE_10;
   DataInPort MBISTPG_GO_11;
   DataInPort MBISTPG_DONE_11;
   DataInPort MBISTPG_GO_12;
   DataInPort MBISTPG_DONE_12;
   DataInPort MBISTPG_GO_13;
   DataInPort MBISTPG_DONE_13;
   DataInPort MBISTPG_GO_14;
   DataInPort MBISTPG_DONE_14;
   DataInPort MBISTPG_GO_15;
   DataInPort MBISTPG_DONE_15;
   DataInPort MBISTPG_GO_16;
   DataInPort MBISTPG_DONE_16;
   DataInPort MBISTPG_GO_17;
   DataInPort MBISTPG_DONE_17;
   DataInPort MBISTPG_GO_18;
   DataInPort MBISTPG_DONE_18;
   DataInPort MBISTPG_GO_19;
   DataInPort MBISTPG_DONE_19;
   DataInPort MBISTPG_GO_20;
   DataInPort MBISTPG_DONE_20;
   DataInPort MBISTPG_GO_21;
   DataInPort MBISTPG_DONE_21;
   DataInPort MBISTPG_GO_22;
   DataInPort MBISTPG_DONE_22;
   DataInPort MBISTPG_GO_23;
   DataInPort MBISTPG_DONE_23;
   DataInPort MBISTPG_GO_24;
   DataInPort MBISTPG_DONE_24;
   DataInPort MBISTPG_GO_25;
   DataInPort MBISTPG_DONE_25;
   DataInPort MBISTPG_GO_26;
   DataInPort MBISTPG_DONE_26;
   DataInPort MBISTPG_GO_27;
   DataInPort MBISTPG_DONE_27;
   DataInPort MBISTPG_GO_28;
   DataInPort MBISTPG_DONE_28;
   DataInPort MBISTPG_GO_29;
   DataInPort MBISTPG_DONE_29;
   DataInPort MBISTPG_GO_30;
   DataInPort MBISTPG_DONE_30;
   DataInPort MBISTPG_GO_31;
   DataInPort MBISTPG_DONE_31;
   DataInPort MBISTPG_GO_32;
   DataInPort MBISTPG_DONE_32;
   DataInPort MBISTPG_GO_33;
   DataInPort MBISTPG_DONE_33;
   DataInPort MBISTPG_GO_34;
   DataInPort MBISTPG_DONE_34;
   DataInPort MBISTPG_GO_35;
   DataInPort MBISTPG_DONE_35;
   DataInPort MBISTPG_GO_36;
   DataInPort MBISTPG_DONE_36;
   DataInPort MBISTPG_GO_37;
   DataInPort MBISTPG_DONE_37;
   DataInPort MBISTPG_GO_38;
   DataInPort MBISTPG_DONE_38;
   DataInPort MBISTPG_GO_39;
   DataInPort MBISTPG_DONE_39;
   DataInPort MBISTPG_GO_40;
   DataInPort MBISTPG_DONE_40;
   DataInPort MBISTPG_GO_41;
   DataInPort MBISTPG_DONE_41;
   DataInPort MBISTPG_GO_42;
   DataInPort MBISTPG_DONE_42;
   DataInPort MBISTPG_GO_43;
   DataInPort MBISTPG_DONE_43;
   DataInPort MBISTPG_GO_44;
   DataInPort MBISTPG_DONE_44;
   DataInPort MBISTPG_GO_45;
   DataInPort MBISTPG_DONE_45;
   DataInPort MBISTPG_GO_46;
   DataInPort MBISTPG_DONE_46;
   DataInPort MBISTPG_GO_47;
   DataInPort MBISTPG_DONE_47;
   DataInPort MBISTPG_GO_48;
   DataInPort MBISTPG_DONE_48;
   DataInPort MBISTPG_GO_49;
   DataInPort MBISTPG_DONE_49;
   DataInPort MBISTPG_GO_50;
   DataInPort MBISTPG_DONE_50;
   DataInPort MBISTPG_GO_51;
   DataInPort MBISTPG_DONE_51;
   DataInPort MBISTPG_GO_52;
   DataInPort MBISTPG_DONE_52;
   DataInPort MBISTPG_GO_53;
   DataInPort MBISTPG_DONE_53;
   DataInPort MBISTPG_GO_54;
   DataInPort MBISTPG_DONE_54;
   DataInPort MBISTPG_GO_55;
   DataInPort MBISTPG_DONE_55;
   DataInPort MBISTPG_GO_56;
   DataInPort MBISTPG_DONE_56;
   DataInPort MBISTPG_GO_57;
   DataInPort MBISTPG_DONE_57;
   DataInPort MBISTPG_GO_58;
   DataInPort MBISTPG_DONE_58;
   DataInPort MBISTPG_GO_59;
   DataInPort MBISTPG_DONE_59;
   DataInPort MBISTPG_GO_60;
   DataInPort MBISTPG_DONE_60;
   DataInPort MBISTPG_GO_61;
   DataInPort MBISTPG_DONE_61;
   DataInPort MBISTPG_GO_62;
   DataInPort MBISTPG_DONE_62;
   DataInPort MBISTPG_GO_63;
   DataInPort MBISTPG_DONE_63;
   DataInPort MBISTPG_GO_64;
   DataInPort MBISTPG_DONE_64;
   DataInPort MBISTPG_GO_65;
   DataInPort MBISTPG_DONE_65;
   DataInPort MBISTPG_GO_66;
   DataInPort MBISTPG_DONE_66;
   DataInPort MBISTPG_GO_67;
   DataInPort MBISTPG_DONE_67;
   DataInPort MBISTPG_GO_68;
   DataInPort MBISTPG_DONE_68;
   DataInPort MBISTPG_GO_69;
   DataInPort MBISTPG_DONE_69;
   DataInPort MBISTPG_GO_70;
   DataInPort MBISTPG_DONE_70;
   DataInPort MBISTPG_GO_71;
   DataInPort MBISTPG_DONE_71;
   DataInPort MBISTPG_GO_72;
   DataInPort MBISTPG_DONE_72;
   DataInPort MBISTPG_GO_73;
   DataInPort MBISTPG_DONE_73;
   DataInPort MBISTPG_GO_74;
   DataInPort MBISTPG_DONE_74;
   DataInPort MBISTPG_GO_75;
   DataInPort MBISTPG_DONE_75;
   DataInPort MBISTPG_GO_76;
   DataInPort MBISTPG_DONE_76;
   DataInPort MBISTPG_GO_77;
   DataInPort MBISTPG_DONE_77;
   DataInPort MBISTPG_GO_78;
   DataInPort MBISTPG_DONE_78;
   DataInPort MBISTPG_GO_79;
   DataInPort MBISTPG_DONE_79;
   DataInPort MBISTPG_GO_80;
   DataInPort MBISTPG_DONE_80;
   DataInPort MBISTPG_GO_81;
   DataInPort MBISTPG_DONE_81;
   DataInPort MBISTPG_GO_82;
   DataInPort MBISTPG_DONE_82;
   DataInPort MBISTPG_GO_83;
   DataInPort MBISTPG_DONE_83;
   DataInPort MBISTPG_GO_84;
   DataInPort MBISTPG_DONE_84;
   DataInPort MBISTPG_GO_85;
   DataInPort MBISTPG_DONE_85;
   DataInPort MBISTPG_GO_86;
   DataInPort MBISTPG_DONE_86;
   DataInPort MBISTPG_GO_87;
   DataInPort MBISTPG_DONE_87;
   DataInPort MBISTPG_GO_88;
   DataInPort MBISTPG_DONE_88;
   DataInPort MBISTPG_GO_89;
   DataInPort MBISTPG_DONE_89;
   DataInPort MBISTPG_GO_90;
   DataInPort MBISTPG_DONE_90;
   DataInPort MBISTPG_GO_91;
   DataInPort MBISTPG_DONE_91;
   DataInPort MBISTPG_GO_92;
   DataInPort MBISTPG_DONE_92;
   DataInPort MBISTPG_GO_93;
   DataInPort MBISTPG_DONE_93;
   DataInPort MBISTPG_GO_94;
   DataInPort MBISTPG_DONE_94;
   DataInPort MBISTPG_GO_95;
   DataInPort MBISTPG_DONE_95;
   DataInPort MBISTPG_GO_96;
   DataInPort MBISTPG_DONE_96;
   DataInPort MBISTPG_GO_97;
   DataInPort MBISTPG_DONE_97;
   DataInPort MBISTPG_GO_98;
   DataInPort MBISTPG_DONE_98;
   DataInPort MBISTPG_GO_99;
   DataInPort MBISTPG_DONE_99;

   DataOutPort TCK_MODE {
      Source TCK_MODE_tdr;
      RefEnum OnOff;
   }
   DataOutPort BIST_ASYNC_RESET {
      Source BIST_ASYNC_RESET_tdr;
      RefEnum OnOff;
   }
   DataOutPort BIST_SETUP[2:0] {
      Source BIST_SETUP_tdr[2:0];
   }
   ScanInterface client {
      Port si;
      Port so;
      Port ijtag_select;
   }
   
   ScanInterface host_0 {
      Port toBist_0;
      Port fromBist_0;
      Port shift_en_R;
   }
   ScanInterface host_1 {
      Port toBist_1;
      Port fromBist_1;
      Port shift_en_R;
   }
   ScanInterface host_2 {
      Port toBist_2;
      Port fromBist_2;
      Port shift_en_R;
   }
   ScanInterface host_3 {
      Port toBist_3;
      Port fromBist_3;
      Port shift_en_R;
   }
   ScanInterface host_4 {
      Port toBist_4;
      Port fromBist_4;
      Port shift_en_R;
   }
   ScanInterface host_5 {
      Port toBist_5;
      Port fromBist_5;
      Port shift_en_R;
   }
   ScanInterface host_6 {
      Port toBist_6;
      Port fromBist_6;
      Port shift_en_R;
   }
   ScanInterface host_7 {
      Port toBist_7;
      Port fromBist_7;
      Port shift_en_R;
   }
   ScanInterface host_8 {
      Port toBist_8;
      Port fromBist_8;
      Port shift_en_R;
   }
   ScanInterface host_9 {
      Port toBist_9;
      Port fromBist_9;
      Port shift_en_R;
   }
   ScanInterface host_10 {
      Port toBist_10;
      Port fromBist_10;
      Port shift_en_R;
   }
   ScanInterface host_11 {
      Port toBist_11;
      Port fromBist_11;
      Port shift_en_R;
   }
   ScanInterface host_12 {
      Port toBist_12;
      Port fromBist_12;
      Port shift_en_R;
   }
   ScanInterface host_13 {
      Port toBist_13;
      Port fromBist_13;
      Port shift_en_R;
   }
   ScanInterface host_14 {
      Port toBist_14;
      Port fromBist_14;
      Port shift_en_R;
   }
   ScanInterface host_15 {
      Port toBist_15;
      Port fromBist_15;
      Port shift_en_R;
   }
   ScanInterface host_16 {
      Port toBist_16;
      Port fromBist_16;
      Port shift_en_R;
   }
   ScanInterface host_17 {
      Port toBist_17;
      Port fromBist_17;
      Port shift_en_R;
   }
   ScanInterface host_18 {
      Port toBist_18;
      Port fromBist_18;
      Port shift_en_R;
   }
   ScanInterface host_19 {
      Port toBist_19;
      Port fromBist_19;
      Port shift_en_R;
   }
   ScanInterface host_20 {
      Port toBist_20;
      Port fromBist_20;
      Port shift_en_R;
   }
   ScanInterface host_21 {
      Port toBist_21;
      Port fromBist_21;
      Port shift_en_R;
   }
   ScanInterface host_22 {
      Port toBist_22;
      Port fromBist_22;
      Port shift_en_R;
   }
   ScanInterface host_23 {
      Port toBist_23;
      Port fromBist_23;
      Port shift_en_R;
   }
   ScanInterface host_24 {
      Port toBist_24;
      Port fromBist_24;
      Port shift_en_R;
   }
   ScanInterface host_25 {
      Port toBist_25;
      Port fromBist_25;
      Port shift_en_R;
   }
   ScanInterface host_26 {
      Port toBist_26;
      Port fromBist_26;
      Port shift_en_R;
   }
   ScanInterface host_27 {
      Port toBist_27;
      Port fromBist_27;
      Port shift_en_R;
   }
   ScanInterface host_28 {
      Port toBist_28;
      Port fromBist_28;
      Port shift_en_R;
   }
   ScanInterface host_29 {
      Port toBist_29;
      Port fromBist_29;
      Port shift_en_R;
   }
   ScanInterface host_30 {
      Port toBist_30;
      Port fromBist_30;
      Port shift_en_R;
   }
   ScanInterface host_31 {
      Port toBist_31;
      Port fromBist_31;
      Port shift_en_R;
   }
   ScanInterface host_32 {
      Port toBist_32;
      Port fromBist_32;
      Port shift_en_R;
   }
   ScanInterface host_33 {
      Port toBist_33;
      Port fromBist_33;
      Port shift_en_R;
   }
   ScanInterface host_34 {
      Port toBist_34;
      Port fromBist_34;
      Port shift_en_R;
   }
   ScanInterface host_35 {
      Port toBist_35;
      Port fromBist_35;
      Port shift_en_R;
   }
   ScanInterface host_36 {
      Port toBist_36;
      Port fromBist_36;
      Port shift_en_R;
   }
   ScanInterface host_37 {
      Port toBist_37;
      Port fromBist_37;
      Port shift_en_R;
   }
   ScanInterface host_38 {
      Port toBist_38;
      Port fromBist_38;
      Port shift_en_R;
   }
   ScanInterface host_39 {
      Port toBist_39;
      Port fromBist_39;
      Port shift_en_R;
   }
   ScanInterface host_40 {
      Port toBist_40;
      Port fromBist_40;
      Port shift_en_R;
   }
   ScanInterface host_41 {
      Port toBist_41;
      Port fromBist_41;
      Port shift_en_R;
   }
   ScanInterface host_42 {
      Port toBist_42;
      Port fromBist_42;
      Port shift_en_R;
   }
   ScanInterface host_43 {
      Port toBist_43;
      Port fromBist_43;
      Port shift_en_R;
   }
   ScanInterface host_44 {
      Port toBist_44;
      Port fromBist_44;
      Port shift_en_R;
   }
   ScanInterface host_45 {
      Port toBist_45;
      Port fromBist_45;
      Port shift_en_R;
   }
   ScanInterface host_46 {
      Port toBist_46;
      Port fromBist_46;
      Port shift_en_R;
   }
   ScanInterface host_47 {
      Port toBist_47;
      Port fromBist_47;
      Port shift_en_R;
   }
   ScanInterface host_48 {
      Port toBist_48;
      Port fromBist_48;
      Port shift_en_R;
   }
   ScanInterface host_49 {
      Port toBist_49;
      Port fromBist_49;
      Port shift_en_R;
   }
   ScanInterface host_50 {
      Port toBist_50;
      Port fromBist_50;
      Port shift_en_R;
   }
   ScanInterface host_51 {
      Port toBist_51;
      Port fromBist_51;
      Port shift_en_R;
   }
   ScanInterface host_52 {
      Port toBist_52;
      Port fromBist_52;
      Port shift_en_R;
   }
   ScanInterface host_53 {
      Port toBist_53;
      Port fromBist_53;
      Port shift_en_R;
   }
   ScanInterface host_54 {
      Port toBist_54;
      Port fromBist_54;
      Port shift_en_R;
   }
   ScanInterface host_55 {
      Port toBist_55;
      Port fromBist_55;
      Port shift_en_R;
   }
   ScanInterface host_56 {
      Port toBist_56;
      Port fromBist_56;
      Port shift_en_R;
   }
   ScanInterface host_57 {
      Port toBist_57;
      Port fromBist_57;
      Port shift_en_R;
   }
   ScanInterface host_58 {
      Port toBist_58;
      Port fromBist_58;
      Port shift_en_R;
   }
   ScanInterface host_59 {
      Port toBist_59;
      Port fromBist_59;
      Port shift_en_R;
   }
   ScanInterface host_60 {
      Port toBist_60;
      Port fromBist_60;
      Port shift_en_R;
   }
   ScanInterface host_61 {
      Port toBist_61;
      Port fromBist_61;
      Port shift_en_R;
   }
   ScanInterface host_62 {
      Port toBist_62;
      Port fromBist_62;
      Port shift_en_R;
   }
   ScanInterface host_63 {
      Port toBist_63;
      Port fromBist_63;
      Port shift_en_R;
   }
   ScanInterface host_64 {
      Port toBist_64;
      Port fromBist_64;
      Port shift_en_R;
   }
   ScanInterface host_65 {
      Port toBist_65;
      Port fromBist_65;
      Port shift_en_R;
   }
   ScanInterface host_66 {
      Port toBist_66;
      Port fromBist_66;
      Port shift_en_R;
   }
   ScanInterface host_67 {
      Port toBist_67;
      Port fromBist_67;
      Port shift_en_R;
   }
   ScanInterface host_68 {
      Port toBist_68;
      Port fromBist_68;
      Port shift_en_R;
   }
   ScanInterface host_69 {
      Port toBist_69;
      Port fromBist_69;
      Port shift_en_R;
   }
   ScanInterface host_70 {
      Port toBist_70;
      Port fromBist_70;
      Port shift_en_R;
   }
   ScanInterface host_71 {
      Port toBist_71;
      Port fromBist_71;
      Port shift_en_R;
   }
   ScanInterface host_72 {
      Port toBist_72;
      Port fromBist_72;
      Port shift_en_R;
   }
   ScanInterface host_73 {
      Port toBist_73;
      Port fromBist_73;
      Port shift_en_R;
   }
   ScanInterface host_74 {
      Port toBist_74;
      Port fromBist_74;
      Port shift_en_R;
   }
   ScanInterface host_75 {
      Port toBist_75;
      Port fromBist_75;
      Port shift_en_R;
   }
   ScanInterface host_76 {
      Port toBist_76;
      Port fromBist_76;
      Port shift_en_R;
   }
   ScanInterface host_77 {
      Port toBist_77;
      Port fromBist_77;
      Port shift_en_R;
   }
   ScanInterface host_78 {
      Port toBist_78;
      Port fromBist_78;
      Port shift_en_R;
   }
   ScanInterface host_79 {
      Port toBist_79;
      Port fromBist_79;
      Port shift_en_R;
   }
   ScanInterface host_80 {
      Port toBist_80;
      Port fromBist_80;
      Port shift_en_R;
   }
   ScanInterface host_81 {
      Port toBist_81;
      Port fromBist_81;
      Port shift_en_R;
   }
   ScanInterface host_82 {
      Port toBist_82;
      Port fromBist_82;
      Port shift_en_R;
   }
   ScanInterface host_83 {
      Port toBist_83;
      Port fromBist_83;
      Port shift_en_R;
   }
   ScanInterface host_84 {
      Port toBist_84;
      Port fromBist_84;
      Port shift_en_R;
   }
   ScanInterface host_85 {
      Port toBist_85;
      Port fromBist_85;
      Port shift_en_R;
   }
   ScanInterface host_86 {
      Port toBist_86;
      Port fromBist_86;
      Port shift_en_R;
   }
   ScanInterface host_87 {
      Port toBist_87;
      Port fromBist_87;
      Port shift_en_R;
   }
   ScanInterface host_88 {
      Port toBist_88;
      Port fromBist_88;
      Port shift_en_R;
   }
   ScanInterface host_89 {
      Port toBist_89;
      Port fromBist_89;
      Port shift_en_R;
   }
   ScanInterface host_90 {
      Port toBist_90;
      Port fromBist_90;
      Port shift_en_R;
   }
   ScanInterface host_91 {
      Port toBist_91;
      Port fromBist_91;
      Port shift_en_R;
   }
   ScanInterface host_92 {
      Port toBist_92;
      Port fromBist_92;
      Port shift_en_R;
   }
   ScanInterface host_93 {
      Port toBist_93;
      Port fromBist_93;
      Port shift_en_R;
   }
   ScanInterface host_94 {
      Port toBist_94;
      Port fromBist_94;
      Port shift_en_R;
   }
   ScanInterface host_95 {
      Port toBist_95;
      Port fromBist_95;
      Port shift_en_R;
   }
   ScanInterface host_96 {
      Port toBist_96;
      Port fromBist_96;
      Port shift_en_R;
   }
   ScanInterface host_97 {
      Port toBist_97;
      Port fromBist_97;
      Port shift_en_R;
   }
   ScanInterface host_98 {
      Port toBist_98;
      Port fromBist_98;
      Port shift_en_R;
   }
   ScanInterface host_99 {
      Port toBist_99;
      Port fromBist_99;
      Port shift_en_R;
   }
   Enum OnOff {
      ON = 1'b1;
      OFF = 1'b0;
   }
   ScanRegister tdr[190:0] {
      ScanInSource si;
      CaptureSource MBISTPG_DONE_99, MBISTPG_DONE_98, MBISTPG_DONE_97, MBISTPG_DONE_96, MBISTPG_DONE_95, MBISTPG_DONE_94, MBISTPG_DONE_93, MBISTPG_DONE_92, MBISTPG_DONE_91, MBISTPG_DONE_90, MBISTPG_DONE_89, MBISTPG_DONE_88, MBISTPG_DONE_87, MBISTPG_DONE_86, MBISTPG_DONE_85, MBISTPG_DONE_84, MBISTPG_DONE_83, MBISTPG_DONE_82, MBISTPG_DONE_81, MBISTPG_DONE_80, MBISTPG_DONE_79, MBISTPG_DONE_78, MBISTPG_DONE_77, MBISTPG_DONE_76, MBISTPG_DONE_75, MBISTPG_DONE_74, MBISTPG_DONE_73, MBISTPG_DONE_72, MBISTPG_DONE_71, MBISTPG_DONE_70, MBISTPG_DONE_69, MBISTPG_DONE_68, MBISTPG_DONE_67, MBISTPG_DONE_66, MBISTPG_DONE_65, MBISTPG_DONE_64, MBISTPG_DONE_63, MBISTPG_DONE_62, MBISTPG_DONE_61, MBISTPG_DONE_60, MBISTPG_DONE_59, MBISTPG_DONE_58, MBISTPG_DONE_57, MBISTPG_DONE_56, MBISTPG_DONE_55, MBISTPG_DONE_54, MBISTPG_DONE_53, MBISTPG_DONE_52, MBISTPG_DONE_51, MBISTPG_DONE_50, MBISTPG_DONE_49, MBISTPG_DONE_48, MBISTPG_DONE_47, MBISTPG_DONE_46, MBISTPG_DONE_45, MBISTPG_DONE_44, MBISTPG_DONE_43, MBISTPG_DONE_42, MBISTPG_DONE_41, MBISTPG_DONE_40, MBISTPG_DONE_39, MBISTPG_DONE_38, MBISTPG_DONE_37, MBISTPG_DONE_36, MBISTPG_DONE_35, MBISTPG_DONE_34, MBISTPG_DONE_33, MBISTPG_DONE_32, MBISTPG_DONE_31, MBISTPG_DONE_30, MBISTPG_DONE_29, MBISTPG_DONE_28, MBISTPG_DONE_27, MBISTPG_DONE_26, MBISTPG_DONE_25, MBISTPG_DONE_24, MBISTPG_DONE_23, MBISTPG_DONE_22, MBISTPG_DONE_21, MBISTPG_DONE_20, MBISTPG_DONE_19, MBISTPG_DONE_18, MBISTPG_DONE_17, MBISTPG_DONE_16, MBISTPG_DONE_15, MBISTPG_DONE_14, MBISTPG_DONE_13, MBISTPG_DONE_12, MBISTPG_DONE_11, MBISTPG_DONE_10, MBISTPG_DONE_9, MBISTPG_DONE_8, MBISTPG_DONE_7, MBISTPG_DONE_6, MBISTPG_DONE_5, MBISTPG_DONE_4, MBISTPG_DONE_3, MBISTPG_DONE_2, MBISTPG_DONE_1, MBISTPG_DONE_0, MBISTPG_GO_99, MBISTPG_GO_98, MBISTPG_GO_97, MBISTPG_GO_96, MBISTPG_GO_95, MBISTPG_GO_94, MBISTPG_GO_93, MBISTPG_GO_92, MBISTPG_GO_91, MBISTPG_GO_90, MBISTPG_GO_89, MBISTPG_GO_88, MBISTPG_GO_87, MBISTPG_GO_86, MBISTPG_GO_85, MBISTPG_GO_84, MBISTPG_GO_83, MBISTPG_GO_82, MBISTPG_GO_81, MBISTPG_GO_80, MBISTPG_GO_79, MBISTPG_GO_78, MBISTPG_GO_77, MBISTPG_GO_76, MBISTPG_GO_75, MBISTPG_GO_74, MBISTPG_GO_73, MBISTPG_GO_72, MBISTPG_GO_71, MBISTPG_GO_70, MBISTPG_GO_69, MBISTPG_GO_68, MBISTPG_GO_67, MBISTPG_GO_66, MBISTPG_GO_65, MBISTPG_GO_64, MBISTPG_GO_63, MBISTPG_GO_62, MBISTPG_GO_61, MBISTPG_GO_60, MBISTPG_GO_59, MBISTPG_GO_58, MBISTPG_GO_57, MBISTPG_GO_56, MBISTPG_GO_55, MBISTPG_GO_54, MBISTPG_GO_53, MBISTPG_GO_52, MBISTPG_GO_51, MBISTPG_GO_50, MBISTPG_GO_49, MBISTPG_GO_48, MBISTPG_GO_47, MBISTPG_GO_46, MBISTPG_GO_45, MBISTPG_GO_44, MBISTPG_GO_43, MBISTPG_GO_42, MBISTPG_GO_41, MBISTPG_GO_40, MBISTPG_GO_39, MBISTPG_GO_38, MBISTPG_GO_37, MBISTPG_GO_36, MBISTPG_GO_35, MBISTPG_GO_34, MBISTPG_GO_33, MBISTPG_GO_32, MBISTPG_GO_31, MBISTPG_GO_30, MBISTPG_GO_29, MBISTPG_GO_28, MBISTPG_GO_27, MBISTPG_GO_26, MBISTPG_GO_25, MBISTPG_GO_24, MBISTPG_GO_23, MBISTPG_GO_22, MBISTPG_GO_21, MBISTPG_GO_20, MBISTPG_GO_19, MBISTPG_GO_18, MBISTPG_GO_17, MBISTPG_GO_16, MBISTPG_GO_15, MBISTPG_GO_14, MBISTPG_GO_13, MBISTPG_GO_12, MBISTPG_GO_11, MBISTPG_GO_10, MBISTPG_GO_9;
   }
   ScanRegister BIST_SETUP_tdr[2:0] {
      ScanInSource tdr[0];
      CaptureSource MBISTPG_GO_8, MBISTPG_GO_7, MBISTPG_GO_6;
      DefaultLoadValue 3'b000;
      ResetValue 3'b000;
   }
   ScanRegister TCK_MODE_tdr {
      ScanInSource BIST_SETUP_tdr[0];
      CaptureSource MBISTPG_GO_5;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister CHAIN_BYPASS_EN_tdr {
      ScanInSource TCK_MODE_tdr;
      CaptureSource MBISTPG_GO_4;
      DefaultLoadValue 1'b1;
      ResetValue 1'b1;
   }
   ScanRegister BIST_ASYNC_RESET_tdr {
      ScanInSource CHAIN_BYPASS_EN_tdr;
      CaptureSource MBISTPG_GO_3;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister BIST_setting1_tdr {
      ScanInSource BIST_ASYNC_RESET_tdr;
      CaptureSource MBISTPG_GO_2;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister BIST_setting2_tdr {
      ScanInSource BIST_setting1_tdr;
      CaptureSource MBISTPG_GO_1;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister BIST_setting3_tdr {
      ScanInSource BIST_setting2_tdr;
      CaptureSource MBISTPG_GO_0;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_0 {
      ScanInSource fromBistMux_0;
      CaptureSource sib_0;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_1 {
      ScanInSource fromBistMux_1;
      CaptureSource sib_1;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_2 {
      ScanInSource fromBistMux_2;
      CaptureSource sib_2;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_3 {
      ScanInSource fromBistMux_3;
      CaptureSource sib_3;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_4 {
      ScanInSource fromBistMux_4;
      CaptureSource sib_4;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_5 {
      ScanInSource fromBistMux_5;
      CaptureSource sib_5;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_6 {
      ScanInSource fromBistMux_6;
      CaptureSource sib_6;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_7 {
      ScanInSource fromBistMux_7;
      CaptureSource sib_7;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_8 {
      ScanInSource fromBistMux_8;
      CaptureSource sib_8;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_9 {
      ScanInSource fromBistMux_9;
      CaptureSource sib_9;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_10 {
      ScanInSource fromBistMux_10;
      CaptureSource sib_10;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_11 {
      ScanInSource fromBistMux_11;
      CaptureSource sib_11;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_12 {
      ScanInSource fromBistMux_12;
      CaptureSource sib_12;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_13 {
      ScanInSource fromBistMux_13;
      CaptureSource sib_13;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_14 {
      ScanInSource fromBistMux_14;
      CaptureSource sib_14;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_15 {
      ScanInSource fromBistMux_15;
      CaptureSource sib_15;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_16 {
      ScanInSource fromBistMux_16;
      CaptureSource sib_16;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_17 {
      ScanInSource fromBistMux_17;
      CaptureSource sib_17;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_18 {
      ScanInSource fromBistMux_18;
      CaptureSource sib_18;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_19 {
      ScanInSource fromBistMux_19;
      CaptureSource sib_19;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_20 {
      ScanInSource fromBistMux_20;
      CaptureSource sib_20;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_21 {
      ScanInSource fromBistMux_21;
      CaptureSource sib_21;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_22 {
      ScanInSource fromBistMux_22;
      CaptureSource sib_22;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_23 {
      ScanInSource fromBistMux_23;
      CaptureSource sib_23;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_24 {
      ScanInSource fromBistMux_24;
      CaptureSource sib_24;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_25 {
      ScanInSource fromBistMux_25;
      CaptureSource sib_25;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_26 {
      ScanInSource fromBistMux_26;
      CaptureSource sib_26;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_27 {
      ScanInSource fromBistMux_27;
      CaptureSource sib_27;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_28 {
      ScanInSource fromBistMux_28;
      CaptureSource sib_28;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_29 {
      ScanInSource fromBistMux_29;
      CaptureSource sib_29;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_30 {
      ScanInSource fromBistMux_30;
      CaptureSource sib_30;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_31 {
      ScanInSource fromBistMux_31;
      CaptureSource sib_31;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_32 {
      ScanInSource fromBistMux_32;
      CaptureSource sib_32;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_33 {
      ScanInSource fromBistMux_33;
      CaptureSource sib_33;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_34 {
      ScanInSource fromBistMux_34;
      CaptureSource sib_34;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_35 {
      ScanInSource fromBistMux_35;
      CaptureSource sib_35;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_36 {
      ScanInSource fromBistMux_36;
      CaptureSource sib_36;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_37 {
      ScanInSource fromBistMux_37;
      CaptureSource sib_37;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_38 {
      ScanInSource fromBistMux_38;
      CaptureSource sib_38;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_39 {
      ScanInSource fromBistMux_39;
      CaptureSource sib_39;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_40 {
      ScanInSource fromBistMux_40;
      CaptureSource sib_40;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_41 {
      ScanInSource fromBistMux_41;
      CaptureSource sib_41;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_42 {
      ScanInSource fromBistMux_42;
      CaptureSource sib_42;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_43 {
      ScanInSource fromBistMux_43;
      CaptureSource sib_43;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_44 {
      ScanInSource fromBistMux_44;
      CaptureSource sib_44;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_45 {
      ScanInSource fromBistMux_45;
      CaptureSource sib_45;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_46 {
      ScanInSource fromBistMux_46;
      CaptureSource sib_46;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_47 {
      ScanInSource fromBistMux_47;
      CaptureSource sib_47;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_48 {
      ScanInSource fromBistMux_48;
      CaptureSource sib_48;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_49 {
      ScanInSource fromBistMux_49;
      CaptureSource sib_49;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_50 {
      ScanInSource fromBistMux_50;
      CaptureSource sib_50;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_51 {
      ScanInSource fromBistMux_51;
      CaptureSource sib_51;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_52 {
      ScanInSource fromBistMux_52;
      CaptureSource sib_52;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_53 {
      ScanInSource fromBistMux_53;
      CaptureSource sib_53;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_54 {
      ScanInSource fromBistMux_54;
      CaptureSource sib_54;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_55 {
      ScanInSource fromBistMux_55;
      CaptureSource sib_55;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_56 {
      ScanInSource fromBistMux_56;
      CaptureSource sib_56;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_57 {
      ScanInSource fromBistMux_57;
      CaptureSource sib_57;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_58 {
      ScanInSource fromBistMux_58;
      CaptureSource sib_58;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_59 {
      ScanInSource fromBistMux_59;
      CaptureSource sib_59;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_60 {
      ScanInSource fromBistMux_60;
      CaptureSource sib_60;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_61 {
      ScanInSource fromBistMux_61;
      CaptureSource sib_61;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_62 {
      ScanInSource fromBistMux_62;
      CaptureSource sib_62;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_63 {
      ScanInSource fromBistMux_63;
      CaptureSource sib_63;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_64 {
      ScanInSource fromBistMux_64;
      CaptureSource sib_64;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_65 {
      ScanInSource fromBistMux_65;
      CaptureSource sib_65;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_66 {
      ScanInSource fromBistMux_66;
      CaptureSource sib_66;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_67 {
      ScanInSource fromBistMux_67;
      CaptureSource sib_67;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_68 {
      ScanInSource fromBistMux_68;
      CaptureSource sib_68;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_69 {
      ScanInSource fromBistMux_69;
      CaptureSource sib_69;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_70 {
      ScanInSource fromBistMux_70;
      CaptureSource sib_70;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_71 {
      ScanInSource fromBistMux_71;
      CaptureSource sib_71;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_72 {
      ScanInSource fromBistMux_72;
      CaptureSource sib_72;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_73 {
      ScanInSource fromBistMux_73;
      CaptureSource sib_73;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_74 {
      ScanInSource fromBistMux_74;
      CaptureSource sib_74;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_75 {
      ScanInSource fromBistMux_75;
      CaptureSource sib_75;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_76 {
      ScanInSource fromBistMux_76;
      CaptureSource sib_76;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_77 {
      ScanInSource fromBistMux_77;
      CaptureSource sib_77;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_78 {
      ScanInSource fromBistMux_78;
      CaptureSource sib_78;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_79 {
      ScanInSource fromBistMux_79;
      CaptureSource sib_79;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_80 {
      ScanInSource fromBistMux_80;
      CaptureSource sib_80;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_81 {
      ScanInSource fromBistMux_81;
      CaptureSource sib_81;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_82 {
      ScanInSource fromBistMux_82;
      CaptureSource sib_82;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_83 {
      ScanInSource fromBistMux_83;
      CaptureSource sib_83;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_84 {
      ScanInSource fromBistMux_84;
      CaptureSource sib_84;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_85 {
      ScanInSource fromBistMux_85;
      CaptureSource sib_85;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_86 {
      ScanInSource fromBistMux_86;
      CaptureSource sib_86;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_87 {
      ScanInSource fromBistMux_87;
      CaptureSource sib_87;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_88 {
      ScanInSource fromBistMux_88;
      CaptureSource sib_88;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_89 {
      ScanInSource fromBistMux_89;
      CaptureSource sib_89;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_90 {
      ScanInSource fromBistMux_90;
      CaptureSource sib_90;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_91 {
      ScanInSource fromBistMux_91;
      CaptureSource sib_91;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_92 {
      ScanInSource fromBistMux_92;
      CaptureSource sib_92;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_93 {
      ScanInSource fromBistMux_93;
      CaptureSource sib_93;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_94 {
      ScanInSource fromBistMux_94;
      CaptureSource sib_94;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_95 {
      ScanInSource fromBistMux_95;
      CaptureSource sib_95;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_96 {
      ScanInSource fromBistMux_96;
      CaptureSource sib_96;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_97 {
      ScanInSource fromBistMux_97;
      CaptureSource sib_97;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_98 {
      ScanInSource fromBistMux_98;
      CaptureSource sib_98;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_99 {
      ScanInSource fromBistMux_99;
      CaptureSource sib_99;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanMux fromBistMux_99 SelectedBy sib_99, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_99;
      3'bxxx : BIST_setting3_tdr;
   }
   ScanMux fromBistMux_0 SelectedBy sib_0, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_0;
      3'bxxx : sib_1;
   }
   ScanMux fromBistMux_1 SelectedBy sib_1, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_1;
      3'bxxx : sib_2;
   }
   ScanMux fromBistMux_2 SelectedBy sib_2, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_2;
      3'bxxx : sib_3;
   }
   ScanMux fromBistMux_3 SelectedBy sib_3, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_3;
      3'bxxx : sib_4;
   }
   ScanMux fromBistMux_4 SelectedBy sib_4, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_4;
      3'bxxx : sib_5;
   }
   ScanMux fromBistMux_5 SelectedBy sib_5, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_5;
      3'bxxx : sib_6;
   }
   ScanMux fromBistMux_6 SelectedBy sib_6, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_6;
      3'bxxx : sib_7;
   }
   ScanMux fromBistMux_7 SelectedBy sib_7, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_7;
      3'bxxx : sib_8;
   }
   ScanMux fromBistMux_8 SelectedBy sib_8, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_8;
      3'bxxx : sib_9;
   }
   ScanMux fromBistMux_9 SelectedBy sib_9, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_9;
      3'bxxx : sib_10;
   }
   ScanMux fromBistMux_10 SelectedBy sib_10, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_10;
      3'bxxx : sib_11;
   }
   ScanMux fromBistMux_11 SelectedBy sib_11, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_11;
      3'bxxx : sib_12;
   }
   ScanMux fromBistMux_12 SelectedBy sib_12, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_12;
      3'bxxx : sib_13;
   }
   ScanMux fromBistMux_13 SelectedBy sib_13, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_13;
      3'bxxx : sib_14;
   }
   ScanMux fromBistMux_14 SelectedBy sib_14, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_14;
      3'bxxx : sib_15;
   }
   ScanMux fromBistMux_15 SelectedBy sib_15, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_15;
      3'bxxx : sib_16;
   }
   ScanMux fromBistMux_16 SelectedBy sib_16, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_16;
      3'bxxx : sib_17;
   }
   ScanMux fromBistMux_17 SelectedBy sib_17, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_17;
      3'bxxx : sib_18;
   }
   ScanMux fromBistMux_18 SelectedBy sib_18, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_18;
      3'bxxx : sib_19;
   }
   ScanMux fromBistMux_19 SelectedBy sib_19, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_19;
      3'bxxx : sib_20;
   }
   ScanMux fromBistMux_20 SelectedBy sib_20, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_20;
      3'bxxx : sib_21;
   }
   ScanMux fromBistMux_21 SelectedBy sib_21, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_21;
      3'bxxx : sib_22;
   }
   ScanMux fromBistMux_22 SelectedBy sib_22, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_22;
      3'bxxx : sib_23;
   }
   ScanMux fromBistMux_23 SelectedBy sib_23, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_23;
      3'bxxx : sib_24;
   }
   ScanMux fromBistMux_24 SelectedBy sib_24, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_24;
      3'bxxx : sib_25;
   }
   ScanMux fromBistMux_25 SelectedBy sib_25, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_25;
      3'bxxx : sib_26;
   }
   ScanMux fromBistMux_26 SelectedBy sib_26, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_26;
      3'bxxx : sib_27;
   }
   ScanMux fromBistMux_27 SelectedBy sib_27, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_27;
      3'bxxx : sib_28;
   }
   ScanMux fromBistMux_28 SelectedBy sib_28, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_28;
      3'bxxx : sib_29;
   }
   ScanMux fromBistMux_29 SelectedBy sib_29, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_29;
      3'bxxx : sib_30;
   }
   ScanMux fromBistMux_30 SelectedBy sib_30, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_30;
      3'bxxx : sib_31;
   }
   ScanMux fromBistMux_31 SelectedBy sib_31, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_31;
      3'bxxx : sib_32;
   }
   ScanMux fromBistMux_32 SelectedBy sib_32, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_32;
      3'bxxx : sib_33;
   }
   ScanMux fromBistMux_33 SelectedBy sib_33, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_33;
      3'bxxx : sib_34;
   }
   ScanMux fromBistMux_34 SelectedBy sib_34, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_34;
      3'bxxx : sib_35;
   }
   ScanMux fromBistMux_35 SelectedBy sib_35, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_35;
      3'bxxx : sib_36;
   }
   ScanMux fromBistMux_36 SelectedBy sib_36, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_36;
      3'bxxx : sib_37;
   }
   ScanMux fromBistMux_37 SelectedBy sib_37, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_37;
      3'bxxx : sib_38;
   }
   ScanMux fromBistMux_38 SelectedBy sib_38, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_38;
      3'bxxx : sib_39;
   }
   ScanMux fromBistMux_39 SelectedBy sib_39, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_39;
      3'bxxx : sib_40;
   }
   ScanMux fromBistMux_40 SelectedBy sib_40, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_40;
      3'bxxx : sib_41;
   }
   ScanMux fromBistMux_41 SelectedBy sib_41, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_41;
      3'bxxx : sib_42;
   }
   ScanMux fromBistMux_42 SelectedBy sib_42, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_42;
      3'bxxx : sib_43;
   }
   ScanMux fromBistMux_43 SelectedBy sib_43, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_43;
      3'bxxx : sib_44;
   }
   ScanMux fromBistMux_44 SelectedBy sib_44, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_44;
      3'bxxx : sib_45;
   }
   ScanMux fromBistMux_45 SelectedBy sib_45, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_45;
      3'bxxx : sib_46;
   }
   ScanMux fromBistMux_46 SelectedBy sib_46, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_46;
      3'bxxx : sib_47;
   }
   ScanMux fromBistMux_47 SelectedBy sib_47, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_47;
      3'bxxx : sib_48;
   }
   ScanMux fromBistMux_48 SelectedBy sib_48, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_48;
      3'bxxx : sib_49;
   }
   ScanMux fromBistMux_49 SelectedBy sib_49, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_49;
      3'bxxx : sib_50;
   }
   ScanMux fromBistMux_50 SelectedBy sib_50, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_50;
      3'bxxx : sib_51;
   }
   ScanMux fromBistMux_51 SelectedBy sib_51, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_51;
      3'bxxx : sib_52;
   }
   ScanMux fromBistMux_52 SelectedBy sib_52, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_52;
      3'bxxx : sib_53;
   }
   ScanMux fromBistMux_53 SelectedBy sib_53, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_53;
      3'bxxx : sib_54;
   }
   ScanMux fromBistMux_54 SelectedBy sib_54, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_54;
      3'bxxx : sib_55;
   }
   ScanMux fromBistMux_55 SelectedBy sib_55, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_55;
      3'bxxx : sib_56;
   }
   ScanMux fromBistMux_56 SelectedBy sib_56, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_56;
      3'bxxx : sib_57;
   }
   ScanMux fromBistMux_57 SelectedBy sib_57, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_57;
      3'bxxx : sib_58;
   }
   ScanMux fromBistMux_58 SelectedBy sib_58, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_58;
      3'bxxx : sib_59;
   }
   ScanMux fromBistMux_59 SelectedBy sib_59, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_59;
      3'bxxx : sib_60;
   }
   ScanMux fromBistMux_60 SelectedBy sib_60, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_60;
      3'bxxx : sib_61;
   }
   ScanMux fromBistMux_61 SelectedBy sib_61, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_61;
      3'bxxx : sib_62;
   }
   ScanMux fromBistMux_62 SelectedBy sib_62, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_62;
      3'bxxx : sib_63;
   }
   ScanMux fromBistMux_63 SelectedBy sib_63, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_63;
      3'bxxx : sib_64;
   }
   ScanMux fromBistMux_64 SelectedBy sib_64, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_64;
      3'bxxx : sib_65;
   }
   ScanMux fromBistMux_65 SelectedBy sib_65, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_65;
      3'bxxx : sib_66;
   }
   ScanMux fromBistMux_66 SelectedBy sib_66, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_66;
      3'bxxx : sib_67;
   }
   ScanMux fromBistMux_67 SelectedBy sib_67, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_67;
      3'bxxx : sib_68;
   }
   ScanMux fromBistMux_68 SelectedBy sib_68, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_68;
      3'bxxx : sib_69;
   }
   ScanMux fromBistMux_69 SelectedBy sib_69, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_69;
      3'bxxx : sib_70;
   }
   ScanMux fromBistMux_70 SelectedBy sib_70, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_70;
      3'bxxx : sib_71;
   }
   ScanMux fromBistMux_71 SelectedBy sib_71, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_71;
      3'bxxx : sib_72;
   }
   ScanMux fromBistMux_72 SelectedBy sib_72, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_72;
      3'bxxx : sib_73;
   }
   ScanMux fromBistMux_73 SelectedBy sib_73, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_73;
      3'bxxx : sib_74;
   }
   ScanMux fromBistMux_74 SelectedBy sib_74, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_74;
      3'bxxx : sib_75;
   }
   ScanMux fromBistMux_75 SelectedBy sib_75, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_75;
      3'bxxx : sib_76;
   }
   ScanMux fromBistMux_76 SelectedBy sib_76, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_76;
      3'bxxx : sib_77;
   }
   ScanMux fromBistMux_77 SelectedBy sib_77, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_77;
      3'bxxx : sib_78;
   }
   ScanMux fromBistMux_78 SelectedBy sib_78, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_78;
      3'bxxx : sib_79;
   }
   ScanMux fromBistMux_79 SelectedBy sib_79, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_79;
      3'bxxx : sib_80;
   }
   ScanMux fromBistMux_80 SelectedBy sib_80, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_80;
      3'bxxx : sib_81;
   }
   ScanMux fromBistMux_81 SelectedBy sib_81, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_81;
      3'bxxx : sib_82;
   }
   ScanMux fromBistMux_82 SelectedBy sib_82, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_82;
      3'bxxx : sib_83;
   }
   ScanMux fromBistMux_83 SelectedBy sib_83, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_83;
      3'bxxx : sib_84;
   }
   ScanMux fromBistMux_84 SelectedBy sib_84, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_84;
      3'bxxx : sib_85;
   }
   ScanMux fromBistMux_85 SelectedBy sib_85, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_85;
      3'bxxx : sib_86;
   }
   ScanMux fromBistMux_86 SelectedBy sib_86, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_86;
      3'bxxx : sib_87;
   }
   ScanMux fromBistMux_87 SelectedBy sib_87, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_87;
      3'bxxx : sib_88;
   }
   ScanMux fromBistMux_88 SelectedBy sib_88, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_88;
      3'bxxx : sib_89;
   }
   ScanMux fromBistMux_89 SelectedBy sib_89, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_89;
      3'bxxx : sib_90;
   }
   ScanMux fromBistMux_90 SelectedBy sib_90, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_90;
      3'bxxx : sib_91;
   }
   ScanMux fromBistMux_91 SelectedBy sib_91, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_91;
      3'bxxx : sib_92;
   }
   ScanMux fromBistMux_92 SelectedBy sib_92, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_92;
      3'bxxx : sib_93;
   }
   ScanMux fromBistMux_93 SelectedBy sib_93, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_93;
      3'bxxx : sib_94;
   }
   ScanMux fromBistMux_94 SelectedBy sib_94, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_94;
      3'bxxx : sib_95;
   }
   ScanMux fromBistMux_95 SelectedBy sib_95, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_95;
      3'bxxx : sib_96;
   }
   ScanMux fromBistMux_96 SelectedBy sib_96, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_96;
      3'bxxx : sib_97;
   }
   ScanMux fromBistMux_97 SelectedBy sib_97, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_97;
      3'bxxx : sib_98;
   }
   ScanMux fromBistMux_98 SelectedBy sib_98, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_98;
      3'bxxx : sib_99;
   }
}

Module mbist_controller {
   ClockPort BIST_CLK;
   DataInPort MBISTPG_EN;

   DataInPort MBISTPG_ASYNC_RESETN;

   DataInPort BIST_CONTROL[2:0];
   DataInPort TCK_MODE;
   DataInPort setting1 {
      RefEnum OnOff;
   }
   DataInPort setting2 {
      RefEnum OnOff;
   }
   DataInPort setting3 {
      RefEnum OnOff;
   }
   DataOutPort MBISTPG_GO {
      RefEnum PassFail;
   }
   DataOutPort MBISTPG_DONE {
      RefEnum PassFail;
   }
   TCKPort TCK;
   ScanInPort BIST_SI;
   ScanOutPort MBISTPG_SO {
      Source CONTROLLER_SETUP_CHAIN;
   }
   ShiftEnPort BIST_SHIFT;
   ToShiftEnPort BIST_SHIFT_COLLAR;
   ScanOutPort MEM0_BIST_COLLAR_SI {
      Source MEM1_BIST_COLLAR_SO;
   }
   ScanInPort MEM0_BIST_COLLAR_SO;
   ScanOutPort MEM1_BIST_COLLAR_SI {
      Source MEM2_BIST_COLLAR_SO;
   }
   ScanInPort MEM1_BIST_COLLAR_SO;
   ScanOutPort MEM2_BIST_COLLAR_SI {
      Source MEM3_BIST_COLLAR_SO;
   }
   ScanInPort MEM2_BIST_COLLAR_SO;
   ScanOutPort MEM3_BIST_COLLAR_SI {
      Source MEM4_BIST_COLLAR_SO;
   }
   ScanInPort MEM3_BIST_COLLAR_SO;
   ScanOutPort MEM4_BIST_COLLAR_SI {
      Source BIST_TO_COLLAR_SO_MUX;
   }
   ScanInPort MEM4_BIST_COLLAR_SO;

   ScanInterface Client {
      Port BIST_SI;
      Port MBISTPG_SO;
      Port BIST_SHIFT;
   }

   ScanInterface MEM0_INTERFACE {
      Port BIST_SHIFT_COLLAR;
      Port MEM0_BIST_COLLAR_SI;
      Port MEM0_BIST_COLLAR_SO;
   }
   ScanInterface MEM1_INTERFACE {
      Port BIST_SHIFT_COLLAR;
      Port MEM1_BIST_COLLAR_SI;
      Port MEM1_BIST_COLLAR_SO;
   }
   ScanInterface MEM2_INTERFACE {
      Port BIST_SHIFT_COLLAR;
      Port MEM2_BIST_COLLAR_SI;
      Port MEM2_BIST_COLLAR_SO;
   }
   ScanInterface MEM3_INTERFACE {
      Port BIST_SHIFT_COLLAR;
      Port MEM3_BIST_COLLAR_SI;
      Port MEM3_BIST_COLLAR_SO;
   }
   ScanInterface MEM4_INTERFACE {
      Port BIST_SHIFT_COLLAR;
      Port MEM4_BIST_COLLAR_SI;
      Port MEM4_BIST_COLLAR_SO;
   }




   Enum PassFail {
      Pass = 1'b1;
      Fail = 1'b0;
      Ignore = 1'bx;
   }
   Enum OnOff {
      ON  = 1'b1;
      OFF = 1'b0;
   }

   ScanRegister MBIST_SETTINGS1[9:0] {
      ScanInSource BIST_SI;
   }
   ScanRegister MBIST_SETTINGS2[9:0] {
      ScanInSource MBIST_SETTINGS1[0];
   }
   ScanRegister MBIST_SETTINGS3[9:0] {
      ScanInSource MBIST_SETTINGS2[0];
   }

   ScanRegister MBIST_RESULT[9:0] {
      ScanInSource MEM0_BIST_COLLAR_SO;
   }

   ScanMux BIST_TO_COLLAR_SO_MUX SelectedBy mode1, mode2 {
      2'b01 : MBIST_SETTINGS2[0];
      2'b10 : MBIST_SETTINGS3[0];
   }
   ScanMux CONTROLLER_SETUP_CHAIN SelectedBy MBISTPG_EN, BIST_CONTROL {
      1'b1, 3'b00x : MBIST_RESULT[0];
   }
   LogicSignal mode1 {
      MBISTPG_EN, BIST_CONTROL == 1'b1, 3'b001;
   }
   LogicSignal mode2 {
      MBISTPG_EN, BIST_CONTROL == 1'b1, 3'b000;
   }
}


Module core {
   ClockPort clk;
   CaptureEnPort ijtag_ce;
   ResetPort ijtag_reset {
      ActivePolarity 0;
   }
   ShiftEnPort ijtag_se;
   SelectPort ijtag_sel;
   ScanInPort ijtag_si;
   ScanOutPort ijtag_so {
      Source sib_inst.ijtag_so;
   }
   TCKPort ijtag_tck;
   UpdateEnPort ijtag_ue;
   ScanInterface ijtag {
      Port ijtag_ce;
      Port ijtag_reset;
      Port ijtag_se;
      Port ijtag_sel;
      Port ijtag_si;
      Port ijtag_so;
      Port ijtag_tck;
      Port ijtag_ue;
   }
   

   // Memory block 0
   Instance memory_block_0_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_0_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_0_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_0_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_0_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_0_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_0_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_0_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_0_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_0_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_0;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_0.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_0.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_0_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_0_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_0_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_0_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_0_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_0_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_0_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_0_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_0_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_0_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_0;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_0.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_0.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_0_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_0_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_0_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_0_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_0_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_0_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_0_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_0_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_0_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_0_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_0;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_0.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_0.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_0_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_0_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_0_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_0_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_0_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_0_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_0_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_0_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_0_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_0_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_0;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_0.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_0.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_0_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_0_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_0_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_0_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_0_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_0_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_0_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_0_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_0_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_0_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_0;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_0.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_0.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_0 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_0;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_0;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_0_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_0_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_0_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_0_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_0_inst_4.BIST_SO;
   }

   // Memory block 1
   Instance memory_block_1_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_1_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_1_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_1_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_1_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_1_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_1_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_1_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_1_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_1_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_1;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_1.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_1.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_1_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_1_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_1_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_1_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_1_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_1_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_1_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_1_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_1_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_1_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_1;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_1.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_1.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_1_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_1_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_1_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_1_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_1_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_1_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_1_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_1_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_1_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_1_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_1;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_1.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_1.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_1_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_1_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_1_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_1_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_1_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_1_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_1_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_1_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_1_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_1_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_1;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_1.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_1.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_1_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_1_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_1_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_1_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_1_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_1_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_1_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_1_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_1_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_1_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_1;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_1.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_1.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_1 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_1;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_1;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_1_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_1_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_1_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_1_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_1_inst_4.BIST_SO;
   }

   // Memory block 2
   Instance memory_block_2_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_2_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_2_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_2_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_2_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_2_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_2_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_2_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_2_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_2_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_2;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_2.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_2.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_2_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_2_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_2_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_2_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_2_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_2_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_2_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_2_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_2_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_2_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_2;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_2.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_2.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_2_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_2_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_2_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_2_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_2_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_2_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_2_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_2_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_2_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_2_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_2;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_2.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_2.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_2_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_2_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_2_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_2_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_2_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_2_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_2_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_2_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_2_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_2_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_2;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_2.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_2.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_2_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_2_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_2_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_2_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_2_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_2_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_2_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_2_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_2_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_2_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_2;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_2.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_2.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_2 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_2;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_2;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_2_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_2_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_2_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_2_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_2_inst_4.BIST_SO;
   }

   // Memory block 3
   Instance memory_block_3_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_3_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_3_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_3_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_3_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_3_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_3_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_3_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_3_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_3_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_3;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_3.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_3.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_3_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_3_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_3_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_3_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_3_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_3_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_3_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_3_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_3_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_3_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_3;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_3.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_3.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_3_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_3_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_3_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_3_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_3_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_3_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_3_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_3_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_3_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_3_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_3;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_3.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_3.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_3_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_3_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_3_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_3_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_3_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_3_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_3_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_3_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_3_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_3_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_3;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_3.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_3.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_3_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_3_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_3_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_3_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_3_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_3_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_3_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_3_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_3_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_3_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_3;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_3.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_3.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_3 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_3;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_3;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_3_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_3_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_3_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_3_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_3_inst_4.BIST_SO;
   }

   // Memory block 4
   Instance memory_block_4_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_4_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_4_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_4_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_4_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_4_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_4_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_4_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_4_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_4_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_4;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_4.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_4.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_4_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_4_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_4_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_4_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_4_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_4_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_4_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_4_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_4_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_4_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_4;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_4.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_4.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_4_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_4_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_4_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_4_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_4_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_4_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_4_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_4_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_4_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_4_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_4;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_4.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_4.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_4_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_4_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_4_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_4_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_4_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_4_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_4_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_4_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_4_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_4_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_4;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_4.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_4.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_4_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_4_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_4_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_4_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_4_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_4_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_4_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_4_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_4_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_4_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_4;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_4.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_4.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_4 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_4;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_4;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_4_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_4_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_4_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_4_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_4_inst_4.BIST_SO;
   }

   // Memory block 5
   Instance memory_block_5_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_5_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_5_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_5_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_5_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_5_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_5_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_5_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_5_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_5_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_5;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_5.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_5.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_5_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_5_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_5_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_5_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_5_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_5_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_5_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_5_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_5_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_5_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_5;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_5.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_5.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_5_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_5_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_5_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_5_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_5_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_5_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_5_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_5_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_5_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_5_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_5;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_5.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_5.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_5_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_5_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_5_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_5_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_5_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_5_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_5_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_5_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_5_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_5_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_5;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_5.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_5.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_5_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_5_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_5_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_5_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_5_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_5_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_5_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_5_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_5_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_5_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_5;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_5.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_5.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_5 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_5;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_5;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_5_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_5_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_5_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_5_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_5_inst_4.BIST_SO;
   }

   // Memory block 6
   Instance memory_block_6_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_6_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_6_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_6_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_6_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_6_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_6_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_6_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_6_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_6_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_6;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_6.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_6.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_6_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_6_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_6_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_6_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_6_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_6_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_6_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_6_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_6_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_6_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_6;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_6.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_6.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_6_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_6_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_6_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_6_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_6_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_6_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_6_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_6_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_6_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_6_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_6;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_6.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_6.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_6_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_6_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_6_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_6_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_6_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_6_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_6_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_6_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_6_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_6_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_6;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_6.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_6.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_6_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_6_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_6_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_6_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_6_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_6_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_6_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_6_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_6_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_6_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_6;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_6.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_6.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_6 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_6;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_6;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_6_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_6_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_6_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_6_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_6_inst_4.BIST_SO;
   }

   // Memory block 7
   Instance memory_block_7_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_7_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_7_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_7_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_7_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_7_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_7_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_7_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_7_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_7_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_7;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_7.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_7.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_7_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_7_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_7_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_7_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_7_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_7_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_7_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_7_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_7_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_7_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_7;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_7.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_7.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_7_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_7_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_7_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_7_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_7_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_7_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_7_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_7_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_7_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_7_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_7;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_7.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_7.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_7_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_7_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_7_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_7_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_7_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_7_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_7_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_7_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_7_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_7_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_7;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_7.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_7.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_7_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_7_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_7_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_7_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_7_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_7_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_7_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_7_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_7_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_7_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_7;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_7.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_7.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_7 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_7;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_7;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_7_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_7_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_7_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_7_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_7_inst_4.BIST_SO;
   }

   // Memory block 8
   Instance memory_block_8_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_8_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_8_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_8_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_8_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_8_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_8_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_8_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_8_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_8_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_8;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_8.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_8.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_8_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_8_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_8_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_8_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_8_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_8_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_8_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_8_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_8_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_8_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_8;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_8.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_8.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_8_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_8_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_8_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_8_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_8_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_8_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_8_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_8_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_8_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_8_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_8;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_8.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_8.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_8_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_8_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_8_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_8_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_8_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_8_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_8_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_8_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_8_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_8_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_8;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_8.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_8.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_8_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_8_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_8_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_8_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_8_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_8_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_8_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_8_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_8_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_8_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_8;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_8.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_8.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_8 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_8;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_8;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_8_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_8_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_8_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_8_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_8_inst_4.BIST_SO;
   }

   // Memory block 9
   Instance memory_block_9_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_9_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_9_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_9_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_9_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_9_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_9_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_9_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_9_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_9_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_9;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_9.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_9.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_9_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_9_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_9_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_9_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_9_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_9_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_9_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_9_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_9_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_9_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_9;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_9.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_9.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_9_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_9_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_9_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_9_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_9_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_9_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_9_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_9_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_9_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_9_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_9;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_9.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_9.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_9_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_9_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_9_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_9_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_9_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_9_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_9_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_9_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_9_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_9_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_9;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_9.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_9.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_9_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_9_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_9_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_9_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_9_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_9_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_9_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_9_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_9_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_9_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_9;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_9.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_9.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_9 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_9;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_9;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_9_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_9_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_9_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_9_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_9_inst_4.BIST_SO;
   }

   // Memory block 10
   Instance memory_block_10_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_10_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_10_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_10_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_10_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_10_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_10_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_10_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_10_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_10_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_10;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_10.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_10.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_10_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_10_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_10_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_10_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_10_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_10_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_10_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_10_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_10_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_10_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_10;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_10.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_10.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_10_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_10_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_10_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_10_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_10_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_10_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_10_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_10_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_10_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_10_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_10;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_10.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_10.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_10_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_10_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_10_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_10_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_10_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_10_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_10_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_10_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_10_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_10_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_10;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_10.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_10.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_10_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_10_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_10_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_10_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_10_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_10_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_10_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_10_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_10_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_10_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_10;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_10.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_10.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_10 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_10;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_10;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_10_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_10_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_10_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_10_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_10_inst_4.BIST_SO;
   }

   // Memory block 11
   Instance memory_block_11_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_11_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_11_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_11_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_11_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_11_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_11_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_11_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_11_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_11_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_11;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_11.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_11.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_11_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_11_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_11_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_11_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_11_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_11_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_11_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_11_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_11_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_11_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_11;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_11.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_11.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_11_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_11_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_11_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_11_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_11_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_11_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_11_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_11_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_11_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_11_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_11;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_11.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_11.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_11_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_11_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_11_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_11_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_11_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_11_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_11_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_11_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_11_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_11_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_11;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_11.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_11.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_11_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_11_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_11_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_11_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_11_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_11_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_11_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_11_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_11_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_11_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_11;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_11.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_11.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_11 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_11;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_11;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_11_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_11_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_11_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_11_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_11_inst_4.BIST_SO;
   }

   // Memory block 12
   Instance memory_block_12_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_12_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_12_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_12_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_12_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_12_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_12_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_12_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_12_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_12_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_12;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_12.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_12.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_12_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_12_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_12_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_12_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_12_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_12_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_12_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_12_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_12_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_12_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_12;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_12.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_12.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_12_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_12_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_12_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_12_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_12_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_12_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_12_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_12_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_12_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_12_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_12;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_12.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_12.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_12_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_12_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_12_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_12_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_12_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_12_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_12_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_12_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_12_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_12_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_12;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_12.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_12.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_12_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_12_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_12_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_12_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_12_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_12_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_12_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_12_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_12_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_12_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_12;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_12.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_12.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_12 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_12;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_12;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_12_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_12_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_12_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_12_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_12_inst_4.BIST_SO;
   }

   // Memory block 13
   Instance memory_block_13_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_13_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_13_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_13_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_13_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_13_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_13_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_13_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_13_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_13_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_13;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_13.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_13.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_13_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_13_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_13_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_13_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_13_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_13_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_13_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_13_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_13_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_13_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_13;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_13.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_13.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_13_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_13_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_13_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_13_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_13_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_13_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_13_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_13_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_13_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_13_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_13;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_13.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_13.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_13_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_13_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_13_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_13_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_13_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_13_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_13_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_13_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_13_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_13_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_13;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_13.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_13.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_13_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_13_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_13_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_13_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_13_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_13_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_13_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_13_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_13_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_13_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_13;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_13.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_13.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_13 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_13;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_13;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_13_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_13_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_13_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_13_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_13_inst_4.BIST_SO;
   }

   // Memory block 14
   Instance memory_block_14_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_14_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_14_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_14_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_14_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_14_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_14_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_14_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_14_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_14_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_14;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_14.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_14.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_14_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_14_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_14_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_14_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_14_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_14_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_14_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_14_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_14_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_14_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_14;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_14.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_14.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_14_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_14_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_14_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_14_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_14_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_14_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_14_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_14_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_14_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_14_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_14;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_14.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_14.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_14_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_14_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_14_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_14_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_14_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_14_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_14_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_14_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_14_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_14_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_14;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_14.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_14.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_14_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_14_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_14_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_14_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_14_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_14_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_14_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_14_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_14_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_14_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_14;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_14.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_14.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_14 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_14;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_14;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_14_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_14_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_14_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_14_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_14_inst_4.BIST_SO;
   }

   // Memory block 15
   Instance memory_block_15_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_15_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_15_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_15_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_15_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_15_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_15_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_15_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_15_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_15_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_15;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_15.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_15.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_15_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_15_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_15_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_15_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_15_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_15_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_15_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_15_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_15_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_15_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_15;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_15.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_15.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_15_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_15_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_15_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_15_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_15_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_15_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_15_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_15_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_15_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_15_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_15;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_15.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_15.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_15_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_15_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_15_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_15_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_15_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_15_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_15_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_15_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_15_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_15_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_15;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_15.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_15.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_15_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_15_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_15_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_15_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_15_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_15_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_15_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_15_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_15_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_15_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_15;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_15.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_15.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_15 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_15;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_15;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_15_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_15_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_15_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_15_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_15_inst_4.BIST_SO;
   }

   // Memory block 16
   Instance memory_block_16_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_16_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_16_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_16_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_16_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_16_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_16_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_16_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_16_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_16_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_16;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_16.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_16.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_16_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_16_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_16_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_16_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_16_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_16_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_16_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_16_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_16_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_16_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_16;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_16.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_16.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_16_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_16_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_16_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_16_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_16_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_16_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_16_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_16_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_16_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_16_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_16;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_16.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_16.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_16_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_16_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_16_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_16_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_16_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_16_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_16_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_16_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_16_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_16_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_16;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_16.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_16.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_16_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_16_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_16_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_16_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_16_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_16_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_16_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_16_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_16_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_16_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_16;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_16.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_16.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_16 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_16;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_16;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_16_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_16_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_16_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_16_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_16_inst_4.BIST_SO;
   }

   // Memory block 17
   Instance memory_block_17_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_17_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_17_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_17_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_17_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_17_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_17_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_17_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_17_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_17_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_17;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_17.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_17.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_17_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_17_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_17_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_17_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_17_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_17_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_17_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_17_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_17_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_17_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_17;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_17.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_17.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_17_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_17_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_17_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_17_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_17_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_17_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_17_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_17_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_17_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_17_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_17;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_17.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_17.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_17_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_17_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_17_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_17_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_17_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_17_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_17_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_17_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_17_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_17_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_17;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_17.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_17.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_17_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_17_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_17_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_17_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_17_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_17_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_17_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_17_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_17_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_17_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_17;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_17.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_17.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_17 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_17;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_17;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_17_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_17_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_17_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_17_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_17_inst_4.BIST_SO;
   }

   // Memory block 18
   Instance memory_block_18_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_18_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_18_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_18_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_18_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_18_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_18_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_18_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_18_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_18_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_18;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_18.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_18.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_18_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_18_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_18_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_18_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_18_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_18_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_18_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_18_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_18_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_18_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_18;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_18.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_18.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_18_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_18_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_18_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_18_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_18_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_18_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_18_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_18_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_18_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_18_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_18;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_18.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_18.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_18_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_18_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_18_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_18_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_18_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_18_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_18_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_18_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_18_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_18_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_18;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_18.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_18.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_18_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_18_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_18_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_18_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_18_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_18_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_18_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_18_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_18_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_18_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_18;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_18.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_18.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_18 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_18;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_18;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_18_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_18_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_18_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_18_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_18_inst_4.BIST_SO;
   }

   // Memory block 19
   Instance memory_block_19_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_19_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_19_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_19_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_19_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_19_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_19_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_19_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_19_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_19_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_19;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_19.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_19.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_19_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_19_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_19_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_19_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_19_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_19_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_19_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_19_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_19_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_19_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_19;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_19.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_19.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_19_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_19_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_19_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_19_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_19_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_19_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_19_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_19_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_19_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_19_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_19;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_19.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_19.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_19_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_19_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_19_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_19_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_19_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_19_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_19_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_19_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_19_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_19_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_19;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_19.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_19.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_19_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_19_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_19_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_19_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_19_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_19_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_19_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_19_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_19_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_19_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_19;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_19.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_19.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_19 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_19;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_19;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_19_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_19_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_19_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_19_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_19_inst_4.BIST_SO;
   }

   // Memory block 20
   Instance memory_block_20_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_20_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_20_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_20_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_20_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_20_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_20_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_20_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_20_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_20_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_20;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_20.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_20.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_20_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_20_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_20_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_20_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_20_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_20_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_20_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_20_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_20_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_20_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_20;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_20.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_20.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_20_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_20_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_20_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_20_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_20_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_20_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_20_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_20_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_20_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_20_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_20;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_20.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_20.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_20_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_20_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_20_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_20_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_20_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_20_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_20_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_20_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_20_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_20_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_20;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_20.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_20.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_20_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_20_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_20_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_20_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_20_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_20_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_20_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_20_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_20_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_20_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_20;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_20.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_20.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_20 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_20;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_20;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_20_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_20_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_20_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_20_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_20_inst_4.BIST_SO;
   }

   // Memory block 21
   Instance memory_block_21_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_21_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_21_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_21_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_21_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_21_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_21_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_21_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_21_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_21_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_21;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_21.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_21.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_21_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_21_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_21_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_21_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_21_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_21_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_21_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_21_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_21_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_21_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_21;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_21.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_21.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_21_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_21_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_21_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_21_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_21_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_21_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_21_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_21_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_21_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_21_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_21;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_21.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_21.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_21_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_21_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_21_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_21_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_21_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_21_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_21_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_21_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_21_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_21_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_21;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_21.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_21.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_21_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_21_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_21_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_21_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_21_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_21_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_21_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_21_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_21_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_21_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_21;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_21.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_21.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_21 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_21;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_21;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_21_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_21_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_21_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_21_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_21_inst_4.BIST_SO;
   }

   // Memory block 22
   Instance memory_block_22_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_22_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_22_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_22_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_22_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_22_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_22_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_22_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_22_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_22_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_22;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_22.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_22.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_22_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_22_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_22_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_22_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_22_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_22_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_22_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_22_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_22_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_22_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_22;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_22.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_22.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_22_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_22_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_22_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_22_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_22_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_22_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_22_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_22_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_22_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_22_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_22;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_22.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_22.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_22_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_22_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_22_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_22_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_22_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_22_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_22_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_22_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_22_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_22_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_22;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_22.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_22.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_22_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_22_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_22_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_22_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_22_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_22_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_22_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_22_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_22_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_22_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_22;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_22.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_22.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_22 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_22;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_22;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_22_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_22_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_22_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_22_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_22_inst_4.BIST_SO;
   }

   // Memory block 23
   Instance memory_block_23_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_23_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_23_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_23_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_23_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_23_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_23_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_23_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_23_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_23_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_23;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_23.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_23.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_23_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_23_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_23_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_23_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_23_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_23_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_23_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_23_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_23_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_23_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_23;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_23.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_23.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_23_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_23_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_23_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_23_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_23_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_23_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_23_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_23_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_23_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_23_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_23;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_23.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_23.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_23_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_23_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_23_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_23_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_23_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_23_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_23_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_23_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_23_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_23_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_23;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_23.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_23.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_23_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_23_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_23_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_23_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_23_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_23_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_23_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_23_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_23_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_23_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_23;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_23.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_23.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_23 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_23;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_23;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_23_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_23_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_23_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_23_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_23_inst_4.BIST_SO;
   }

   // Memory block 24
   Instance memory_block_24_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_24_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_24_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_24_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_24_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_24_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_24_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_24_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_24_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_24_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_24;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_24.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_24.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_24_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_24_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_24_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_24_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_24_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_24_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_24_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_24_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_24_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_24_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_24;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_24.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_24.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_24_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_24_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_24_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_24_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_24_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_24_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_24_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_24_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_24_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_24_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_24;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_24.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_24.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_24_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_24_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_24_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_24_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_24_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_24_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_24_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_24_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_24_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_24_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_24;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_24.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_24.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_24_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_24_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_24_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_24_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_24_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_24_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_24_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_24_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_24_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_24_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_24;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_24.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_24.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_24 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_24;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_24;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_24_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_24_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_24_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_24_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_24_inst_4.BIST_SO;
   }

   // Memory block 25
   Instance memory_block_25_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_25_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_25_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_25_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_25_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_25_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_25_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_25_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_25_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_25_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_25;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_25.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_25.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_25_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_25_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_25_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_25_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_25_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_25_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_25_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_25_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_25_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_25_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_25;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_25.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_25.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_25_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_25_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_25_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_25_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_25_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_25_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_25_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_25_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_25_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_25_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_25;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_25.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_25.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_25_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_25_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_25_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_25_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_25_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_25_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_25_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_25_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_25_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_25_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_25;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_25.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_25.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_25_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_25_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_25_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_25_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_25_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_25_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_25_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_25_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_25_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_25_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_25;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_25.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_25.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_25 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_25;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_25;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_25_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_25_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_25_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_25_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_25_inst_4.BIST_SO;
   }

   // Memory block 26
   Instance memory_block_26_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_26_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_26_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_26_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_26_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_26_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_26_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_26_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_26_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_26_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_26;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_26.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_26.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_26_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_26_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_26_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_26_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_26_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_26_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_26_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_26_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_26_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_26_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_26;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_26.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_26.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_26_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_26_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_26_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_26_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_26_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_26_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_26_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_26_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_26_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_26_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_26;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_26.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_26.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_26_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_26_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_26_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_26_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_26_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_26_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_26_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_26_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_26_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_26_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_26;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_26.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_26.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_26_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_26_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_26_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_26_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_26_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_26_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_26_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_26_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_26_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_26_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_26;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_26.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_26.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_26 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_26;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_26;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_26_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_26_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_26_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_26_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_26_inst_4.BIST_SO;
   }

   // Memory block 27
   Instance memory_block_27_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_27_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_27_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_27_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_27_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_27_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_27_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_27_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_27_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_27_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_27;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_27.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_27.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_27_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_27_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_27_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_27_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_27_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_27_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_27_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_27_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_27_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_27_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_27;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_27.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_27.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_27_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_27_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_27_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_27_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_27_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_27_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_27_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_27_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_27_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_27_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_27;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_27.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_27.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_27_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_27_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_27_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_27_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_27_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_27_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_27_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_27_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_27_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_27_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_27;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_27.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_27.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_27_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_27_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_27_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_27_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_27_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_27_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_27_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_27_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_27_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_27_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_27;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_27.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_27.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_27 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_27;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_27;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_27_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_27_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_27_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_27_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_27_inst_4.BIST_SO;
   }

   // Memory block 28
   Instance memory_block_28_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_28_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_28_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_28_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_28_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_28_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_28_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_28_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_28_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_28_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_28;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_28.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_28.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_28_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_28_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_28_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_28_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_28_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_28_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_28_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_28_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_28_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_28_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_28;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_28.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_28.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_28_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_28_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_28_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_28_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_28_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_28_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_28_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_28_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_28_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_28_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_28;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_28.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_28.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_28_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_28_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_28_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_28_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_28_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_28_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_28_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_28_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_28_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_28_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_28;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_28.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_28.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_28_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_28_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_28_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_28_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_28_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_28_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_28_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_28_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_28_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_28_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_28;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_28.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_28.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_28 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_28;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_28;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_28_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_28_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_28_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_28_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_28_inst_4.BIST_SO;
   }

   // Memory block 29
   Instance memory_block_29_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_29_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_29_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_29_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_29_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_29_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_29_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_29_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_29_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_29_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_29;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_29.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_29.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_29_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_29_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_29_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_29_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_29_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_29_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_29_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_29_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_29_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_29_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_29;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_29.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_29.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_29_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_29_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_29_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_29_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_29_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_29_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_29_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_29_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_29_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_29_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_29;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_29.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_29.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_29_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_29_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_29_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_29_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_29_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_29_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_29_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_29_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_29_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_29_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_29;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_29.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_29.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_29_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_29_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_29_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_29_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_29_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_29_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_29_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_29_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_29_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_29_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_29;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_29.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_29.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_29 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_29;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_29;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_29_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_29_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_29_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_29_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_29_inst_4.BIST_SO;
   }

   // Memory block 30
   Instance memory_block_30_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_30_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_30_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_30_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_30_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_30_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_30_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_30_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_30_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_30_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_30;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_30.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_30.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_30_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_30_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_30_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_30_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_30_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_30_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_30_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_30_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_30_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_30_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_30;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_30.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_30.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_30_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_30_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_30_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_30_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_30_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_30_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_30_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_30_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_30_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_30_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_30;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_30.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_30.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_30_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_30_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_30_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_30_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_30_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_30_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_30_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_30_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_30_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_30_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_30;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_30.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_30.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_30_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_30_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_30_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_30_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_30_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_30_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_30_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_30_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_30_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_30_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_30;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_30.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_30.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_30 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_30;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_30;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_30_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_30_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_30_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_30_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_30_inst_4.BIST_SO;
   }

   // Memory block 31
   Instance memory_block_31_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_31_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_31_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_31_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_31_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_31_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_31_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_31_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_31_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_31_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_31;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_31.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_31.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_31_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_31_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_31_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_31_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_31_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_31_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_31_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_31_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_31_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_31_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_31;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_31.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_31.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_31_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_31_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_31_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_31_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_31_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_31_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_31_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_31_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_31_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_31_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_31;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_31.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_31.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_31_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_31_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_31_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_31_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_31_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_31_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_31_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_31_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_31_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_31_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_31;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_31.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_31.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_31_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_31_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_31_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_31_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_31_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_31_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_31_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_31_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_31_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_31_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_31;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_31.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_31.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_31 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_31;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_31;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_31_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_31_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_31_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_31_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_31_inst_4.BIST_SO;
   }

   // Memory block 32
   Instance memory_block_32_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_32_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_32_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_32_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_32_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_32_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_32_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_32_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_32_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_32_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_32;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_32.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_32.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_32_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_32_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_32_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_32_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_32_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_32_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_32_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_32_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_32_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_32_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_32;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_32.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_32.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_32_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_32_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_32_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_32_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_32_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_32_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_32_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_32_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_32_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_32_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_32;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_32.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_32.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_32_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_32_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_32_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_32_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_32_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_32_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_32_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_32_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_32_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_32_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_32;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_32.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_32.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_32_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_32_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_32_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_32_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_32_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_32_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_32_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_32_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_32_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_32_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_32;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_32.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_32.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_32 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_32;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_32;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_32_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_32_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_32_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_32_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_32_inst_4.BIST_SO;
   }

   // Memory block 33
   Instance memory_block_33_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_33_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_33_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_33_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_33_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_33_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_33_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_33_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_33_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_33_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_33;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_33.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_33.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_33_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_33_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_33_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_33_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_33_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_33_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_33_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_33_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_33_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_33_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_33;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_33.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_33.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_33_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_33_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_33_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_33_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_33_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_33_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_33_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_33_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_33_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_33_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_33;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_33.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_33.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_33_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_33_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_33_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_33_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_33_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_33_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_33_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_33_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_33_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_33_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_33;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_33.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_33.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_33_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_33_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_33_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_33_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_33_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_33_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_33_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_33_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_33_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_33_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_33;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_33.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_33.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_33 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_33;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_33;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_33_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_33_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_33_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_33_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_33_inst_4.BIST_SO;
   }

   // Memory block 34
   Instance memory_block_34_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_34_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_34_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_34_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_34_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_34_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_34_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_34_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_34_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_34_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_34;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_34.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_34.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_34_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_34_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_34_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_34_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_34_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_34_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_34_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_34_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_34_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_34_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_34;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_34.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_34.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_34_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_34_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_34_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_34_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_34_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_34_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_34_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_34_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_34_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_34_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_34;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_34.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_34.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_34_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_34_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_34_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_34_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_34_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_34_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_34_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_34_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_34_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_34_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_34;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_34.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_34.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_34_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_34_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_34_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_34_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_34_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_34_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_34_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_34_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_34_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_34_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_34;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_34.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_34.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_34 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_34;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_34;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_34_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_34_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_34_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_34_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_34_inst_4.BIST_SO;
   }

   // Memory block 35
   Instance memory_block_35_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_35_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_35_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_35_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_35_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_35_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_35_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_35_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_35_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_35_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_35;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_35.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_35.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_35_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_35_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_35_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_35_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_35_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_35_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_35_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_35_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_35_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_35_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_35;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_35.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_35.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_35_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_35_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_35_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_35_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_35_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_35_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_35_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_35_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_35_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_35_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_35;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_35.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_35.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_35_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_35_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_35_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_35_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_35_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_35_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_35_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_35_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_35_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_35_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_35;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_35.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_35.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_35_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_35_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_35_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_35_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_35_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_35_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_35_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_35_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_35_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_35_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_35;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_35.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_35.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_35 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_35;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_35;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_35_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_35_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_35_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_35_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_35_inst_4.BIST_SO;
   }

   // Memory block 36
   Instance memory_block_36_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_36_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_36_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_36_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_36_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_36_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_36_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_36_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_36_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_36_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_36;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_36.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_36.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_36_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_36_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_36_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_36_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_36_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_36_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_36_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_36_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_36_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_36_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_36;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_36.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_36.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_36_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_36_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_36_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_36_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_36_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_36_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_36_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_36_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_36_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_36_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_36;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_36.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_36.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_36_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_36_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_36_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_36_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_36_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_36_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_36_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_36_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_36_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_36_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_36;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_36.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_36.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_36_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_36_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_36_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_36_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_36_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_36_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_36_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_36_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_36_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_36_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_36;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_36.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_36.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_36 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_36;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_36;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_36_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_36_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_36_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_36_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_36_inst_4.BIST_SO;
   }

   // Memory block 37
   Instance memory_block_37_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_37_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_37_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_37_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_37_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_37_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_37_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_37_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_37_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_37_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_37;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_37.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_37.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_37_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_37_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_37_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_37_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_37_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_37_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_37_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_37_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_37_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_37_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_37;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_37.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_37.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_37_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_37_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_37_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_37_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_37_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_37_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_37_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_37_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_37_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_37_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_37;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_37.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_37.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_37_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_37_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_37_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_37_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_37_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_37_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_37_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_37_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_37_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_37_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_37;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_37.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_37.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_37_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_37_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_37_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_37_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_37_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_37_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_37_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_37_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_37_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_37_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_37;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_37.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_37.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_37 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_37;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_37;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_37_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_37_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_37_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_37_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_37_inst_4.BIST_SO;
   }

   // Memory block 38
   Instance memory_block_38_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_38_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_38_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_38_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_38_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_38_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_38_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_38_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_38_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_38_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_38;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_38.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_38.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_38_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_38_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_38_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_38_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_38_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_38_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_38_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_38_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_38_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_38_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_38;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_38.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_38.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_38_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_38_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_38_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_38_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_38_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_38_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_38_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_38_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_38_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_38_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_38;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_38.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_38.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_38_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_38_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_38_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_38_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_38_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_38_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_38_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_38_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_38_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_38_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_38;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_38.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_38.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_38_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_38_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_38_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_38_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_38_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_38_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_38_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_38_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_38_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_38_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_38;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_38.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_38.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_38 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_38;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_38;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_38_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_38_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_38_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_38_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_38_inst_4.BIST_SO;
   }

   // Memory block 39
   Instance memory_block_39_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_39_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_39_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_39_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_39_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_39_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_39_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_39_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_39_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_39_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_39;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_39.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_39.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_39_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_39_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_39_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_39_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_39_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_39_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_39_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_39_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_39_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_39_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_39;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_39.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_39.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_39_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_39_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_39_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_39_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_39_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_39_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_39_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_39_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_39_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_39_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_39;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_39.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_39.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_39_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_39_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_39_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_39_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_39_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_39_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_39_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_39_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_39_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_39_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_39;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_39.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_39.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_39_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_39_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_39_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_39_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_39_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_39_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_39_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_39_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_39_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_39_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_39;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_39.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_39.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_39 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_39;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_39;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_39_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_39_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_39_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_39_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_39_inst_4.BIST_SO;
   }

   // Memory block 40
   Instance memory_block_40_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_40_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_40_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_40_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_40_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_40_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_40_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_40_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_40_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_40_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_40;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_40.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_40.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_40_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_40_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_40_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_40_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_40_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_40_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_40_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_40_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_40_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_40_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_40;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_40.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_40.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_40_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_40_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_40_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_40_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_40_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_40_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_40_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_40_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_40_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_40_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_40;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_40.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_40.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_40_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_40_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_40_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_40_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_40_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_40_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_40_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_40_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_40_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_40_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_40;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_40.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_40.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_40_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_40_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_40_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_40_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_40_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_40_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_40_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_40_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_40_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_40_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_40;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_40.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_40.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_40 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_40;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_40;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_40_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_40_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_40_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_40_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_40_inst_4.BIST_SO;
   }

   // Memory block 41
   Instance memory_block_41_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_41_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_41_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_41_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_41_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_41_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_41_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_41_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_41_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_41_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_41;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_41.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_41.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_41_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_41_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_41_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_41_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_41_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_41_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_41_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_41_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_41_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_41_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_41;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_41.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_41.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_41_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_41_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_41_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_41_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_41_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_41_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_41_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_41_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_41_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_41_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_41;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_41.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_41.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_41_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_41_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_41_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_41_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_41_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_41_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_41_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_41_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_41_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_41_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_41;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_41.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_41.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_41_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_41_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_41_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_41_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_41_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_41_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_41_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_41_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_41_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_41_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_41;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_41.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_41.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_41 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_41;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_41;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_41_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_41_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_41_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_41_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_41_inst_4.BIST_SO;
   }

   // Memory block 42
   Instance memory_block_42_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_42_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_42_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_42_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_42_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_42_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_42_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_42_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_42_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_42_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_42;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_42.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_42.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_42_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_42_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_42_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_42_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_42_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_42_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_42_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_42_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_42_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_42_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_42;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_42.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_42.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_42_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_42_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_42_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_42_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_42_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_42_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_42_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_42_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_42_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_42_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_42;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_42.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_42.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_42_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_42_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_42_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_42_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_42_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_42_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_42_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_42_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_42_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_42_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_42;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_42.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_42.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_42_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_42_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_42_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_42_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_42_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_42_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_42_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_42_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_42_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_42_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_42;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_42.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_42.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_42 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_42;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_42;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_42_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_42_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_42_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_42_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_42_inst_4.BIST_SO;
   }

   // Memory block 43
   Instance memory_block_43_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_43_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_43_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_43_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_43_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_43_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_43_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_43_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_43_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_43_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_43;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_43.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_43.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_43_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_43_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_43_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_43_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_43_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_43_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_43_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_43_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_43_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_43_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_43;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_43.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_43.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_43_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_43_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_43_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_43_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_43_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_43_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_43_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_43_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_43_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_43_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_43;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_43.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_43.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_43_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_43_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_43_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_43_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_43_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_43_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_43_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_43_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_43_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_43_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_43;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_43.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_43.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_43_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_43_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_43_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_43_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_43_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_43_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_43_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_43_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_43_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_43_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_43;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_43.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_43.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_43 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_43;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_43;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_43_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_43_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_43_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_43_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_43_inst_4.BIST_SO;
   }

   // Memory block 44
   Instance memory_block_44_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_44_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_44_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_44_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_44_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_44_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_44_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_44_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_44_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_44_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_44;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_44.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_44.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_44_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_44_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_44_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_44_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_44_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_44_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_44_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_44_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_44_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_44_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_44;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_44.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_44.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_44_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_44_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_44_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_44_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_44_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_44_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_44_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_44_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_44_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_44_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_44;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_44.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_44.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_44_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_44_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_44_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_44_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_44_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_44_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_44_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_44_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_44_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_44_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_44;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_44.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_44.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_44_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_44_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_44_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_44_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_44_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_44_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_44_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_44_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_44_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_44_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_44;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_44.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_44.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_44 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_44;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_44;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_44_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_44_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_44_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_44_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_44_inst_4.BIST_SO;
   }

   // Memory block 45
   Instance memory_block_45_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_45_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_45_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_45_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_45_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_45_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_45_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_45_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_45_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_45_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_45;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_45.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_45.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_45_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_45_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_45_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_45_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_45_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_45_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_45_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_45_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_45_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_45_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_45;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_45.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_45.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_45_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_45_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_45_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_45_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_45_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_45_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_45_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_45_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_45_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_45_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_45;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_45.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_45.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_45_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_45_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_45_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_45_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_45_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_45_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_45_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_45_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_45_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_45_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_45;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_45.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_45.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_45_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_45_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_45_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_45_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_45_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_45_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_45_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_45_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_45_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_45_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_45;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_45.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_45.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_45 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_45;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_45;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_45_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_45_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_45_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_45_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_45_inst_4.BIST_SO;
   }

   // Memory block 46
   Instance memory_block_46_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_46_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_46_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_46_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_46_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_46_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_46_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_46_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_46_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_46_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_46;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_46.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_46.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_46_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_46_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_46_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_46_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_46_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_46_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_46_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_46_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_46_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_46_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_46;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_46.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_46.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_46_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_46_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_46_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_46_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_46_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_46_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_46_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_46_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_46_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_46_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_46;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_46.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_46.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_46_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_46_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_46_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_46_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_46_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_46_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_46_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_46_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_46_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_46_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_46;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_46.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_46.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_46_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_46_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_46_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_46_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_46_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_46_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_46_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_46_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_46_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_46_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_46;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_46.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_46.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_46 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_46;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_46;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_46_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_46_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_46_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_46_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_46_inst_4.BIST_SO;
   }

   // Memory block 47
   Instance memory_block_47_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_47_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_47_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_47_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_47_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_47_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_47_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_47_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_47_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_47_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_47;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_47.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_47.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_47_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_47_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_47_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_47_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_47_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_47_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_47_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_47_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_47_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_47_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_47;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_47.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_47.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_47_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_47_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_47_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_47_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_47_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_47_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_47_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_47_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_47_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_47_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_47;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_47.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_47.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_47_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_47_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_47_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_47_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_47_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_47_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_47_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_47_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_47_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_47_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_47;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_47.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_47.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_47_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_47_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_47_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_47_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_47_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_47_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_47_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_47_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_47_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_47_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_47;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_47.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_47.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_47 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_47;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_47;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_47_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_47_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_47_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_47_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_47_inst_4.BIST_SO;
   }

   // Memory block 48
   Instance memory_block_48_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_48_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_48_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_48_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_48_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_48_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_48_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_48_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_48_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_48_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_48;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_48.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_48.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_48_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_48_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_48_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_48_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_48_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_48_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_48_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_48_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_48_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_48_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_48;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_48.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_48.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_48_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_48_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_48_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_48_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_48_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_48_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_48_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_48_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_48_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_48_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_48;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_48.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_48.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_48_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_48_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_48_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_48_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_48_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_48_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_48_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_48_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_48_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_48_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_48;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_48.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_48.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_48_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_48_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_48_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_48_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_48_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_48_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_48_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_48_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_48_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_48_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_48;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_48.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_48.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_48 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_48;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_48;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_48_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_48_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_48_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_48_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_48_inst_4.BIST_SO;
   }

   // Memory block 49
   Instance memory_block_49_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_49_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_49_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_49_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_49_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_49_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_49_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_49_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_49_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_49_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_49;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_49.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_49.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_49_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_49_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_49_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_49_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_49_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_49_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_49_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_49_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_49_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_49_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_49;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_49.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_49.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_49_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_49_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_49_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_49_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_49_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_49_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_49_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_49_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_49_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_49_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_49;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_49.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_49.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_49_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_49_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_49_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_49_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_49_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_49_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_49_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_49_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_49_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_49_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_49;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_49.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_49.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_49_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_49_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_49_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_49_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_49_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_49_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_49_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_49_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_49_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_49_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_49;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_49.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_49.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_49 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_49;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_49;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_49_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_49_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_49_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_49_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_49_inst_4.BIST_SO;
   }

   // Memory block 50
   Instance memory_block_50_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_50_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_50_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_50_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_50_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_50_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_50_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_50_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_50_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_50_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_50;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_50.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_50.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_50_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_50_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_50_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_50_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_50_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_50_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_50_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_50_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_50_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_50_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_50;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_50.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_50.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_50_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_50_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_50_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_50_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_50_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_50_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_50_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_50_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_50_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_50_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_50;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_50.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_50.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_50_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_50_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_50_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_50_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_50_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_50_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_50_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_50_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_50_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_50_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_50;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_50.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_50.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_50_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_50_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_50_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_50_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_50_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_50_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_50_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_50_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_50_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_50_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_50;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_50.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_50.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_50 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_50;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_50;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_50_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_50_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_50_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_50_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_50_inst_4.BIST_SO;
   }

   // Memory block 51
   Instance memory_block_51_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_51_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_51_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_51_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_51_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_51_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_51_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_51_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_51_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_51_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_51;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_51.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_51.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_51_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_51_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_51_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_51_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_51_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_51_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_51_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_51_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_51_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_51_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_51;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_51.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_51.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_51_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_51_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_51_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_51_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_51_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_51_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_51_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_51_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_51_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_51_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_51;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_51.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_51.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_51_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_51_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_51_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_51_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_51_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_51_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_51_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_51_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_51_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_51_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_51;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_51.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_51.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_51_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_51_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_51_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_51_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_51_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_51_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_51_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_51_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_51_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_51_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_51;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_51.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_51.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_51 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_51;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_51;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_51_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_51_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_51_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_51_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_51_inst_4.BIST_SO;
   }

   // Memory block 52
   Instance memory_block_52_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_52_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_52_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_52_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_52_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_52_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_52_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_52_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_52_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_52_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_52;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_52.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_52.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_52_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_52_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_52_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_52_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_52_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_52_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_52_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_52_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_52_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_52_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_52;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_52.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_52.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_52_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_52_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_52_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_52_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_52_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_52_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_52_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_52_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_52_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_52_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_52;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_52.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_52.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_52_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_52_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_52_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_52_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_52_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_52_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_52_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_52_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_52_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_52_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_52;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_52.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_52.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_52_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_52_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_52_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_52_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_52_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_52_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_52_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_52_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_52_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_52_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_52;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_52.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_52.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_52 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_52;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_52;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_52_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_52_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_52_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_52_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_52_inst_4.BIST_SO;
   }

   // Memory block 53
   Instance memory_block_53_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_53_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_53_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_53_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_53_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_53_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_53_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_53_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_53_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_53_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_53;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_53.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_53.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_53_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_53_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_53_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_53_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_53_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_53_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_53_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_53_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_53_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_53_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_53;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_53.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_53.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_53_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_53_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_53_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_53_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_53_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_53_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_53_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_53_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_53_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_53_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_53;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_53.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_53.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_53_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_53_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_53_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_53_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_53_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_53_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_53_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_53_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_53_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_53_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_53;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_53.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_53.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_53_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_53_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_53_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_53_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_53_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_53_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_53_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_53_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_53_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_53_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_53;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_53.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_53.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_53 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_53;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_53;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_53_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_53_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_53_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_53_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_53_inst_4.BIST_SO;
   }

   // Memory block 54
   Instance memory_block_54_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_54_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_54_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_54_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_54_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_54_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_54_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_54_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_54_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_54_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_54;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_54.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_54.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_54_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_54_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_54_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_54_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_54_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_54_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_54_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_54_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_54_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_54_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_54;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_54.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_54.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_54_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_54_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_54_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_54_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_54_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_54_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_54_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_54_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_54_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_54_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_54;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_54.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_54.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_54_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_54_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_54_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_54_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_54_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_54_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_54_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_54_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_54_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_54_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_54;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_54.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_54.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_54_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_54_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_54_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_54_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_54_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_54_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_54_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_54_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_54_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_54_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_54;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_54.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_54.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_54 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_54;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_54;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_54_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_54_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_54_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_54_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_54_inst_4.BIST_SO;
   }

   // Memory block 55
   Instance memory_block_55_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_55_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_55_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_55_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_55_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_55_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_55_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_55_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_55_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_55_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_55;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_55.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_55.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_55_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_55_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_55_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_55_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_55_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_55_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_55_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_55_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_55_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_55_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_55;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_55.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_55.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_55_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_55_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_55_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_55_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_55_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_55_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_55_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_55_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_55_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_55_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_55;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_55.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_55.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_55_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_55_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_55_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_55_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_55_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_55_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_55_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_55_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_55_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_55_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_55;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_55.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_55.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_55_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_55_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_55_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_55_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_55_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_55_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_55_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_55_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_55_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_55_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_55;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_55.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_55.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_55 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_55;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_55;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_55_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_55_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_55_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_55_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_55_inst_4.BIST_SO;
   }

   // Memory block 56
   Instance memory_block_56_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_56_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_56_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_56_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_56_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_56_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_56_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_56_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_56_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_56_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_56;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_56.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_56.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_56_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_56_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_56_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_56_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_56_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_56_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_56_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_56_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_56_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_56_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_56;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_56.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_56.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_56_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_56_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_56_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_56_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_56_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_56_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_56_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_56_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_56_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_56_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_56;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_56.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_56.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_56_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_56_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_56_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_56_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_56_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_56_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_56_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_56_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_56_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_56_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_56;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_56.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_56.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_56_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_56_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_56_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_56_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_56_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_56_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_56_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_56_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_56_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_56_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_56;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_56.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_56.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_56 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_56;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_56;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_56_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_56_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_56_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_56_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_56_inst_4.BIST_SO;
   }

   // Memory block 57
   Instance memory_block_57_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_57_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_57_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_57_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_57_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_57_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_57_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_57_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_57_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_57_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_57;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_57.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_57.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_57_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_57_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_57_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_57_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_57_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_57_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_57_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_57_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_57_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_57_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_57;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_57.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_57.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_57_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_57_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_57_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_57_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_57_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_57_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_57_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_57_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_57_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_57_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_57;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_57.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_57.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_57_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_57_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_57_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_57_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_57_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_57_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_57_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_57_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_57_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_57_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_57;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_57.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_57.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_57_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_57_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_57_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_57_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_57_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_57_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_57_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_57_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_57_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_57_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_57;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_57.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_57.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_57 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_57;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_57;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_57_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_57_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_57_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_57_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_57_inst_4.BIST_SO;
   }

   // Memory block 58
   Instance memory_block_58_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_58_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_58_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_58_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_58_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_58_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_58_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_58_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_58_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_58_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_58;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_58.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_58.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_58_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_58_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_58_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_58_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_58_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_58_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_58_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_58_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_58_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_58_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_58;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_58.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_58.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_58_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_58_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_58_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_58_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_58_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_58_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_58_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_58_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_58_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_58_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_58;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_58.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_58.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_58_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_58_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_58_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_58_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_58_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_58_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_58_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_58_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_58_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_58_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_58;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_58.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_58.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_58_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_58_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_58_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_58_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_58_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_58_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_58_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_58_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_58_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_58_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_58;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_58.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_58.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_58 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_58;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_58;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_58_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_58_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_58_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_58_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_58_inst_4.BIST_SO;
   }

   // Memory block 59
   Instance memory_block_59_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_59_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_59_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_59_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_59_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_59_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_59_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_59_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_59_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_59_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_59;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_59.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_59.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_59_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_59_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_59_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_59_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_59_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_59_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_59_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_59_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_59_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_59_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_59;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_59.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_59.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_59_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_59_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_59_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_59_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_59_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_59_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_59_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_59_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_59_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_59_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_59;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_59.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_59.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_59_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_59_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_59_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_59_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_59_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_59_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_59_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_59_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_59_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_59_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_59;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_59.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_59.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_59_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_59_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_59_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_59_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_59_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_59_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_59_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_59_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_59_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_59_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_59;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_59.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_59.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_59 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_59;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_59;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_59_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_59_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_59_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_59_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_59_inst_4.BIST_SO;
   }

   // Memory block 60
   Instance memory_block_60_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_60_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_60_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_60_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_60_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_60_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_60_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_60_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_60_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_60_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_60;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_60.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_60.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_60_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_60_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_60_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_60_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_60_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_60_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_60_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_60_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_60_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_60_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_60;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_60.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_60.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_60_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_60_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_60_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_60_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_60_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_60_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_60_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_60_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_60_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_60_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_60;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_60.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_60.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_60_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_60_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_60_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_60_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_60_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_60_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_60_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_60_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_60_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_60_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_60;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_60.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_60.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_60_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_60_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_60_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_60_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_60_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_60_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_60_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_60_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_60_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_60_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_60;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_60.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_60.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_60 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_60;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_60;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_60_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_60_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_60_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_60_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_60_inst_4.BIST_SO;
   }

   // Memory block 61
   Instance memory_block_61_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_61_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_61_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_61_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_61_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_61_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_61_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_61_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_61_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_61_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_61;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_61.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_61.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_61_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_61_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_61_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_61_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_61_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_61_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_61_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_61_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_61_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_61_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_61;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_61.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_61.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_61_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_61_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_61_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_61_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_61_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_61_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_61_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_61_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_61_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_61_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_61;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_61.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_61.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_61_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_61_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_61_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_61_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_61_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_61_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_61_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_61_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_61_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_61_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_61;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_61.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_61.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_61_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_61_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_61_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_61_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_61_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_61_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_61_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_61_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_61_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_61_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_61;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_61.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_61.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_61 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_61;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_61;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_61_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_61_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_61_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_61_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_61_inst_4.BIST_SO;
   }

   // Memory block 62
   Instance memory_block_62_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_62_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_62_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_62_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_62_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_62_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_62_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_62_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_62_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_62_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_62;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_62.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_62.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_62_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_62_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_62_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_62_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_62_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_62_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_62_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_62_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_62_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_62_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_62;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_62.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_62.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_62_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_62_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_62_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_62_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_62_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_62_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_62_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_62_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_62_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_62_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_62;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_62.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_62.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_62_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_62_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_62_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_62_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_62_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_62_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_62_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_62_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_62_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_62_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_62;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_62.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_62.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_62_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_62_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_62_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_62_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_62_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_62_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_62_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_62_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_62_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_62_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_62;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_62.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_62.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_62 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_62;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_62;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_62_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_62_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_62_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_62_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_62_inst_4.BIST_SO;
   }

   // Memory block 63
   Instance memory_block_63_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_63_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_63_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_63_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_63_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_63_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_63_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_63_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_63_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_63_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_63;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_63.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_63.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_63_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_63_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_63_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_63_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_63_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_63_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_63_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_63_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_63_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_63_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_63;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_63.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_63.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_63_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_63_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_63_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_63_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_63_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_63_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_63_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_63_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_63_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_63_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_63;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_63.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_63.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_63_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_63_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_63_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_63_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_63_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_63_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_63_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_63_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_63_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_63_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_63;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_63.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_63.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_63_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_63_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_63_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_63_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_63_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_63_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_63_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_63_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_63_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_63_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_63;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_63.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_63.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_63 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_63;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_63;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_63_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_63_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_63_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_63_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_63_inst_4.BIST_SO;
   }

   // Memory block 64
   Instance memory_block_64_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_64_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_64_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_64_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_64_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_64_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_64_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_64_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_64_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_64_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_64;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_64.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_64.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_64_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_64_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_64_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_64_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_64_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_64_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_64_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_64_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_64_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_64_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_64;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_64.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_64.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_64_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_64_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_64_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_64_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_64_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_64_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_64_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_64_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_64_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_64_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_64;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_64.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_64.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_64_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_64_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_64_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_64_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_64_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_64_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_64_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_64_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_64_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_64_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_64;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_64.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_64.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_64_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_64_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_64_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_64_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_64_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_64_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_64_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_64_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_64_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_64_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_64;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_64.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_64.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_64 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_64;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_64;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_64_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_64_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_64_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_64_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_64_inst_4.BIST_SO;
   }

   // Memory block 65
   Instance memory_block_65_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_65_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_65_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_65_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_65_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_65_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_65_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_65_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_65_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_65_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_65;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_65.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_65.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_65_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_65_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_65_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_65_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_65_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_65_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_65_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_65_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_65_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_65_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_65;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_65.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_65.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_65_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_65_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_65_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_65_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_65_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_65_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_65_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_65_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_65_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_65_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_65;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_65.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_65.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_65_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_65_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_65_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_65_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_65_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_65_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_65_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_65_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_65_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_65_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_65;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_65.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_65.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_65_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_65_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_65_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_65_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_65_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_65_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_65_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_65_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_65_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_65_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_65;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_65.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_65.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_65 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_65;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_65;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_65_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_65_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_65_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_65_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_65_inst_4.BIST_SO;
   }

   // Memory block 66
   Instance memory_block_66_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_66_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_66_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_66_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_66_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_66_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_66_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_66_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_66_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_66_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_66;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_66.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_66.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_66_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_66_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_66_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_66_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_66_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_66_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_66_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_66_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_66_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_66_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_66;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_66.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_66.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_66_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_66_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_66_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_66_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_66_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_66_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_66_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_66_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_66_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_66_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_66;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_66.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_66.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_66_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_66_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_66_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_66_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_66_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_66_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_66_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_66_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_66_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_66_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_66;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_66.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_66.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_66_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_66_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_66_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_66_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_66_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_66_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_66_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_66_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_66_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_66_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_66;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_66.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_66.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_66 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_66;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_66;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_66_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_66_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_66_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_66_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_66_inst_4.BIST_SO;
   }

   // Memory block 67
   Instance memory_block_67_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_67_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_67_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_67_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_67_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_67_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_67_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_67_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_67_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_67_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_67;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_67.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_67.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_67_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_67_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_67_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_67_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_67_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_67_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_67_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_67_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_67_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_67_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_67;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_67.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_67.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_67_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_67_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_67_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_67_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_67_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_67_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_67_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_67_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_67_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_67_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_67;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_67.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_67.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_67_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_67_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_67_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_67_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_67_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_67_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_67_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_67_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_67_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_67_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_67;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_67.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_67.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_67_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_67_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_67_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_67_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_67_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_67_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_67_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_67_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_67_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_67_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_67;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_67.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_67.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_67 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_67;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_67;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_67_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_67_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_67_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_67_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_67_inst_4.BIST_SO;
   }

   // Memory block 68
   Instance memory_block_68_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_68_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_68_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_68_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_68_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_68_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_68_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_68_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_68_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_68_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_68;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_68.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_68.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_68_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_68_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_68_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_68_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_68_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_68_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_68_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_68_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_68_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_68_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_68;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_68.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_68.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_68_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_68_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_68_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_68_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_68_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_68_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_68_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_68_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_68_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_68_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_68;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_68.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_68.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_68_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_68_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_68_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_68_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_68_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_68_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_68_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_68_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_68_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_68_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_68;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_68.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_68.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_68_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_68_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_68_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_68_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_68_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_68_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_68_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_68_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_68_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_68_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_68;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_68.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_68.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_68 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_68;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_68;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_68_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_68_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_68_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_68_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_68_inst_4.BIST_SO;
   }

   // Memory block 69
   Instance memory_block_69_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_69_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_69_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_69_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_69_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_69_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_69_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_69_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_69_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_69_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_69;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_69.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_69.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_69_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_69_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_69_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_69_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_69_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_69_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_69_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_69_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_69_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_69_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_69;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_69.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_69.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_69_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_69_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_69_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_69_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_69_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_69_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_69_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_69_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_69_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_69_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_69;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_69.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_69.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_69_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_69_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_69_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_69_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_69_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_69_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_69_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_69_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_69_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_69_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_69;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_69.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_69.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_69_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_69_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_69_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_69_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_69_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_69_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_69_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_69_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_69_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_69_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_69;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_69.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_69.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_69 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_69;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_69;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_69_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_69_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_69_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_69_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_69_inst_4.BIST_SO;
   }

   // Memory block 70
   Instance memory_block_70_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_70_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_70_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_70_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_70_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_70_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_70_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_70_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_70_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_70_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_70;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_70.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_70.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_70_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_70_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_70_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_70_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_70_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_70_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_70_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_70_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_70_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_70_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_70;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_70.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_70.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_70_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_70_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_70_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_70_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_70_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_70_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_70_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_70_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_70_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_70_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_70;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_70.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_70.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_70_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_70_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_70_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_70_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_70_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_70_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_70_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_70_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_70_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_70_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_70;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_70.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_70.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_70_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_70_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_70_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_70_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_70_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_70_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_70_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_70_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_70_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_70_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_70;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_70.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_70.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_70 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_70;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_70;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_70_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_70_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_70_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_70_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_70_inst_4.BIST_SO;
   }

   // Memory block 71
   Instance memory_block_71_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_71_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_71_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_71_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_71_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_71_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_71_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_71_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_71_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_71_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_71;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_71.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_71.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_71_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_71_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_71_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_71_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_71_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_71_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_71_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_71_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_71_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_71_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_71;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_71.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_71.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_71_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_71_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_71_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_71_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_71_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_71_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_71_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_71_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_71_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_71_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_71;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_71.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_71.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_71_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_71_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_71_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_71_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_71_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_71_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_71_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_71_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_71_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_71_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_71;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_71.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_71.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_71_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_71_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_71_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_71_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_71_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_71_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_71_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_71_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_71_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_71_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_71;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_71.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_71.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_71 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_71;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_71;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_71_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_71_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_71_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_71_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_71_inst_4.BIST_SO;
   }

   // Memory block 72
   Instance memory_block_72_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_72_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_72_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_72_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_72_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_72_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_72_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_72_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_72_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_72_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_72;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_72.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_72.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_72_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_72_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_72_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_72_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_72_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_72_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_72_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_72_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_72_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_72_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_72;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_72.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_72.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_72_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_72_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_72_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_72_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_72_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_72_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_72_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_72_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_72_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_72_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_72;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_72.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_72.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_72_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_72_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_72_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_72_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_72_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_72_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_72_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_72_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_72_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_72_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_72;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_72.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_72.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_72_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_72_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_72_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_72_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_72_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_72_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_72_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_72_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_72_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_72_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_72;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_72.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_72.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_72 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_72;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_72;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_72_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_72_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_72_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_72_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_72_inst_4.BIST_SO;
   }

   // Memory block 73
   Instance memory_block_73_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_73_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_73_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_73_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_73_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_73_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_73_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_73_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_73_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_73_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_73;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_73.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_73.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_73_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_73_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_73_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_73_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_73_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_73_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_73_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_73_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_73_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_73_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_73;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_73.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_73.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_73_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_73_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_73_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_73_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_73_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_73_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_73_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_73_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_73_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_73_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_73;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_73.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_73.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_73_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_73_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_73_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_73_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_73_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_73_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_73_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_73_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_73_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_73_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_73;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_73.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_73.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_73_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_73_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_73_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_73_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_73_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_73_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_73_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_73_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_73_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_73_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_73;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_73.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_73.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_73 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_73;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_73;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_73_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_73_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_73_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_73_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_73_inst_4.BIST_SO;
   }

   // Memory block 74
   Instance memory_block_74_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_74_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_74_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_74_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_74_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_74_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_74_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_74_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_74_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_74_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_74;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_74.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_74.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_74_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_74_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_74_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_74_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_74_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_74_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_74_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_74_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_74_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_74_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_74;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_74.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_74.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_74_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_74_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_74_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_74_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_74_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_74_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_74_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_74_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_74_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_74_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_74;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_74.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_74.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_74_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_74_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_74_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_74_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_74_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_74_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_74_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_74_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_74_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_74_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_74;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_74.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_74.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_74_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_74_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_74_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_74_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_74_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_74_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_74_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_74_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_74_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_74_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_74;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_74.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_74.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_74 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_74;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_74;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_74_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_74_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_74_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_74_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_74_inst_4.BIST_SO;
   }

   // Memory block 75
   Instance memory_block_75_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_75_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_75_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_75_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_75_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_75_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_75_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_75_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_75_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_75_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_75;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_75.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_75.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_75_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_75_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_75_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_75_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_75_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_75_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_75_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_75_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_75_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_75_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_75;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_75.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_75.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_75_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_75_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_75_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_75_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_75_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_75_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_75_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_75_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_75_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_75_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_75;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_75.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_75.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_75_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_75_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_75_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_75_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_75_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_75_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_75_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_75_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_75_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_75_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_75;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_75.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_75.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_75_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_75_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_75_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_75_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_75_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_75_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_75_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_75_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_75_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_75_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_75;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_75.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_75.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_75 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_75;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_75;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_75_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_75_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_75_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_75_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_75_inst_4.BIST_SO;
   }

   // Memory block 76
   Instance memory_block_76_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_76_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_76_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_76_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_76_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_76_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_76_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_76_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_76_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_76_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_76;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_76.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_76.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_76_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_76_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_76_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_76_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_76_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_76_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_76_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_76_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_76_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_76_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_76;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_76.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_76.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_76_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_76_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_76_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_76_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_76_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_76_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_76_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_76_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_76_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_76_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_76;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_76.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_76.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_76_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_76_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_76_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_76_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_76_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_76_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_76_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_76_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_76_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_76_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_76;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_76.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_76.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_76_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_76_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_76_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_76_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_76_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_76_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_76_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_76_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_76_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_76_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_76;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_76.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_76.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_76 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_76;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_76;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_76_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_76_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_76_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_76_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_76_inst_4.BIST_SO;
   }

   // Memory block 77
   Instance memory_block_77_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_77_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_77_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_77_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_77_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_77_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_77_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_77_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_77_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_77_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_77;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_77.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_77.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_77_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_77_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_77_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_77_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_77_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_77_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_77_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_77_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_77_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_77_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_77;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_77.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_77.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_77_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_77_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_77_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_77_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_77_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_77_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_77_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_77_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_77_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_77_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_77;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_77.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_77.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_77_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_77_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_77_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_77_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_77_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_77_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_77_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_77_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_77_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_77_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_77;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_77.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_77.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_77_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_77_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_77_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_77_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_77_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_77_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_77_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_77_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_77_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_77_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_77;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_77.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_77.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_77 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_77;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_77;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_77_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_77_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_77_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_77_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_77_inst_4.BIST_SO;
   }

   // Memory block 78
   Instance memory_block_78_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_78_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_78_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_78_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_78_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_78_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_78_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_78_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_78_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_78_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_78;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_78.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_78.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_78_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_78_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_78_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_78_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_78_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_78_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_78_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_78_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_78_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_78_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_78;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_78.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_78.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_78_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_78_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_78_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_78_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_78_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_78_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_78_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_78_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_78_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_78_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_78;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_78.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_78.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_78_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_78_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_78_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_78_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_78_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_78_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_78_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_78_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_78_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_78_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_78;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_78.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_78.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_78_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_78_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_78_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_78_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_78_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_78_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_78_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_78_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_78_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_78_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_78;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_78.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_78.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_78 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_78;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_78;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_78_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_78_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_78_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_78_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_78_inst_4.BIST_SO;
   }

   // Memory block 79
   Instance memory_block_79_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_79_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_79_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_79_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_79_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_79_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_79_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_79_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_79_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_79_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_79;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_79.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_79.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_79_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_79_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_79_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_79_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_79_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_79_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_79_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_79_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_79_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_79_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_79;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_79.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_79.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_79_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_79_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_79_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_79_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_79_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_79_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_79_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_79_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_79_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_79_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_79;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_79.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_79.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_79_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_79_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_79_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_79_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_79_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_79_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_79_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_79_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_79_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_79_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_79;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_79.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_79.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_79_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_79_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_79_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_79_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_79_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_79_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_79_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_79_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_79_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_79_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_79;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_79.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_79.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_79 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_79;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_79;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_79_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_79_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_79_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_79_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_79_inst_4.BIST_SO;
   }

   // Memory block 80
   Instance memory_block_80_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_80_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_80_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_80_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_80_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_80_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_80_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_80_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_80_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_80_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_80;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_80.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_80.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_80_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_80_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_80_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_80_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_80_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_80_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_80_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_80_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_80_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_80_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_80;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_80.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_80.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_80_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_80_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_80_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_80_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_80_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_80_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_80_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_80_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_80_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_80_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_80;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_80.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_80.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_80_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_80_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_80_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_80_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_80_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_80_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_80_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_80_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_80_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_80_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_80;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_80.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_80.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_80_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_80_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_80_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_80_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_80_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_80_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_80_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_80_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_80_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_80_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_80;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_80.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_80.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_80 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_80;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_80;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_80_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_80_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_80_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_80_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_80_inst_4.BIST_SO;
   }

   // Memory block 81
   Instance memory_block_81_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_81_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_81_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_81_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_81_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_81_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_81_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_81_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_81_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_81_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_81;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_81.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_81.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_81_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_81_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_81_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_81_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_81_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_81_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_81_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_81_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_81_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_81_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_81;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_81.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_81.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_81_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_81_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_81_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_81_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_81_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_81_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_81_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_81_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_81_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_81_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_81;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_81.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_81.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_81_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_81_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_81_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_81_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_81_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_81_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_81_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_81_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_81_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_81_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_81;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_81.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_81.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_81_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_81_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_81_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_81_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_81_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_81_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_81_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_81_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_81_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_81_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_81;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_81.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_81.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_81 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_81;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_81;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_81_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_81_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_81_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_81_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_81_inst_4.BIST_SO;
   }

   // Memory block 82
   Instance memory_block_82_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_82_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_82_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_82_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_82_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_82_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_82_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_82_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_82_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_82_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_82;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_82.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_82.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_82_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_82_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_82_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_82_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_82_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_82_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_82_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_82_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_82_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_82_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_82;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_82.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_82.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_82_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_82_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_82_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_82_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_82_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_82_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_82_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_82_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_82_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_82_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_82;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_82.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_82.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_82_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_82_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_82_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_82_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_82_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_82_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_82_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_82_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_82_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_82_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_82;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_82.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_82.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_82_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_82_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_82_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_82_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_82_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_82_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_82_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_82_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_82_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_82_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_82;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_82.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_82.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_82 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_82;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_82;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_82_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_82_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_82_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_82_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_82_inst_4.BIST_SO;
   }

   // Memory block 83
   Instance memory_block_83_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_83_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_83_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_83_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_83_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_83_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_83_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_83_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_83_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_83_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_83;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_83.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_83.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_83_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_83_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_83_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_83_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_83_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_83_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_83_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_83_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_83_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_83_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_83;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_83.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_83.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_83_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_83_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_83_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_83_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_83_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_83_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_83_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_83_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_83_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_83_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_83;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_83.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_83.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_83_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_83_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_83_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_83_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_83_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_83_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_83_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_83_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_83_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_83_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_83;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_83.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_83.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_83_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_83_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_83_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_83_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_83_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_83_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_83_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_83_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_83_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_83_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_83;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_83.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_83.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_83 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_83;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_83;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_83_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_83_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_83_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_83_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_83_inst_4.BIST_SO;
   }

   // Memory block 84
   Instance memory_block_84_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_84_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_84_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_84_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_84_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_84_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_84_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_84_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_84_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_84_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_84;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_84.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_84.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_84_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_84_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_84_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_84_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_84_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_84_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_84_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_84_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_84_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_84_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_84;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_84.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_84.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_84_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_84_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_84_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_84_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_84_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_84_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_84_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_84_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_84_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_84_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_84;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_84.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_84.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_84_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_84_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_84_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_84_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_84_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_84_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_84_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_84_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_84_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_84_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_84;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_84.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_84.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_84_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_84_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_84_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_84_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_84_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_84_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_84_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_84_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_84_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_84_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_84;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_84.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_84.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_84 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_84;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_84;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_84_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_84_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_84_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_84_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_84_inst_4.BIST_SO;
   }

   // Memory block 85
   Instance memory_block_85_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_85_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_85_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_85_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_85_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_85_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_85_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_85_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_85_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_85_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_85;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_85.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_85.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_85_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_85_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_85_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_85_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_85_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_85_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_85_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_85_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_85_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_85_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_85;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_85.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_85.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_85_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_85_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_85_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_85_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_85_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_85_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_85_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_85_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_85_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_85_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_85;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_85.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_85.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_85_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_85_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_85_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_85_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_85_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_85_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_85_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_85_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_85_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_85_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_85;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_85.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_85.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_85_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_85_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_85_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_85_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_85_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_85_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_85_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_85_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_85_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_85_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_85;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_85.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_85.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_85 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_85;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_85;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_85_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_85_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_85_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_85_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_85_inst_4.BIST_SO;
   }

   // Memory block 86
   Instance memory_block_86_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_86_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_86_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_86_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_86_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_86_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_86_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_86_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_86_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_86_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_86;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_86.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_86.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_86_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_86_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_86_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_86_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_86_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_86_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_86_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_86_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_86_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_86_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_86;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_86.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_86.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_86_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_86_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_86_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_86_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_86_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_86_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_86_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_86_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_86_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_86_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_86;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_86.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_86.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_86_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_86_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_86_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_86_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_86_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_86_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_86_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_86_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_86_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_86_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_86;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_86.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_86.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_86_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_86_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_86_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_86_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_86_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_86_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_86_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_86_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_86_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_86_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_86;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_86.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_86.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_86 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_86;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_86;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_86_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_86_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_86_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_86_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_86_inst_4.BIST_SO;
   }

   // Memory block 87
   Instance memory_block_87_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_87_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_87_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_87_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_87_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_87_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_87_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_87_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_87_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_87_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_87;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_87.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_87.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_87_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_87_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_87_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_87_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_87_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_87_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_87_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_87_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_87_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_87_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_87;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_87.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_87.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_87_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_87_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_87_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_87_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_87_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_87_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_87_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_87_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_87_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_87_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_87;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_87.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_87.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_87_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_87_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_87_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_87_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_87_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_87_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_87_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_87_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_87_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_87_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_87;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_87.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_87.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_87_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_87_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_87_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_87_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_87_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_87_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_87_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_87_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_87_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_87_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_87;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_87.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_87.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_87 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_87;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_87;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_87_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_87_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_87_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_87_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_87_inst_4.BIST_SO;
   }

   // Memory block 88
   Instance memory_block_88_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_88_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_88_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_88_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_88_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_88_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_88_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_88_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_88_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_88_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_88;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_88.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_88.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_88_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_88_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_88_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_88_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_88_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_88_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_88_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_88_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_88_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_88_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_88;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_88.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_88.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_88_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_88_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_88_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_88_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_88_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_88_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_88_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_88_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_88_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_88_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_88;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_88.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_88.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_88_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_88_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_88_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_88_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_88_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_88_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_88_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_88_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_88_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_88_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_88;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_88.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_88.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_88_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_88_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_88_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_88_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_88_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_88_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_88_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_88_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_88_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_88_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_88;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_88.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_88.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_88 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_88;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_88;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_88_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_88_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_88_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_88_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_88_inst_4.BIST_SO;
   }

   // Memory block 89
   Instance memory_block_89_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_89_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_89_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_89_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_89_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_89_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_89_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_89_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_89_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_89_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_89;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_89.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_89.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_89_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_89_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_89_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_89_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_89_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_89_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_89_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_89_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_89_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_89_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_89;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_89.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_89.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_89_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_89_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_89_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_89_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_89_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_89_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_89_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_89_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_89_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_89_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_89;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_89.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_89.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_89_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_89_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_89_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_89_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_89_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_89_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_89_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_89_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_89_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_89_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_89;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_89.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_89.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_89_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_89_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_89_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_89_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_89_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_89_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_89_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_89_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_89_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_89_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_89;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_89.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_89.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_89 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_89;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_89;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_89_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_89_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_89_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_89_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_89_inst_4.BIST_SO;
   }

   // Memory block 90
   Instance memory_block_90_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_90_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_90_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_90_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_90_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_90_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_90_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_90_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_90_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_90_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_90;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_90.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_90.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_90_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_90_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_90_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_90_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_90_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_90_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_90_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_90_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_90_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_90_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_90;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_90.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_90.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_90_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_90_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_90_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_90_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_90_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_90_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_90_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_90_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_90_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_90_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_90;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_90.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_90.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_90_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_90_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_90_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_90_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_90_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_90_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_90_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_90_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_90_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_90_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_90;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_90.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_90.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_90_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_90_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_90_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_90_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_90_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_90_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_90_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_90_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_90_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_90_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_90;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_90.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_90.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_90 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_90;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_90;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_90_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_90_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_90_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_90_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_90_inst_4.BIST_SO;
   }

   // Memory block 91
   Instance memory_block_91_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_91_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_91_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_91_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_91_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_91_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_91_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_91_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_91_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_91_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_91;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_91.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_91.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_91_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_91_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_91_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_91_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_91_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_91_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_91_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_91_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_91_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_91_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_91;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_91.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_91.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_91_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_91_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_91_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_91_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_91_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_91_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_91_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_91_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_91_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_91_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_91;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_91.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_91.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_91_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_91_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_91_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_91_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_91_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_91_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_91_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_91_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_91_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_91_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_91;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_91.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_91.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_91_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_91_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_91_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_91_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_91_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_91_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_91_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_91_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_91_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_91_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_91;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_91.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_91.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_91 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_91;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_91;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_91_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_91_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_91_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_91_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_91_inst_4.BIST_SO;
   }

   // Memory block 92
   Instance memory_block_92_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_92_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_92_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_92_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_92_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_92_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_92_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_92_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_92_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_92_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_92;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_92.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_92.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_92_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_92_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_92_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_92_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_92_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_92_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_92_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_92_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_92_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_92_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_92;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_92.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_92.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_92_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_92_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_92_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_92_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_92_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_92_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_92_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_92_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_92_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_92_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_92;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_92.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_92.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_92_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_92_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_92_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_92_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_92_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_92_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_92_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_92_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_92_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_92_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_92;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_92.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_92.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_92_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_92_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_92_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_92_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_92_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_92_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_92_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_92_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_92_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_92_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_92;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_92.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_92.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_92 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_92;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_92;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_92_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_92_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_92_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_92_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_92_inst_4.BIST_SO;
   }

   // Memory block 93
   Instance memory_block_93_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_93_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_93_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_93_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_93_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_93_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_93_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_93_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_93_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_93_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_93;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_93.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_93.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_93_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_93_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_93_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_93_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_93_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_93_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_93_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_93_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_93_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_93_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_93;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_93.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_93.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_93_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_93_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_93_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_93_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_93_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_93_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_93_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_93_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_93_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_93_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_93;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_93.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_93.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_93_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_93_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_93_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_93_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_93_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_93_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_93_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_93_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_93_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_93_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_93;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_93.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_93.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_93_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_93_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_93_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_93_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_93_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_93_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_93_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_93_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_93_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_93_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_93;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_93.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_93.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_93 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_93;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_93;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_93_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_93_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_93_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_93_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_93_inst_4.BIST_SO;
   }

   // Memory block 94
   Instance memory_block_94_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_94_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_94_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_94_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_94_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_94_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_94_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_94_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_94_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_94_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_94;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_94.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_94.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_94_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_94_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_94_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_94_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_94_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_94_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_94_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_94_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_94_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_94_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_94;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_94.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_94.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_94_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_94_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_94_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_94_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_94_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_94_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_94_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_94_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_94_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_94_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_94;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_94.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_94.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_94_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_94_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_94_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_94_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_94_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_94_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_94_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_94_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_94_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_94_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_94;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_94.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_94.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_94_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_94_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_94_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_94_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_94_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_94_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_94_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_94_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_94_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_94_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_94;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_94.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_94.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_94 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_94;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_94;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_94_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_94_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_94_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_94_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_94_inst_4.BIST_SO;
   }

   // Memory block 95
   Instance memory_block_95_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_95_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_95_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_95_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_95_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_95_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_95_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_95_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_95_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_95_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_95;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_95.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_95.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_95_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_95_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_95_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_95_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_95_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_95_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_95_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_95_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_95_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_95_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_95;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_95.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_95.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_95_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_95_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_95_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_95_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_95_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_95_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_95_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_95_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_95_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_95_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_95;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_95.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_95.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_95_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_95_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_95_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_95_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_95_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_95_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_95_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_95_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_95_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_95_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_95;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_95.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_95.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_95_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_95_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_95_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_95_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_95_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_95_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_95_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_95_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_95_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_95_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_95;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_95.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_95.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_95 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_95;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_95;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_95_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_95_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_95_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_95_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_95_inst_4.BIST_SO;
   }

   // Memory block 96
   Instance memory_block_96_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_96_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_96_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_96_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_96_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_96_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_96_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_96_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_96_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_96_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_96;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_96.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_96.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_96_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_96_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_96_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_96_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_96_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_96_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_96_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_96_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_96_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_96_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_96;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_96.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_96.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_96_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_96_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_96_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_96_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_96_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_96_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_96_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_96_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_96_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_96_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_96;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_96.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_96.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_96_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_96_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_96_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_96_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_96_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_96_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_96_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_96_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_96_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_96_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_96;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_96.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_96.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_96_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_96_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_96_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_96_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_96_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_96_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_96_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_96_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_96_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_96_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_96;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_96.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_96.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_96 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_96;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_96;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_96_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_96_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_96_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_96_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_96_inst_4.BIST_SO;
   }

   // Memory block 97
   Instance memory_block_97_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_97_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_97_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_97_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_97_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_97_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_97_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_97_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_97_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_97_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_97;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_97.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_97.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_97_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_97_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_97_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_97_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_97_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_97_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_97_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_97_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_97_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_97_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_97;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_97.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_97.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_97_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_97_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_97_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_97_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_97_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_97_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_97_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_97_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_97_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_97_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_97;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_97.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_97.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_97_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_97_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_97_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_97_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_97_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_97_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_97_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_97_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_97_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_97_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_97;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_97.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_97.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_97_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_97_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_97_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_97_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_97_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_97_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_97_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_97_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_97_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_97_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_97;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_97.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_97.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_97 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_97;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_97;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_97_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_97_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_97_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_97_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_97_inst_4.BIST_SO;
   }

   // Memory block 98
   Instance memory_block_98_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_98_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_98_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_98_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_98_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_98_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_98_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_98_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_98_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_98_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_98;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_98.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_98.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_98_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_98_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_98_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_98_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_98_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_98_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_98_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_98_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_98_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_98_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_98;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_98.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_98.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_98_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_98_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_98_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_98_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_98_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_98_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_98_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_98_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_98_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_98_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_98;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_98.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_98.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_98_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_98_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_98_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_98_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_98_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_98_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_98_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_98_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_98_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_98_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_98;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_98.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_98.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_98_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_98_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_98_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_98_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_98_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_98_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_98_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_98_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_98_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_98_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_98;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_98.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_98.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_98 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_98;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_98;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_98_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_98_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_98_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_98_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_98_inst_4.BIST_SO;
   }

   // Memory block 99
   Instance memory_block_99_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_99_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_99_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_99_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_99_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_99_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_99_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_99_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_99_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_99_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_99;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_99.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_99.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_99_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_99_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_99_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_99_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_99_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_99_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_99_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_99_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_99_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_99_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_99;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_99.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_99.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_99_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_99_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_99_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_99_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_99_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_99_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_99_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_99_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_99_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_99_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_99;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_99.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_99.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_99_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_99_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_99_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_99_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_99_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_99_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_99_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_99_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_99_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_99_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_99;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_99.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_99.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_99_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_99_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_99_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_99_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_99_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_99_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_99_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_99_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_99_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_99_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_99;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_99.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_99.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_99 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_99;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_99;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_99_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_99_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_99_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_99_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_99_inst_4.BIST_SO;
   }



// One MBIST BAP for the core
   Instance mbist_bap_inst Of mbist_bap {
      InputPort reset              = sib_inst.to_ijtag_reset;
      InputPort ijtag_select       = sib_mbist_inst.ijtag_to_sel;
      InputPort si                 = sib_inst.to_ijtag_si;
      InputPort capture_en         = sib_inst.to_ijtag_ce;
      InputPort shift_en           = sib_inst.to_ijtag_se;
      InputPort update_en          = sib_inst.to_ijtag_ue;
      InputPort tck                = sib_inst.to_ijtag_tck;
      InputPort fromBist_0 = mbist_controller_block_0.MBISTPG_SO;
      InputPort fromBist_1 = mbist_controller_block_1.MBISTPG_SO;
      InputPort fromBist_2 = mbist_controller_block_2.MBISTPG_SO;
      InputPort fromBist_3 = mbist_controller_block_3.MBISTPG_SO;
      InputPort fromBist_4 = mbist_controller_block_4.MBISTPG_SO;
      InputPort fromBist_5 = mbist_controller_block_5.MBISTPG_SO;
      InputPort fromBist_6 = mbist_controller_block_6.MBISTPG_SO;
      InputPort fromBist_7 = mbist_controller_block_7.MBISTPG_SO;
      InputPort fromBist_8 = mbist_controller_block_8.MBISTPG_SO;
      InputPort fromBist_9 = mbist_controller_block_9.MBISTPG_SO;
      InputPort fromBist_10 = mbist_controller_block_10.MBISTPG_SO;
      InputPort fromBist_11 = mbist_controller_block_11.MBISTPG_SO;
      InputPort fromBist_12 = mbist_controller_block_12.MBISTPG_SO;
      InputPort fromBist_13 = mbist_controller_block_13.MBISTPG_SO;
      InputPort fromBist_14 = mbist_controller_block_14.MBISTPG_SO;
      InputPort fromBist_15 = mbist_controller_block_15.MBISTPG_SO;
      InputPort fromBist_16 = mbist_controller_block_16.MBISTPG_SO;
      InputPort fromBist_17 = mbist_controller_block_17.MBISTPG_SO;
      InputPort fromBist_18 = mbist_controller_block_18.MBISTPG_SO;
      InputPort fromBist_19 = mbist_controller_block_19.MBISTPG_SO;
      InputPort fromBist_20 = mbist_controller_block_20.MBISTPG_SO;
      InputPort fromBist_21 = mbist_controller_block_21.MBISTPG_SO;
      InputPort fromBist_22 = mbist_controller_block_22.MBISTPG_SO;
      InputPort fromBist_23 = mbist_controller_block_23.MBISTPG_SO;
      InputPort fromBist_24 = mbist_controller_block_24.MBISTPG_SO;
      InputPort fromBist_25 = mbist_controller_block_25.MBISTPG_SO;
      InputPort fromBist_26 = mbist_controller_block_26.MBISTPG_SO;
      InputPort fromBist_27 = mbist_controller_block_27.MBISTPG_SO;
      InputPort fromBist_28 = mbist_controller_block_28.MBISTPG_SO;
      InputPort fromBist_29 = mbist_controller_block_29.MBISTPG_SO;
      InputPort fromBist_30 = mbist_controller_block_30.MBISTPG_SO;
      InputPort fromBist_31 = mbist_controller_block_31.MBISTPG_SO;
      InputPort fromBist_32 = mbist_controller_block_32.MBISTPG_SO;
      InputPort fromBist_33 = mbist_controller_block_33.MBISTPG_SO;
      InputPort fromBist_34 = mbist_controller_block_34.MBISTPG_SO;
      InputPort fromBist_35 = mbist_controller_block_35.MBISTPG_SO;
      InputPort fromBist_36 = mbist_controller_block_36.MBISTPG_SO;
      InputPort fromBist_37 = mbist_controller_block_37.MBISTPG_SO;
      InputPort fromBist_38 = mbist_controller_block_38.MBISTPG_SO;
      InputPort fromBist_39 = mbist_controller_block_39.MBISTPG_SO;
      InputPort fromBist_40 = mbist_controller_block_40.MBISTPG_SO;
      InputPort fromBist_41 = mbist_controller_block_41.MBISTPG_SO;
      InputPort fromBist_42 = mbist_controller_block_42.MBISTPG_SO;
      InputPort fromBist_43 = mbist_controller_block_43.MBISTPG_SO;
      InputPort fromBist_44 = mbist_controller_block_44.MBISTPG_SO;
      InputPort fromBist_45 = mbist_controller_block_45.MBISTPG_SO;
      InputPort fromBist_46 = mbist_controller_block_46.MBISTPG_SO;
      InputPort fromBist_47 = mbist_controller_block_47.MBISTPG_SO;
      InputPort fromBist_48 = mbist_controller_block_48.MBISTPG_SO;
      InputPort fromBist_49 = mbist_controller_block_49.MBISTPG_SO;
      InputPort fromBist_50 = mbist_controller_block_50.MBISTPG_SO;
      InputPort fromBist_51 = mbist_controller_block_51.MBISTPG_SO;
      InputPort fromBist_52 = mbist_controller_block_52.MBISTPG_SO;
      InputPort fromBist_53 = mbist_controller_block_53.MBISTPG_SO;
      InputPort fromBist_54 = mbist_controller_block_54.MBISTPG_SO;
      InputPort fromBist_55 = mbist_controller_block_55.MBISTPG_SO;
      InputPort fromBist_56 = mbist_controller_block_56.MBISTPG_SO;
      InputPort fromBist_57 = mbist_controller_block_57.MBISTPG_SO;
      InputPort fromBist_58 = mbist_controller_block_58.MBISTPG_SO;
      InputPort fromBist_59 = mbist_controller_block_59.MBISTPG_SO;
      InputPort fromBist_60 = mbist_controller_block_60.MBISTPG_SO;
      InputPort fromBist_61 = mbist_controller_block_61.MBISTPG_SO;
      InputPort fromBist_62 = mbist_controller_block_62.MBISTPG_SO;
      InputPort fromBist_63 = mbist_controller_block_63.MBISTPG_SO;
      InputPort fromBist_64 = mbist_controller_block_64.MBISTPG_SO;
      InputPort fromBist_65 = mbist_controller_block_65.MBISTPG_SO;
      InputPort fromBist_66 = mbist_controller_block_66.MBISTPG_SO;
      InputPort fromBist_67 = mbist_controller_block_67.MBISTPG_SO;
      InputPort fromBist_68 = mbist_controller_block_68.MBISTPG_SO;
      InputPort fromBist_69 = mbist_controller_block_69.MBISTPG_SO;
      InputPort fromBist_70 = mbist_controller_block_70.MBISTPG_SO;
      InputPort fromBist_71 = mbist_controller_block_71.MBISTPG_SO;
      InputPort fromBist_72 = mbist_controller_block_72.MBISTPG_SO;
      InputPort fromBist_73 = mbist_controller_block_73.MBISTPG_SO;
      InputPort fromBist_74 = mbist_controller_block_74.MBISTPG_SO;
      InputPort fromBist_75 = mbist_controller_block_75.MBISTPG_SO;
      InputPort fromBist_76 = mbist_controller_block_76.MBISTPG_SO;
      InputPort fromBist_77 = mbist_controller_block_77.MBISTPG_SO;
      InputPort fromBist_78 = mbist_controller_block_78.MBISTPG_SO;
      InputPort fromBist_79 = mbist_controller_block_79.MBISTPG_SO;
      InputPort fromBist_80 = mbist_controller_block_80.MBISTPG_SO;
      InputPort fromBist_81 = mbist_controller_block_81.MBISTPG_SO;
      InputPort fromBist_82 = mbist_controller_block_82.MBISTPG_SO;
      InputPort fromBist_83 = mbist_controller_block_83.MBISTPG_SO;
      InputPort fromBist_84 = mbist_controller_block_84.MBISTPG_SO;
      InputPort fromBist_85 = mbist_controller_block_85.MBISTPG_SO;
      InputPort fromBist_86 = mbist_controller_block_86.MBISTPG_SO;
      InputPort fromBist_87 = mbist_controller_block_87.MBISTPG_SO;
      InputPort fromBist_88 = mbist_controller_block_88.MBISTPG_SO;
      InputPort fromBist_89 = mbist_controller_block_89.MBISTPG_SO;
      InputPort fromBist_90 = mbist_controller_block_90.MBISTPG_SO;
      InputPort fromBist_91 = mbist_controller_block_91.MBISTPG_SO;
      InputPort fromBist_92 = mbist_controller_block_92.MBISTPG_SO;
      InputPort fromBist_93 = mbist_controller_block_93.MBISTPG_SO;
      InputPort fromBist_94 = mbist_controller_block_94.MBISTPG_SO;
      InputPort fromBist_95 = mbist_controller_block_95.MBISTPG_SO;
      InputPort fromBist_96 = mbist_controller_block_96.MBISTPG_SO;
      InputPort fromBist_97 = mbist_controller_block_97.MBISTPG_SO;
      InputPort fromBist_98 = mbist_controller_block_98.MBISTPG_SO;
      InputPort fromBist_99 = mbist_controller_block_99.MBISTPG_SO;
      InputPort MBISTPG_GO_0 = mbist_controller_block_0.MBISTPG_GO;
      InputPort MBISTPG_GO_1 = mbist_controller_block_1.MBISTPG_GO;
      InputPort MBISTPG_GO_2 = mbist_controller_block_2.MBISTPG_GO;
      InputPort MBISTPG_GO_3 = mbist_controller_block_3.MBISTPG_GO;
      InputPort MBISTPG_GO_4 = mbist_controller_block_4.MBISTPG_GO;
      InputPort MBISTPG_GO_5 = mbist_controller_block_5.MBISTPG_GO;
      InputPort MBISTPG_GO_6 = mbist_controller_block_6.MBISTPG_GO;
      InputPort MBISTPG_GO_7 = mbist_controller_block_7.MBISTPG_GO;
      InputPort MBISTPG_GO_8 = mbist_controller_block_8.MBISTPG_GO;
      InputPort MBISTPG_GO_9 = mbist_controller_block_9.MBISTPG_GO;
      InputPort MBISTPG_GO_10 = mbist_controller_block_10.MBISTPG_GO;
      InputPort MBISTPG_GO_11 = mbist_controller_block_11.MBISTPG_GO;
      InputPort MBISTPG_GO_12 = mbist_controller_block_12.MBISTPG_GO;
      InputPort MBISTPG_GO_13 = mbist_controller_block_13.MBISTPG_GO;
      InputPort MBISTPG_GO_14 = mbist_controller_block_14.MBISTPG_GO;
      InputPort MBISTPG_GO_15 = mbist_controller_block_15.MBISTPG_GO;
      InputPort MBISTPG_GO_16 = mbist_controller_block_16.MBISTPG_GO;
      InputPort MBISTPG_GO_17 = mbist_controller_block_17.MBISTPG_GO;
      InputPort MBISTPG_GO_18 = mbist_controller_block_18.MBISTPG_GO;
      InputPort MBISTPG_GO_19 = mbist_controller_block_19.MBISTPG_GO;
      InputPort MBISTPG_GO_20 = mbist_controller_block_20.MBISTPG_GO;
      InputPort MBISTPG_GO_21 = mbist_controller_block_21.MBISTPG_GO;
      InputPort MBISTPG_GO_22 = mbist_controller_block_22.MBISTPG_GO;
      InputPort MBISTPG_GO_23 = mbist_controller_block_23.MBISTPG_GO;
      InputPort MBISTPG_GO_24 = mbist_controller_block_24.MBISTPG_GO;
      InputPort MBISTPG_GO_25 = mbist_controller_block_25.MBISTPG_GO;
      InputPort MBISTPG_GO_26 = mbist_controller_block_26.MBISTPG_GO;
      InputPort MBISTPG_GO_27 = mbist_controller_block_27.MBISTPG_GO;
      InputPort MBISTPG_GO_28 = mbist_controller_block_28.MBISTPG_GO;
      InputPort MBISTPG_GO_29 = mbist_controller_block_29.MBISTPG_GO;
      InputPort MBISTPG_GO_30 = mbist_controller_block_30.MBISTPG_GO;
      InputPort MBISTPG_GO_31 = mbist_controller_block_31.MBISTPG_GO;
      InputPort MBISTPG_GO_32 = mbist_controller_block_32.MBISTPG_GO;
      InputPort MBISTPG_GO_33 = mbist_controller_block_33.MBISTPG_GO;
      InputPort MBISTPG_GO_34 = mbist_controller_block_34.MBISTPG_GO;
      InputPort MBISTPG_GO_35 = mbist_controller_block_35.MBISTPG_GO;
      InputPort MBISTPG_GO_36 = mbist_controller_block_36.MBISTPG_GO;
      InputPort MBISTPG_GO_37 = mbist_controller_block_37.MBISTPG_GO;
      InputPort MBISTPG_GO_38 = mbist_controller_block_38.MBISTPG_GO;
      InputPort MBISTPG_GO_39 = mbist_controller_block_39.MBISTPG_GO;
      InputPort MBISTPG_GO_40 = mbist_controller_block_40.MBISTPG_GO;
      InputPort MBISTPG_GO_41 = mbist_controller_block_41.MBISTPG_GO;
      InputPort MBISTPG_GO_42 = mbist_controller_block_42.MBISTPG_GO;
      InputPort MBISTPG_GO_43 = mbist_controller_block_43.MBISTPG_GO;
      InputPort MBISTPG_GO_44 = mbist_controller_block_44.MBISTPG_GO;
      InputPort MBISTPG_GO_45 = mbist_controller_block_45.MBISTPG_GO;
      InputPort MBISTPG_GO_46 = mbist_controller_block_46.MBISTPG_GO;
      InputPort MBISTPG_GO_47 = mbist_controller_block_47.MBISTPG_GO;
      InputPort MBISTPG_GO_48 = mbist_controller_block_48.MBISTPG_GO;
      InputPort MBISTPG_GO_49 = mbist_controller_block_49.MBISTPG_GO;
      InputPort MBISTPG_GO_50 = mbist_controller_block_50.MBISTPG_GO;
      InputPort MBISTPG_GO_51 = mbist_controller_block_51.MBISTPG_GO;
      InputPort MBISTPG_GO_52 = mbist_controller_block_52.MBISTPG_GO;
      InputPort MBISTPG_GO_53 = mbist_controller_block_53.MBISTPG_GO;
      InputPort MBISTPG_GO_54 = mbist_controller_block_54.MBISTPG_GO;
      InputPort MBISTPG_GO_55 = mbist_controller_block_55.MBISTPG_GO;
      InputPort MBISTPG_GO_56 = mbist_controller_block_56.MBISTPG_GO;
      InputPort MBISTPG_GO_57 = mbist_controller_block_57.MBISTPG_GO;
      InputPort MBISTPG_GO_58 = mbist_controller_block_58.MBISTPG_GO;
      InputPort MBISTPG_GO_59 = mbist_controller_block_59.MBISTPG_GO;
      InputPort MBISTPG_GO_60 = mbist_controller_block_60.MBISTPG_GO;
      InputPort MBISTPG_GO_61 = mbist_controller_block_61.MBISTPG_GO;
      InputPort MBISTPG_GO_62 = mbist_controller_block_62.MBISTPG_GO;
      InputPort MBISTPG_GO_63 = mbist_controller_block_63.MBISTPG_GO;
      InputPort MBISTPG_GO_64 = mbist_controller_block_64.MBISTPG_GO;
      InputPort MBISTPG_GO_65 = mbist_controller_block_65.MBISTPG_GO;
      InputPort MBISTPG_GO_66 = mbist_controller_block_66.MBISTPG_GO;
      InputPort MBISTPG_GO_67 = mbist_controller_block_67.MBISTPG_GO;
      InputPort MBISTPG_GO_68 = mbist_controller_block_68.MBISTPG_GO;
      InputPort MBISTPG_GO_69 = mbist_controller_block_69.MBISTPG_GO;
      InputPort MBISTPG_GO_70 = mbist_controller_block_70.MBISTPG_GO;
      InputPort MBISTPG_GO_71 = mbist_controller_block_71.MBISTPG_GO;
      InputPort MBISTPG_GO_72 = mbist_controller_block_72.MBISTPG_GO;
      InputPort MBISTPG_GO_73 = mbist_controller_block_73.MBISTPG_GO;
      InputPort MBISTPG_GO_74 = mbist_controller_block_74.MBISTPG_GO;
      InputPort MBISTPG_GO_75 = mbist_controller_block_75.MBISTPG_GO;
      InputPort MBISTPG_GO_76 = mbist_controller_block_76.MBISTPG_GO;
      InputPort MBISTPG_GO_77 = mbist_controller_block_77.MBISTPG_GO;
      InputPort MBISTPG_GO_78 = mbist_controller_block_78.MBISTPG_GO;
      InputPort MBISTPG_GO_79 = mbist_controller_block_79.MBISTPG_GO;
      InputPort MBISTPG_GO_80 = mbist_controller_block_80.MBISTPG_GO;
      InputPort MBISTPG_GO_81 = mbist_controller_block_81.MBISTPG_GO;
      InputPort MBISTPG_GO_82 = mbist_controller_block_82.MBISTPG_GO;
      InputPort MBISTPG_GO_83 = mbist_controller_block_83.MBISTPG_GO;
      InputPort MBISTPG_GO_84 = mbist_controller_block_84.MBISTPG_GO;
      InputPort MBISTPG_GO_85 = mbist_controller_block_85.MBISTPG_GO;
      InputPort MBISTPG_GO_86 = mbist_controller_block_86.MBISTPG_GO;
      InputPort MBISTPG_GO_87 = mbist_controller_block_87.MBISTPG_GO;
      InputPort MBISTPG_GO_88 = mbist_controller_block_88.MBISTPG_GO;
      InputPort MBISTPG_GO_89 = mbist_controller_block_89.MBISTPG_GO;
      InputPort MBISTPG_GO_90 = mbist_controller_block_90.MBISTPG_GO;
      InputPort MBISTPG_GO_91 = mbist_controller_block_91.MBISTPG_GO;
      InputPort MBISTPG_GO_92 = mbist_controller_block_92.MBISTPG_GO;
      InputPort MBISTPG_GO_93 = mbist_controller_block_93.MBISTPG_GO;
      InputPort MBISTPG_GO_94 = mbist_controller_block_94.MBISTPG_GO;
      InputPort MBISTPG_GO_95 = mbist_controller_block_95.MBISTPG_GO;
      InputPort MBISTPG_GO_96 = mbist_controller_block_96.MBISTPG_GO;
      InputPort MBISTPG_GO_97 = mbist_controller_block_97.MBISTPG_GO;
      InputPort MBISTPG_GO_98 = mbist_controller_block_98.MBISTPG_GO;
      InputPort MBISTPG_GO_99 = mbist_controller_block_99.MBISTPG_GO;
      InputPort MBISTPG_DONE_0 = mbist_controller_block_0.MBISTPG_DONE;
      InputPort MBISTPG_DONE_1 = mbist_controller_block_1.MBISTPG_DONE;
      InputPort MBISTPG_DONE_2 = mbist_controller_block_2.MBISTPG_DONE;
      InputPort MBISTPG_DONE_3 = mbist_controller_block_3.MBISTPG_DONE;
      InputPort MBISTPG_DONE_4 = mbist_controller_block_4.MBISTPG_DONE;
      InputPort MBISTPG_DONE_5 = mbist_controller_block_5.MBISTPG_DONE;
      InputPort MBISTPG_DONE_6 = mbist_controller_block_6.MBISTPG_DONE;
      InputPort MBISTPG_DONE_7 = mbist_controller_block_7.MBISTPG_DONE;
      InputPort MBISTPG_DONE_8 = mbist_controller_block_8.MBISTPG_DONE;
      InputPort MBISTPG_DONE_9 = mbist_controller_block_9.MBISTPG_DONE;
      InputPort MBISTPG_DONE_10 = mbist_controller_block_10.MBISTPG_DONE;
      InputPort MBISTPG_DONE_11 = mbist_controller_block_11.MBISTPG_DONE;
      InputPort MBISTPG_DONE_12 = mbist_controller_block_12.MBISTPG_DONE;
      InputPort MBISTPG_DONE_13 = mbist_controller_block_13.MBISTPG_DONE;
      InputPort MBISTPG_DONE_14 = mbist_controller_block_14.MBISTPG_DONE;
      InputPort MBISTPG_DONE_15 = mbist_controller_block_15.MBISTPG_DONE;
      InputPort MBISTPG_DONE_16 = mbist_controller_block_16.MBISTPG_DONE;
      InputPort MBISTPG_DONE_17 = mbist_controller_block_17.MBISTPG_DONE;
      InputPort MBISTPG_DONE_18 = mbist_controller_block_18.MBISTPG_DONE;
      InputPort MBISTPG_DONE_19 = mbist_controller_block_19.MBISTPG_DONE;
      InputPort MBISTPG_DONE_20 = mbist_controller_block_20.MBISTPG_DONE;
      InputPort MBISTPG_DONE_21 = mbist_controller_block_21.MBISTPG_DONE;
      InputPort MBISTPG_DONE_22 = mbist_controller_block_22.MBISTPG_DONE;
      InputPort MBISTPG_DONE_23 = mbist_controller_block_23.MBISTPG_DONE;
      InputPort MBISTPG_DONE_24 = mbist_controller_block_24.MBISTPG_DONE;
      InputPort MBISTPG_DONE_25 = mbist_controller_block_25.MBISTPG_DONE;
      InputPort MBISTPG_DONE_26 = mbist_controller_block_26.MBISTPG_DONE;
      InputPort MBISTPG_DONE_27 = mbist_controller_block_27.MBISTPG_DONE;
      InputPort MBISTPG_DONE_28 = mbist_controller_block_28.MBISTPG_DONE;
      InputPort MBISTPG_DONE_29 = mbist_controller_block_29.MBISTPG_DONE;
      InputPort MBISTPG_DONE_30 = mbist_controller_block_30.MBISTPG_DONE;
      InputPort MBISTPG_DONE_31 = mbist_controller_block_31.MBISTPG_DONE;
      InputPort MBISTPG_DONE_32 = mbist_controller_block_32.MBISTPG_DONE;
      InputPort MBISTPG_DONE_33 = mbist_controller_block_33.MBISTPG_DONE;
      InputPort MBISTPG_DONE_34 = mbist_controller_block_34.MBISTPG_DONE;
      InputPort MBISTPG_DONE_35 = mbist_controller_block_35.MBISTPG_DONE;
      InputPort MBISTPG_DONE_36 = mbist_controller_block_36.MBISTPG_DONE;
      InputPort MBISTPG_DONE_37 = mbist_controller_block_37.MBISTPG_DONE;
      InputPort MBISTPG_DONE_38 = mbist_controller_block_38.MBISTPG_DONE;
      InputPort MBISTPG_DONE_39 = mbist_controller_block_39.MBISTPG_DONE;
      InputPort MBISTPG_DONE_40 = mbist_controller_block_40.MBISTPG_DONE;
      InputPort MBISTPG_DONE_41 = mbist_controller_block_41.MBISTPG_DONE;
      InputPort MBISTPG_DONE_42 = mbist_controller_block_42.MBISTPG_DONE;
      InputPort MBISTPG_DONE_43 = mbist_controller_block_43.MBISTPG_DONE;
      InputPort MBISTPG_DONE_44 = mbist_controller_block_44.MBISTPG_DONE;
      InputPort MBISTPG_DONE_45 = mbist_controller_block_45.MBISTPG_DONE;
      InputPort MBISTPG_DONE_46 = mbist_controller_block_46.MBISTPG_DONE;
      InputPort MBISTPG_DONE_47 = mbist_controller_block_47.MBISTPG_DONE;
      InputPort MBISTPG_DONE_48 = mbist_controller_block_48.MBISTPG_DONE;
      InputPort MBISTPG_DONE_49 = mbist_controller_block_49.MBISTPG_DONE;
      InputPort MBISTPG_DONE_50 = mbist_controller_block_50.MBISTPG_DONE;
      InputPort MBISTPG_DONE_51 = mbist_controller_block_51.MBISTPG_DONE;
      InputPort MBISTPG_DONE_52 = mbist_controller_block_52.MBISTPG_DONE;
      InputPort MBISTPG_DONE_53 = mbist_controller_block_53.MBISTPG_DONE;
      InputPort MBISTPG_DONE_54 = mbist_controller_block_54.MBISTPG_DONE;
      InputPort MBISTPG_DONE_55 = mbist_controller_block_55.MBISTPG_DONE;
      InputPort MBISTPG_DONE_56 = mbist_controller_block_56.MBISTPG_DONE;
      InputPort MBISTPG_DONE_57 = mbist_controller_block_57.MBISTPG_DONE;
      InputPort MBISTPG_DONE_58 = mbist_controller_block_58.MBISTPG_DONE;
      InputPort MBISTPG_DONE_59 = mbist_controller_block_59.MBISTPG_DONE;
      InputPort MBISTPG_DONE_60 = mbist_controller_block_60.MBISTPG_DONE;
      InputPort MBISTPG_DONE_61 = mbist_controller_block_61.MBISTPG_DONE;
      InputPort MBISTPG_DONE_62 = mbist_controller_block_62.MBISTPG_DONE;
      InputPort MBISTPG_DONE_63 = mbist_controller_block_63.MBISTPG_DONE;
      InputPort MBISTPG_DONE_64 = mbist_controller_block_64.MBISTPG_DONE;
      InputPort MBISTPG_DONE_65 = mbist_controller_block_65.MBISTPG_DONE;
      InputPort MBISTPG_DONE_66 = mbist_controller_block_66.MBISTPG_DONE;
      InputPort MBISTPG_DONE_67 = mbist_controller_block_67.MBISTPG_DONE;
      InputPort MBISTPG_DONE_68 = mbist_controller_block_68.MBISTPG_DONE;
      InputPort MBISTPG_DONE_69 = mbist_controller_block_69.MBISTPG_DONE;
      InputPort MBISTPG_DONE_70 = mbist_controller_block_70.MBISTPG_DONE;
      InputPort MBISTPG_DONE_71 = mbist_controller_block_71.MBISTPG_DONE;
      InputPort MBISTPG_DONE_72 = mbist_controller_block_72.MBISTPG_DONE;
      InputPort MBISTPG_DONE_73 = mbist_controller_block_73.MBISTPG_DONE;
      InputPort MBISTPG_DONE_74 = mbist_controller_block_74.MBISTPG_DONE;
      InputPort MBISTPG_DONE_75 = mbist_controller_block_75.MBISTPG_DONE;
      InputPort MBISTPG_DONE_76 = mbist_controller_block_76.MBISTPG_DONE;
      InputPort MBISTPG_DONE_77 = mbist_controller_block_77.MBISTPG_DONE;
      InputPort MBISTPG_DONE_78 = mbist_controller_block_78.MBISTPG_DONE;
      InputPort MBISTPG_DONE_79 = mbist_controller_block_79.MBISTPG_DONE;
      InputPort MBISTPG_DONE_80 = mbist_controller_block_80.MBISTPG_DONE;
      InputPort MBISTPG_DONE_81 = mbist_controller_block_81.MBISTPG_DONE;
      InputPort MBISTPG_DONE_82 = mbist_controller_block_82.MBISTPG_DONE;
      InputPort MBISTPG_DONE_83 = mbist_controller_block_83.MBISTPG_DONE;
      InputPort MBISTPG_DONE_84 = mbist_controller_block_84.MBISTPG_DONE;
      InputPort MBISTPG_DONE_85 = mbist_controller_block_85.MBISTPG_DONE;
      InputPort MBISTPG_DONE_86 = mbist_controller_block_86.MBISTPG_DONE;
      InputPort MBISTPG_DONE_87 = mbist_controller_block_87.MBISTPG_DONE;
      InputPort MBISTPG_DONE_88 = mbist_controller_block_88.MBISTPG_DONE;
      InputPort MBISTPG_DONE_89 = mbist_controller_block_89.MBISTPG_DONE;
      InputPort MBISTPG_DONE_90 = mbist_controller_block_90.MBISTPG_DONE;
      InputPort MBISTPG_DONE_91 = mbist_controller_block_91.MBISTPG_DONE;
      InputPort MBISTPG_DONE_92 = mbist_controller_block_92.MBISTPG_DONE;
      InputPort MBISTPG_DONE_93 = mbist_controller_block_93.MBISTPG_DONE;
      InputPort MBISTPG_DONE_94 = mbist_controller_block_94.MBISTPG_DONE;
      InputPort MBISTPG_DONE_95 = mbist_controller_block_95.MBISTPG_DONE;
      InputPort MBISTPG_DONE_96 = mbist_controller_block_96.MBISTPG_DONE;
      InputPort MBISTPG_DONE_97 = mbist_controller_block_97.MBISTPG_DONE;
      InputPort MBISTPG_DONE_98 = mbist_controller_block_98.MBISTPG_DONE;
      InputPort MBISTPG_DONE_99 = mbist_controller_block_99.MBISTPG_DONE;
   }
   Instance sib_mbist_inst Of sib_1 {
      InputPort ijtag_reset = sib_inst.to_ijtag_reset;
      InputPort ijtag_sel = sib_inst.ijtag_to_sel;
      InputPort ijtag_si = sib_inst.to_ijtag_si;
      InputPort ijtag_ce = sib_inst.to_ijtag_ce;
      InputPort ijtag_se = sib_inst.to_ijtag_se;
      InputPort ijtag_ue = sib_inst.to_ijtag_ue;
      InputPort ijtag_tck = sib_inst.to_ijtag_tck;
      InputPort ijtag_from_so = mbist_bap_inst.so;
      }
   Instance sib_inst Of sib_2 {
      InputPort ijtag_reset = ijtag_reset;
      InputPort ijtag_sel = ijtag_sel;
      InputPort ijtag_si = ijtag_si;
      InputPort ijtag_ce = ijtag_ce;
      InputPort ijtag_se = ijtag_se;
      InputPort ijtag_ue = ijtag_ue;
      InputPort ijtag_tck = ijtag_tck;
      InputPort ijtag_from_so = sib_mbist_inst.ijtag_so;
   }
}



Module chip_tap_fsm {
   TCKPort tck;
   TMSPort tms;
   TRSTPort trst;
   ToIRSelectPort irSel;
   ToResetPort tlr;
}

Module chip_tap {
   TCKPort tck;
   ScanInPort tdi;
   ScanOutPort tdo {
      Source IRMux;
   }
   TMSPort tms;
   TRSTPort trst;
   ToCaptureEnPort capture_dr_en;
   ToShiftEnPort shift_dr_en;
   ToUpdateEnPort update_dr_en;
   ToResetPort test_logic_reset {
      ActivePolarity 0;
   }
   ToSelectPort host_1_to_sel {
      Source host_1_to_sel_int;
   }
   ScanInPort host_1_from_so;
   DataOutPort force_disable {
      Source force_disable_int;
   }
   DataOutPort select_jtag_input {
      Source select_jtag_input_int;
   }
   DataOutPort select_jtag_output {
      Source select_jtag_output_int;
   }
   DataOutPort extest_pulse {
      Source ext_test_pulse_int;
   }
   DataOutPort extest_train {
      Source ext_test_train_int;
   }
   DataOutPort fsm_state[3:0] {
      RefEnum state_encoding;
   }
   ScanInterface tap_client {
      Port tdi;
      Port tdo;
      Port tms;
   }
   ScanInterface host_ijtag_1 {
      Port host_1_from_so;
      Port host_1_to_sel;
   }
   Enum state_encoding {
      test_logic_reset = 4'b1111;
      run_test_idle = 4'b1100;
      select_dr = 4'b0111;
      capture_dr = 4'b0110;
      shift_dr = 4'b0010;
      exit1_dr = 4'b0001;
      pause_dr = 4'b0011;
      exit2_dr = 4'b0000;
      update_dr = 4'b0101;
      select_ir = 4'b0100;
      capture_ir = 4'b1110;
      shift_ir = 4'b1010;
      exit1_ir = 4'b1001;
      pause_ir = 4'b1011;
      exit2_ir = 4'b1000;
      update_ir = 4'b1101;
   }
   Enum instruction_opcodes {
      BYPASS = 4'b1111;
      CLAMP = 4'b0000;
      EXTEST = 4'b0001;
      EXTEST_PULSE = 4'b0010;
      EXTEST_TRAIN = 4'b0011;
      INTEST = 4'b0100;
      SAMPLE = 4'b0101;
      PRELOAD = 4'b0101;
      HIGHZ = 4'b0110;
      HOSTIJTAG_1 = 4'b0111;
   }
   ScanRegister instruction[3:0] {
      ScanInSource tdi;
      CaptureSource 4'b0001;
      ResetValue 4'b1111;
      RefEnum instruction_opcodes;
   }
   ScanRegister bypass {
      ScanInSource tdi;
      CaptureSource 1'b0;
   }
   ScanMux IRMux SelectedBy fsm.irSel {
      1'b0 : DRMux;
      1'b1 : instruction[0];
   }
   ScanMux DRMux SelectedBy instruction {
      4'b1111 : bypass;
      4'b0000 : bypass;
      4'b0001 : bypass;
      4'b0010 : bypass;
      4'b0011 : bypass;
      4'b0100 : bypass;
      4'b0101 : bypass;
      4'b0110 : bypass;
      4'b0111 : host_1_from_so;
   }
   LogicSignal host_1_to_sel_int {
      instruction == HOSTIJTAG_1;
   }
   LogicSignal force_disable_int {
      instruction == HIGHZ;
   }
   LogicSignal select_jtag_input_int {
      instruction == INTEST;
   }
   LogicSignal select_jtag_output_int {
      ((((instruction == EXTEST) || (instruction == EXTEST_PULSE)) ||
          (instruction == EXTEST_TRAIN)) || (instruction == CLAMP)) ||
          (instruction == HIGHZ);
   }
   LogicSignal ext_test_pulse_int {
      instruction == EXTEST_PULSE;
   }
   LogicSignal ext_test_train_int {
      instruction == EXTEST_TRAIN;
   }
   Instance fsm Of chip_tap_fsm {
      InputPort tck = tck;
      InputPort tms = tms;
      InputPort trst = trst;
   }
}


Module chip {
   ClockPort clk;
   TCKPort tck;
   ScanInPort tdi;
   ScanOutPort tdo {
      Source chip_tap_inst.tdo;
   }
   TMSPort tms;
   TRSTPort trst;
   ScanInterface tap {
      Port tck;
      Port tdi;
      Port tdo;
      Port tms;
      Port trst;
   }


   Instance chip_tap_inst Of chip_tap {
      InputPort tck    = tck;
      InputPort tdi    = tdi;
      InputPort tms    = tms;
      InputPort trst   = trst;
      InputPort host_1_from_so = sib_core99.ijtag_so;
   }
   Instance core0 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core0.ijtag_to_sel;
      InputPort ijtag_si    = tdi;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core0 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = tdi;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core0.ijtag_so;
   }
   Instance core1 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core1.ijtag_to_sel;
      InputPort ijtag_si    = sib_core0.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core1 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core0.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core1.ijtag_so;
   }
   Instance core2 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core2.ijtag_to_sel;
      InputPort ijtag_si    = sib_core1.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core2 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core1.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core2.ijtag_so;
   }
   Instance core3 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core3.ijtag_to_sel;
      InputPort ijtag_si    = sib_core2.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core3 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core2.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core3.ijtag_so;
   }
   Instance core4 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core4.ijtag_to_sel;
      InputPort ijtag_si    = sib_core3.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core4 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core3.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core4.ijtag_so;
   }
   Instance core5 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core5.ijtag_to_sel;
      InputPort ijtag_si    = sib_core4.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core5 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core4.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core5.ijtag_so;
   }
   Instance core6 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core6.ijtag_to_sel;
      InputPort ijtag_si    = sib_core5.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core6 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core5.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core6.ijtag_so;
   }
   Instance core7 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core7.ijtag_to_sel;
      InputPort ijtag_si    = sib_core6.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core7 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core6.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core7.ijtag_so;
   }
   Instance core8 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core8.ijtag_to_sel;
      InputPort ijtag_si    = sib_core7.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core8 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core7.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core8.ijtag_so;
   }
   Instance core9 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core9.ijtag_to_sel;
      InputPort ijtag_si    = sib_core8.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core9 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core8.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core9.ijtag_so;
   }
   Instance core10 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core10.ijtag_to_sel;
      InputPort ijtag_si    = sib_core9.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core10 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core9.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core10.ijtag_so;
   }
   Instance core11 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core11.ijtag_to_sel;
      InputPort ijtag_si    = sib_core10.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core11 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core10.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core11.ijtag_so;
   }
   Instance core12 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core12.ijtag_to_sel;
      InputPort ijtag_si    = sib_core11.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core12 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core11.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core12.ijtag_so;
   }
   Instance core13 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core13.ijtag_to_sel;
      InputPort ijtag_si    = sib_core12.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core13 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core12.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core13.ijtag_so;
   }
   Instance core14 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core14.ijtag_to_sel;
      InputPort ijtag_si    = sib_core13.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core14 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core13.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core14.ijtag_so;
   }
   Instance core15 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core15.ijtag_to_sel;
      InputPort ijtag_si    = sib_core14.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core15 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core14.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core15.ijtag_so;
   }
   Instance core16 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core16.ijtag_to_sel;
      InputPort ijtag_si    = sib_core15.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core16 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core15.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core16.ijtag_so;
   }
   Instance core17 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core17.ijtag_to_sel;
      InputPort ijtag_si    = sib_core16.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core17 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core16.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core17.ijtag_so;
   }
   Instance core18 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core18.ijtag_to_sel;
      InputPort ijtag_si    = sib_core17.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core18 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core17.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core18.ijtag_so;
   }
   Instance core19 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core19.ijtag_to_sel;
      InputPort ijtag_si    = sib_core18.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core19 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core18.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core19.ijtag_so;
   }
   Instance core20 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core20.ijtag_to_sel;
      InputPort ijtag_si    = sib_core19.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core20 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core19.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core20.ijtag_so;
   }
   Instance core21 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core21.ijtag_to_sel;
      InputPort ijtag_si    = sib_core20.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core21 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core20.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core21.ijtag_so;
   }
   Instance core22 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core22.ijtag_to_sel;
      InputPort ijtag_si    = sib_core21.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core22 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core21.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core22.ijtag_so;
   }
   Instance core23 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core23.ijtag_to_sel;
      InputPort ijtag_si    = sib_core22.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core23 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core22.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core23.ijtag_so;
   }
   Instance core24 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core24.ijtag_to_sel;
      InputPort ijtag_si    = sib_core23.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core24 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core23.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core24.ijtag_so;
   }
   Instance core25 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core25.ijtag_to_sel;
      InputPort ijtag_si    = sib_core24.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core25 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core24.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core25.ijtag_so;
   }
   Instance core26 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core26.ijtag_to_sel;
      InputPort ijtag_si    = sib_core25.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core26 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core25.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core26.ijtag_so;
   }
   Instance core27 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core27.ijtag_to_sel;
      InputPort ijtag_si    = sib_core26.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core27 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core26.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core27.ijtag_so;
   }
   Instance core28 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core28.ijtag_to_sel;
      InputPort ijtag_si    = sib_core27.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core28 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core27.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core28.ijtag_so;
   }
   Instance core29 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core29.ijtag_to_sel;
      InputPort ijtag_si    = sib_core28.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core29 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core28.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core29.ijtag_so;
   }
   Instance core30 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core30.ijtag_to_sel;
      InputPort ijtag_si    = sib_core29.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core30 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core29.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core30.ijtag_so;
   }
   Instance core31 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core31.ijtag_to_sel;
      InputPort ijtag_si    = sib_core30.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core31 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core30.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core31.ijtag_so;
   }
   Instance core32 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core32.ijtag_to_sel;
      InputPort ijtag_si    = sib_core31.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core32 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core31.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core32.ijtag_so;
   }
   Instance core33 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core33.ijtag_to_sel;
      InputPort ijtag_si    = sib_core32.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core33 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core32.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core33.ijtag_so;
   }
   Instance core34 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core34.ijtag_to_sel;
      InputPort ijtag_si    = sib_core33.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core34 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core33.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core34.ijtag_so;
   }
   Instance core35 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core35.ijtag_to_sel;
      InputPort ijtag_si    = sib_core34.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core35 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core34.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core35.ijtag_so;
   }
   Instance core36 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core36.ijtag_to_sel;
      InputPort ijtag_si    = sib_core35.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core36 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core35.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core36.ijtag_so;
   }
   Instance core37 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core37.ijtag_to_sel;
      InputPort ijtag_si    = sib_core36.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core37 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core36.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core37.ijtag_so;
   }
   Instance core38 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core38.ijtag_to_sel;
      InputPort ijtag_si    = sib_core37.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core38 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core37.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core38.ijtag_so;
   }
   Instance core39 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core39.ijtag_to_sel;
      InputPort ijtag_si    = sib_core38.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core39 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core38.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core39.ijtag_so;
   }
   Instance core40 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core40.ijtag_to_sel;
      InputPort ijtag_si    = sib_core39.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core40 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core39.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core40.ijtag_so;
   }
   Instance core41 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core41.ijtag_to_sel;
      InputPort ijtag_si    = sib_core40.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core41 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core40.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core41.ijtag_so;
   }
   Instance core42 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core42.ijtag_to_sel;
      InputPort ijtag_si    = sib_core41.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core42 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core41.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core42.ijtag_so;
   }
   Instance core43 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core43.ijtag_to_sel;
      InputPort ijtag_si    = sib_core42.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core43 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core42.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core43.ijtag_so;
   }
   Instance core44 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core44.ijtag_to_sel;
      InputPort ijtag_si    = sib_core43.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core44 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core43.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core44.ijtag_so;
   }
   Instance core45 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core45.ijtag_to_sel;
      InputPort ijtag_si    = sib_core44.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core45 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core44.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core45.ijtag_so;
   }
   Instance core46 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core46.ijtag_to_sel;
      InputPort ijtag_si    = sib_core45.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core46 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core45.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core46.ijtag_so;
   }
   Instance core47 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core47.ijtag_to_sel;
      InputPort ijtag_si    = sib_core46.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core47 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core46.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core47.ijtag_so;
   }
   Instance core48 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core48.ijtag_to_sel;
      InputPort ijtag_si    = sib_core47.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core48 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core47.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core48.ijtag_so;
   }
   Instance core49 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core49.ijtag_to_sel;
      InputPort ijtag_si    = sib_core48.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core49 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core48.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core49.ijtag_so;
   }
   Instance core50 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core50.ijtag_to_sel;
      InputPort ijtag_si    = sib_core49.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core50 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core49.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core50.ijtag_so;
   }
   Instance core51 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core51.ijtag_to_sel;
      InputPort ijtag_si    = sib_core50.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core51 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core50.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core51.ijtag_so;
   }
   Instance core52 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core52.ijtag_to_sel;
      InputPort ijtag_si    = sib_core51.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core52 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core51.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core52.ijtag_so;
   }
   Instance core53 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core53.ijtag_to_sel;
      InputPort ijtag_si    = sib_core52.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core53 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core52.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core53.ijtag_so;
   }
   Instance core54 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core54.ijtag_to_sel;
      InputPort ijtag_si    = sib_core53.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core54 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core53.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core54.ijtag_so;
   }
   Instance core55 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core55.ijtag_to_sel;
      InputPort ijtag_si    = sib_core54.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core55 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core54.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core55.ijtag_so;
   }
   Instance core56 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core56.ijtag_to_sel;
      InputPort ijtag_si    = sib_core55.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core56 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core55.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core56.ijtag_so;
   }
   Instance core57 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core57.ijtag_to_sel;
      InputPort ijtag_si    = sib_core56.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core57 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core56.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core57.ijtag_so;
   }
   Instance core58 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core58.ijtag_to_sel;
      InputPort ijtag_si    = sib_core57.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core58 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core57.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core58.ijtag_so;
   }
   Instance core59 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core59.ijtag_to_sel;
      InputPort ijtag_si    = sib_core58.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core59 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core58.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core59.ijtag_so;
   }
   Instance core60 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core60.ijtag_to_sel;
      InputPort ijtag_si    = sib_core59.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core60 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core59.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core60.ijtag_so;
   }
   Instance core61 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core61.ijtag_to_sel;
      InputPort ijtag_si    = sib_core60.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core61 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core60.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core61.ijtag_so;
   }
   Instance core62 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core62.ijtag_to_sel;
      InputPort ijtag_si    = sib_core61.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core62 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core61.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core62.ijtag_so;
   }
   Instance core63 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core63.ijtag_to_sel;
      InputPort ijtag_si    = sib_core62.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core63 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core62.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core63.ijtag_so;
   }
   Instance core64 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core64.ijtag_to_sel;
      InputPort ijtag_si    = sib_core63.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core64 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core63.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core64.ijtag_so;
   }
   Instance core65 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core65.ijtag_to_sel;
      InputPort ijtag_si    = sib_core64.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core65 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core64.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core65.ijtag_so;
   }
   Instance core66 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core66.ijtag_to_sel;
      InputPort ijtag_si    = sib_core65.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core66 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core65.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core66.ijtag_so;
   }
   Instance core67 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core67.ijtag_to_sel;
      InputPort ijtag_si    = sib_core66.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core67 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core66.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core67.ijtag_so;
   }
   Instance core68 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core68.ijtag_to_sel;
      InputPort ijtag_si    = sib_core67.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core68 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core67.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core68.ijtag_so;
   }
   Instance core69 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core69.ijtag_to_sel;
      InputPort ijtag_si    = sib_core68.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core69 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core68.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core69.ijtag_so;
   }
   Instance core70 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core70.ijtag_to_sel;
      InputPort ijtag_si    = sib_core69.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core70 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core69.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core70.ijtag_so;
   }
   Instance core71 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core71.ijtag_to_sel;
      InputPort ijtag_si    = sib_core70.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core71 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core70.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core71.ijtag_so;
   }
   Instance core72 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core72.ijtag_to_sel;
      InputPort ijtag_si    = sib_core71.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core72 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core71.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core72.ijtag_so;
   }
   Instance core73 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core73.ijtag_to_sel;
      InputPort ijtag_si    = sib_core72.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core73 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core72.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core73.ijtag_so;
   }
   Instance core74 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core74.ijtag_to_sel;
      InputPort ijtag_si    = sib_core73.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core74 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core73.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core74.ijtag_so;
   }
   Instance core75 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core75.ijtag_to_sel;
      InputPort ijtag_si    = sib_core74.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core75 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core74.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core75.ijtag_so;
   }
   Instance core76 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core76.ijtag_to_sel;
      InputPort ijtag_si    = sib_core75.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core76 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core75.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core76.ijtag_so;
   }
   Instance core77 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core77.ijtag_to_sel;
      InputPort ijtag_si    = sib_core76.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core77 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core76.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core77.ijtag_so;
   }
   Instance core78 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core78.ijtag_to_sel;
      InputPort ijtag_si    = sib_core77.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core78 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core77.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core78.ijtag_so;
   }
   Instance core79 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core79.ijtag_to_sel;
      InputPort ijtag_si    = sib_core78.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core79 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core78.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core79.ijtag_so;
   }
   Instance core80 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core80.ijtag_to_sel;
      InputPort ijtag_si    = sib_core79.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core80 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core79.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core80.ijtag_so;
   }
   Instance core81 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core81.ijtag_to_sel;
      InputPort ijtag_si    = sib_core80.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core81 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core80.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core81.ijtag_so;
   }
   Instance core82 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core82.ijtag_to_sel;
      InputPort ijtag_si    = sib_core81.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core82 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core81.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core82.ijtag_so;
   }
   Instance core83 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core83.ijtag_to_sel;
      InputPort ijtag_si    = sib_core82.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core83 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core82.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core83.ijtag_so;
   }
   Instance core84 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core84.ijtag_to_sel;
      InputPort ijtag_si    = sib_core83.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core84 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core83.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core84.ijtag_so;
   }
   Instance core85 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core85.ijtag_to_sel;
      InputPort ijtag_si    = sib_core84.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core85 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core84.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core85.ijtag_so;
   }
   Instance core86 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core86.ijtag_to_sel;
      InputPort ijtag_si    = sib_core85.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core86 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core85.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core86.ijtag_so;
   }
   Instance core87 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core87.ijtag_to_sel;
      InputPort ijtag_si    = sib_core86.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core87 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core86.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core87.ijtag_so;
   }
   Instance core88 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core88.ijtag_to_sel;
      InputPort ijtag_si    = sib_core87.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core88 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core87.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core88.ijtag_so;
   }
   Instance core89 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core89.ijtag_to_sel;
      InputPort ijtag_si    = sib_core88.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core89 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core88.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core89.ijtag_so;
   }
   Instance core90 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core90.ijtag_to_sel;
      InputPort ijtag_si    = sib_core89.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core90 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core89.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core90.ijtag_so;
   }
   Instance core91 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core91.ijtag_to_sel;
      InputPort ijtag_si    = sib_core90.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core91 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core90.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core91.ijtag_so;
   }
   Instance core92 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core92.ijtag_to_sel;
      InputPort ijtag_si    = sib_core91.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core92 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core91.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core92.ijtag_so;
   }
   Instance core93 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core93.ijtag_to_sel;
      InputPort ijtag_si    = sib_core92.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core93 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core92.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core93.ijtag_so;
   }
   Instance core94 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core94.ijtag_to_sel;
      InputPort ijtag_si    = sib_core93.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core94 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core93.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core94.ijtag_so;
   }
   Instance core95 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core95.ijtag_to_sel;
      InputPort ijtag_si    = sib_core94.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core95 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core94.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core95.ijtag_so;
   }
   Instance core96 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core96.ijtag_to_sel;
      InputPort ijtag_si    = sib_core95.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core96 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core95.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core96.ijtag_so;
   }
   Instance core97 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core97.ijtag_to_sel;
      InputPort ijtag_si    = sib_core96.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core97 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core96.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core97.ijtag_so;
   }
   Instance core98 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core98.ijtag_to_sel;
      InputPort ijtag_si    = sib_core97.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core98 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core97.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core98.ijtag_so;
   }
   Instance core99 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core99.ijtag_to_sel;
      InputPort ijtag_si    = sib_core98.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core99 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core98.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core99.ijtag_so;
   }
}
