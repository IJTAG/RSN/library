// Copyright 2018: Mentor, a Siemens Business
// Benchmark for a chip containing 5 cores
// Each core contains 20 memory BIST controllers
// Each memory BIST controller tests 5 memories



Module memory_1 {
   DataInPort AR[3:0];
   DataInPort AW[3:0];
   ClockPort CLKR;
   ClockPort CLKW;
}


Module mbist_interface {
   ClockPort BIST_CLK;
   DataInPort BIST_EN;
   DataInPort BIST_ASYNC_RESETN;
   ScanInPort BIST_SI;
   ScanOutPort BIST_SO {
      Source reg3[0];
   }
   ShiftEnPort BIST_SHIFT_COLLAR;
   DataInPort BIST_CONTROL;
   DataOutPort AR[3:0];
   DataOutPort AW[3:0];

   ScanRegister reg1[0:0] {
      ScanInSource BIST_SI;
   }
   ScanRegister reg2[0:3] {
      ScanInSource reg1[0];
   }
   ScanRegister reg3[7:0] {
      ScanInSource reg2[3];
   }
}

Module sib_1 {
   ResetPort ijtag_reset {
      ActivePolarity 0;
   }
   SelectPort ijtag_sel;
   ScanInPort ijtag_si;
   CaptureEnPort ijtag_ce;
   ShiftEnPort ijtag_se;
   UpdateEnPort ijtag_ue;
   TCKPort ijtag_tck;
   ScanOutPort ijtag_so {
      Source sib;
   }
   ToSelectPort ijtag_to_sel;
   ScanInPort ijtag_from_so;
   ScanInterface client {
      Port ijtag_si;
      Port ijtag_so;
      Port ijtag_sel;
   }
   ScanInterface host {
      Port ijtag_from_so;
      Port ijtag_to_sel;
   }
   ScanRegister sib {
      ScanInSource scan_in_mux;
      CaptureSource 1'b0;
      ResetValue 1'b0;
   }
   ScanMux scan_in_mux SelectedBy sib {
      1'b0 : ijtag_si;
      1'b1 : ijtag_from_so;
   }
}
Module sib_2 {
   ResetPort ijtag_reset {
      ActivePolarity 0;
   }
   SelectPort ijtag_sel;
   ScanInPort ijtag_si;
   CaptureEnPort ijtag_ce;
   ShiftEnPort ijtag_se;
   UpdateEnPort ijtag_ue;
   TCKPort ijtag_tck;
   
   ToResetPort to_ijtag_reset {
      ActivePolarity 0;
      Source ijtag_reset;
   }
   ScanOutPort to_ijtag_si {
      Source ijtag_si;
   }
   ToCaptureEnPort to_ijtag_ce {
      Source ijtag_ce;
   }
   ToShiftEnPort to_ijtag_se {
      Source ijtag_se;
   }
   ToUpdateEnPort to_ijtag_ue {
      Source ijtag_ue;
   }
   ToTCKPort to_ijtag_tck;
   
   ScanOutPort ijtag_so {
      Source sib;
   }
   ToSelectPort ijtag_to_sel;
   ScanInPort ijtag_from_so;

   ScanInterface client {
      Port ijtag_si;
      Port ijtag_so;
      Port ijtag_sel;
   }
   ScanInterface host {
      Port ijtag_from_so;
      Port ijtag_to_sel;
      Port to_ijtag_si;
   }
   ScanRegister sib {
      ScanInSource scan_in_mux;
      CaptureSource 1'b0;
      ResetValue 1'b0;
   }
   ScanMux scan_in_mux SelectedBy sib {
      1'b0 : ijtag_si;
      1'b1 : ijtag_from_so;
   }
}
Module  mbist_bap {
   ResetPort reset {
      ActivePolarity 0;
   }
   SelectPort ijtag_select;
   ScanInPort si;
   CaptureEnPort capture_en;
   ShiftEnPort shift_en;
   ToShiftEnPort shift_en_R;
   UpdateEnPort update_en;
   TCKPort tck;

   DataOutPort setting1 {
      Source BIST_setting1_tdr;
      RefEnum OnOff;
   }
   DataOutPort setting2 {
      Source BIST_setting2_tdr;
      RefEnum OnOff;
   }
   DataOutPort setting3 {
      Source BIST_setting3_tdr;
      RefEnum OnOff;
   }

   ScanOutPort so {
      Source sib_0;
   }
   
   ScanOutPort toBist_19 {
      Source BIST_setting3_tdr;    }
   ScanOutPort toBist_0 {
      Source sib_1;
   }
   ScanOutPort toBist_1 {
      Source sib_2;
   }
   ScanOutPort toBist_2 {
      Source sib_3;
   }
   ScanOutPort toBist_3 {
      Source sib_4;
   }
   ScanOutPort toBist_4 {
      Source sib_5;
   }
   ScanOutPort toBist_5 {
      Source sib_6;
   }
   ScanOutPort toBist_6 {
      Source sib_7;
   }
   ScanOutPort toBist_7 {
      Source sib_8;
   }
   ScanOutPort toBist_8 {
      Source sib_9;
   }
   ScanOutPort toBist_9 {
      Source sib_10;
   }
   ScanOutPort toBist_10 {
      Source sib_11;
   }
   ScanOutPort toBist_11 {
      Source sib_12;
   }
   ScanOutPort toBist_12 {
      Source sib_13;
   }
   ScanOutPort toBist_13 {
      Source sib_14;
   }
   ScanOutPort toBist_14 {
      Source sib_15;
   }
   ScanOutPort toBist_15 {
      Source sib_16;
   }
   ScanOutPort toBist_16 {
      Source sib_17;
   }
   ScanOutPort toBist_17 {
      Source sib_18;
   }
   ScanOutPort toBist_18 {
      Source sib_19;
   }
   ScanInPort fromBist_0;
   DataOutPort bistEn_0 {
      Source sib_0;
    }
   ScanInPort fromBist_1;
   DataOutPort bistEn_1 {
      Source sib_1;
    }
   ScanInPort fromBist_2;
   DataOutPort bistEn_2 {
      Source sib_2;
    }
   ScanInPort fromBist_3;
   DataOutPort bistEn_3 {
      Source sib_3;
    }
   ScanInPort fromBist_4;
   DataOutPort bistEn_4 {
      Source sib_4;
    }
   ScanInPort fromBist_5;
   DataOutPort bistEn_5 {
      Source sib_5;
    }
   ScanInPort fromBist_6;
   DataOutPort bistEn_6 {
      Source sib_6;
    }
   ScanInPort fromBist_7;
   DataOutPort bistEn_7 {
      Source sib_7;
    }
   ScanInPort fromBist_8;
   DataOutPort bistEn_8 {
      Source sib_8;
    }
   ScanInPort fromBist_9;
   DataOutPort bistEn_9 {
      Source sib_9;
    }
   ScanInPort fromBist_10;
   DataOutPort bistEn_10 {
      Source sib_10;
    }
   ScanInPort fromBist_11;
   DataOutPort bistEn_11 {
      Source sib_11;
    }
   ScanInPort fromBist_12;
   DataOutPort bistEn_12 {
      Source sib_12;
    }
   ScanInPort fromBist_13;
   DataOutPort bistEn_13 {
      Source sib_13;
    }
   ScanInPort fromBist_14;
   DataOutPort bistEn_14 {
      Source sib_14;
    }
   ScanInPort fromBist_15;
   DataOutPort bistEn_15 {
      Source sib_15;
    }
   ScanInPort fromBist_16;
   DataOutPort bistEn_16 {
      Source sib_16;
    }
   ScanInPort fromBist_17;
   DataOutPort bistEn_17 {
      Source sib_17;
    }
   ScanInPort fromBist_18;
   DataOutPort bistEn_18 {
      Source sib_18;
    }
   ScanInPort fromBist_19;
   DataOutPort bistEn_19 {
      Source sib_19;
    }
   DataInPort MBISTPG_GO_0;
   DataInPort MBISTPG_DONE_0;
   DataInPort MBISTPG_GO_1;
   DataInPort MBISTPG_DONE_1;
   DataInPort MBISTPG_GO_2;
   DataInPort MBISTPG_DONE_2;
   DataInPort MBISTPG_GO_3;
   DataInPort MBISTPG_DONE_3;
   DataInPort MBISTPG_GO_4;
   DataInPort MBISTPG_DONE_4;
   DataInPort MBISTPG_GO_5;
   DataInPort MBISTPG_DONE_5;
   DataInPort MBISTPG_GO_6;
   DataInPort MBISTPG_DONE_6;
   DataInPort MBISTPG_GO_7;
   DataInPort MBISTPG_DONE_7;
   DataInPort MBISTPG_GO_8;
   DataInPort MBISTPG_DONE_8;
   DataInPort MBISTPG_GO_9;
   DataInPort MBISTPG_DONE_9;
   DataInPort MBISTPG_GO_10;
   DataInPort MBISTPG_DONE_10;
   DataInPort MBISTPG_GO_11;
   DataInPort MBISTPG_DONE_11;
   DataInPort MBISTPG_GO_12;
   DataInPort MBISTPG_DONE_12;
   DataInPort MBISTPG_GO_13;
   DataInPort MBISTPG_DONE_13;
   DataInPort MBISTPG_GO_14;
   DataInPort MBISTPG_DONE_14;
   DataInPort MBISTPG_GO_15;
   DataInPort MBISTPG_DONE_15;
   DataInPort MBISTPG_GO_16;
   DataInPort MBISTPG_DONE_16;
   DataInPort MBISTPG_GO_17;
   DataInPort MBISTPG_DONE_17;
   DataInPort MBISTPG_GO_18;
   DataInPort MBISTPG_DONE_18;
   DataInPort MBISTPG_GO_19;
   DataInPort MBISTPG_DONE_19;

   DataOutPort TCK_MODE {
      Source TCK_MODE_tdr;
      RefEnum OnOff;
   }
   DataOutPort BIST_ASYNC_RESET {
      Source BIST_ASYNC_RESET_tdr;
      RefEnum OnOff;
   }
   DataOutPort BIST_SETUP[2:0] {
      Source BIST_SETUP_tdr[2:0];
   }
   ScanInterface client {
      Port si;
      Port so;
      Port ijtag_select;
   }
   
   ScanInterface host_0 {
      Port toBist_0;
      Port fromBist_0;
      Port shift_en_R;
   }
   ScanInterface host_1 {
      Port toBist_1;
      Port fromBist_1;
      Port shift_en_R;
   }
   ScanInterface host_2 {
      Port toBist_2;
      Port fromBist_2;
      Port shift_en_R;
   }
   ScanInterface host_3 {
      Port toBist_3;
      Port fromBist_3;
      Port shift_en_R;
   }
   ScanInterface host_4 {
      Port toBist_4;
      Port fromBist_4;
      Port shift_en_R;
   }
   ScanInterface host_5 {
      Port toBist_5;
      Port fromBist_5;
      Port shift_en_R;
   }
   ScanInterface host_6 {
      Port toBist_6;
      Port fromBist_6;
      Port shift_en_R;
   }
   ScanInterface host_7 {
      Port toBist_7;
      Port fromBist_7;
      Port shift_en_R;
   }
   ScanInterface host_8 {
      Port toBist_8;
      Port fromBist_8;
      Port shift_en_R;
   }
   ScanInterface host_9 {
      Port toBist_9;
      Port fromBist_9;
      Port shift_en_R;
   }
   ScanInterface host_10 {
      Port toBist_10;
      Port fromBist_10;
      Port shift_en_R;
   }
   ScanInterface host_11 {
      Port toBist_11;
      Port fromBist_11;
      Port shift_en_R;
   }
   ScanInterface host_12 {
      Port toBist_12;
      Port fromBist_12;
      Port shift_en_R;
   }
   ScanInterface host_13 {
      Port toBist_13;
      Port fromBist_13;
      Port shift_en_R;
   }
   ScanInterface host_14 {
      Port toBist_14;
      Port fromBist_14;
      Port shift_en_R;
   }
   ScanInterface host_15 {
      Port toBist_15;
      Port fromBist_15;
      Port shift_en_R;
   }
   ScanInterface host_16 {
      Port toBist_16;
      Port fromBist_16;
      Port shift_en_R;
   }
   ScanInterface host_17 {
      Port toBist_17;
      Port fromBist_17;
      Port shift_en_R;
   }
   ScanInterface host_18 {
      Port toBist_18;
      Port fromBist_18;
      Port shift_en_R;
   }
   ScanInterface host_19 {
      Port toBist_19;
      Port fromBist_19;
      Port shift_en_R;
   }
   Enum OnOff {
      ON = 1'b1;
      OFF = 1'b0;
   }
   ScanRegister tdr[30:0] {
      ScanInSource si;
      CaptureSource MBISTPG_DONE_19, MBISTPG_DONE_18, MBISTPG_DONE_17, MBISTPG_DONE_16, MBISTPG_DONE_15, MBISTPG_DONE_14, MBISTPG_DONE_13, MBISTPG_DONE_12, MBISTPG_DONE_11, MBISTPG_DONE_10, MBISTPG_DONE_9, MBISTPG_DONE_8, MBISTPG_DONE_7, MBISTPG_DONE_6, MBISTPG_DONE_5, MBISTPG_DONE_4, MBISTPG_DONE_3, MBISTPG_DONE_2, MBISTPG_DONE_1, MBISTPG_DONE_0, MBISTPG_GO_19, MBISTPG_GO_18, MBISTPG_GO_17, MBISTPG_GO_16, MBISTPG_GO_15, MBISTPG_GO_14, MBISTPG_GO_13, MBISTPG_GO_12, MBISTPG_GO_11, MBISTPG_GO_10, MBISTPG_GO_9;
   }
   ScanRegister BIST_SETUP_tdr[2:0] {
      ScanInSource tdr[0];
      CaptureSource MBISTPG_GO_8, MBISTPG_GO_7, MBISTPG_GO_6;
      DefaultLoadValue 3'b000;
      ResetValue 3'b000;
   }
   ScanRegister TCK_MODE_tdr {
      ScanInSource BIST_SETUP_tdr[0];
      CaptureSource MBISTPG_GO_5;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister CHAIN_BYPASS_EN_tdr {
      ScanInSource TCK_MODE_tdr;
      CaptureSource MBISTPG_GO_4;
      DefaultLoadValue 1'b1;
      ResetValue 1'b1;
   }
   ScanRegister BIST_ASYNC_RESET_tdr {
      ScanInSource CHAIN_BYPASS_EN_tdr;
      CaptureSource MBISTPG_GO_3;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister BIST_setting1_tdr {
      ScanInSource BIST_ASYNC_RESET_tdr;
      CaptureSource MBISTPG_GO_2;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister BIST_setting2_tdr {
      ScanInSource BIST_setting1_tdr;
      CaptureSource MBISTPG_GO_1;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister BIST_setting3_tdr {
      ScanInSource BIST_setting2_tdr;
      CaptureSource MBISTPG_GO_0;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_0 {
      ScanInSource fromBistMux_0;
      CaptureSource sib_0;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_1 {
      ScanInSource fromBistMux_1;
      CaptureSource sib_1;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_2 {
      ScanInSource fromBistMux_2;
      CaptureSource sib_2;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_3 {
      ScanInSource fromBistMux_3;
      CaptureSource sib_3;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_4 {
      ScanInSource fromBistMux_4;
      CaptureSource sib_4;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_5 {
      ScanInSource fromBistMux_5;
      CaptureSource sib_5;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_6 {
      ScanInSource fromBistMux_6;
      CaptureSource sib_6;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_7 {
      ScanInSource fromBistMux_7;
      CaptureSource sib_7;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_8 {
      ScanInSource fromBistMux_8;
      CaptureSource sib_8;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_9 {
      ScanInSource fromBistMux_9;
      CaptureSource sib_9;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_10 {
      ScanInSource fromBistMux_10;
      CaptureSource sib_10;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_11 {
      ScanInSource fromBistMux_11;
      CaptureSource sib_11;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_12 {
      ScanInSource fromBistMux_12;
      CaptureSource sib_12;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_13 {
      ScanInSource fromBistMux_13;
      CaptureSource sib_13;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_14 {
      ScanInSource fromBistMux_14;
      CaptureSource sib_14;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_15 {
      ScanInSource fromBistMux_15;
      CaptureSource sib_15;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_16 {
      ScanInSource fromBistMux_16;
      CaptureSource sib_16;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_17 {
      ScanInSource fromBistMux_17;
      CaptureSource sib_17;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_18 {
      ScanInSource fromBistMux_18;
      CaptureSource sib_18;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_19 {
      ScanInSource fromBistMux_19;
      CaptureSource sib_19;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanMux fromBistMux_19 SelectedBy sib_19, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_19;
      3'bxxx : BIST_setting3_tdr;
   }
   ScanMux fromBistMux_0 SelectedBy sib_0, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_0;
      3'bxxx : sib_1;
   }
   ScanMux fromBistMux_1 SelectedBy sib_1, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_1;
      3'bxxx : sib_2;
   }
   ScanMux fromBistMux_2 SelectedBy sib_2, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_2;
      3'bxxx : sib_3;
   }
   ScanMux fromBistMux_3 SelectedBy sib_3, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_3;
      3'bxxx : sib_4;
   }
   ScanMux fromBistMux_4 SelectedBy sib_4, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_4;
      3'bxxx : sib_5;
   }
   ScanMux fromBistMux_5 SelectedBy sib_5, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_5;
      3'bxxx : sib_6;
   }
   ScanMux fromBistMux_6 SelectedBy sib_6, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_6;
      3'bxxx : sib_7;
   }
   ScanMux fromBistMux_7 SelectedBy sib_7, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_7;
      3'bxxx : sib_8;
   }
   ScanMux fromBistMux_8 SelectedBy sib_8, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_8;
      3'bxxx : sib_9;
   }
   ScanMux fromBistMux_9 SelectedBy sib_9, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_9;
      3'bxxx : sib_10;
   }
   ScanMux fromBistMux_10 SelectedBy sib_10, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_10;
      3'bxxx : sib_11;
   }
   ScanMux fromBistMux_11 SelectedBy sib_11, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_11;
      3'bxxx : sib_12;
   }
   ScanMux fromBistMux_12 SelectedBy sib_12, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_12;
      3'bxxx : sib_13;
   }
   ScanMux fromBistMux_13 SelectedBy sib_13, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_13;
      3'bxxx : sib_14;
   }
   ScanMux fromBistMux_14 SelectedBy sib_14, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_14;
      3'bxxx : sib_15;
   }
   ScanMux fromBistMux_15 SelectedBy sib_15, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_15;
      3'bxxx : sib_16;
   }
   ScanMux fromBistMux_16 SelectedBy sib_16, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_16;
      3'bxxx : sib_17;
   }
   ScanMux fromBistMux_17 SelectedBy sib_17, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_17;
      3'bxxx : sib_18;
   }
   ScanMux fromBistMux_18 SelectedBy sib_18, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist_18;
      3'bxxx : sib_19;
   }
}

Module mbist_controller {
   ClockPort BIST_CLK;
   DataInPort MBISTPG_EN;

   DataInPort MBISTPG_ASYNC_RESETN;

   DataInPort BIST_CONTROL[2:0];
   DataInPort TCK_MODE;
   DataInPort setting1 {
      RefEnum OnOff;
   }
   DataInPort setting2 {
      RefEnum OnOff;
   }
   DataInPort setting3 {
      RefEnum OnOff;
   }
   DataOutPort MBISTPG_GO {
      RefEnum PassFail;
   }
   DataOutPort MBISTPG_DONE {
      RefEnum PassFail;
   }
   TCKPort TCK;
   ScanInPort BIST_SI;
   ScanOutPort MBISTPG_SO {
      Source CONTROLLER_SETUP_CHAIN;
   }
   ShiftEnPort BIST_SHIFT;
   ToShiftEnPort BIST_SHIFT_COLLAR;
   ScanOutPort MEM0_BIST_COLLAR_SI {
      Source MEM1_BIST_COLLAR_SO;
   }
   ScanInPort MEM0_BIST_COLLAR_SO;
   ScanOutPort MEM1_BIST_COLLAR_SI {
      Source MEM2_BIST_COLLAR_SO;
   }
   ScanInPort MEM1_BIST_COLLAR_SO;
   ScanOutPort MEM2_BIST_COLLAR_SI {
      Source MEM3_BIST_COLLAR_SO;
   }
   ScanInPort MEM2_BIST_COLLAR_SO;
   ScanOutPort MEM3_BIST_COLLAR_SI {
      Source MEM4_BIST_COLLAR_SO;
   }
   ScanInPort MEM3_BIST_COLLAR_SO;
   ScanOutPort MEM4_BIST_COLLAR_SI {
      Source BIST_TO_COLLAR_SO_MUX;
   }
   ScanInPort MEM4_BIST_COLLAR_SO;

   ScanInterface Client {
      Port BIST_SI;
      Port MBISTPG_SO;
      Port BIST_SHIFT;
   }

   ScanInterface MEM0_INTERFACE {
      Port BIST_SHIFT_COLLAR;
      Port MEM0_BIST_COLLAR_SI;
      Port MEM0_BIST_COLLAR_SO;
   }
   ScanInterface MEM1_INTERFACE {
      Port BIST_SHIFT_COLLAR;
      Port MEM1_BIST_COLLAR_SI;
      Port MEM1_BIST_COLLAR_SO;
   }
   ScanInterface MEM2_INTERFACE {
      Port BIST_SHIFT_COLLAR;
      Port MEM2_BIST_COLLAR_SI;
      Port MEM2_BIST_COLLAR_SO;
   }
   ScanInterface MEM3_INTERFACE {
      Port BIST_SHIFT_COLLAR;
      Port MEM3_BIST_COLLAR_SI;
      Port MEM3_BIST_COLLAR_SO;
   }
   ScanInterface MEM4_INTERFACE {
      Port BIST_SHIFT_COLLAR;
      Port MEM4_BIST_COLLAR_SI;
      Port MEM4_BIST_COLLAR_SO;
   }




   Enum PassFail {
      Pass = 1'b1;
      Fail = 1'b0;
      Ignore = 1'bx;
   }
   Enum OnOff {
      ON  = 1'b1;
      OFF = 1'b0;
   }

   ScanRegister MBIST_SETTINGS1[9:0] {
      ScanInSource BIST_SI;
   }
   ScanRegister MBIST_SETTINGS2[9:0] {
      ScanInSource MBIST_SETTINGS1[0];
   }
   ScanRegister MBIST_SETTINGS3[9:0] {
      ScanInSource MBIST_SETTINGS2[0];
   }

   ScanRegister MBIST_RESULT[9:0] {
      ScanInSource MEM0_BIST_COLLAR_SO;
   }

   ScanMux BIST_TO_COLLAR_SO_MUX SelectedBy mode1, mode2 {
      2'b01 : MBIST_SETTINGS2[0];
      2'b10 : MBIST_SETTINGS3[0];
   }
   ScanMux CONTROLLER_SETUP_CHAIN SelectedBy MBISTPG_EN, BIST_CONTROL {
      1'b1, 3'b00x : MBIST_RESULT[0];
   }
   LogicSignal mode1 {
      MBISTPG_EN, BIST_CONTROL == 1'b1, 3'b001;
   }
   LogicSignal mode2 {
      MBISTPG_EN, BIST_CONTROL == 1'b1, 3'b000;
   }
}


Module core {
   ClockPort clk;
   CaptureEnPort ijtag_ce;
   ResetPort ijtag_reset {
      ActivePolarity 0;
   }
   ShiftEnPort ijtag_se;
   SelectPort ijtag_sel;
   ScanInPort ijtag_si;
   ScanOutPort ijtag_so {
      Source sib_inst.ijtag_so;
   }
   TCKPort ijtag_tck;
   UpdateEnPort ijtag_ue;
   ScanInterface ijtag {
      Port ijtag_ce;
      Port ijtag_reset;
      Port ijtag_se;
      Port ijtag_sel;
      Port ijtag_si;
      Port ijtag_so;
      Port ijtag_tck;
      Port ijtag_ue;
   }
   

   // Memory block 0
   Instance memory_block_0_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_0_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_0_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_0_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_0_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_0_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_0_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_0_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_0_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_0_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_0;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_0.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_0.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_0_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_0_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_0_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_0_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_0_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_0_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_0_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_0_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_0_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_0_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_0;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_0.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_0.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_0_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_0_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_0_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_0_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_0_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_0_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_0_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_0_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_0_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_0_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_0;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_0.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_0.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_0_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_0_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_0_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_0_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_0_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_0_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_0_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_0_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_0_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_0_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_0;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_0.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_0.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_0_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_0_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_0_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_0_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_0_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_0_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_0_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_0_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_0_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_0_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_0;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_0.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_0.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_0 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_0;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_0;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_0_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_0_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_0_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_0_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_0_inst_4.BIST_SO;
   }

   // Memory block 1
   Instance memory_block_1_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_1_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_1_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_1_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_1_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_1_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_1_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_1_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_1_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_1_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_1;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_1.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_1.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_1_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_1_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_1_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_1_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_1_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_1_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_1_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_1_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_1_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_1_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_1;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_1.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_1.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_1_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_1_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_1_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_1_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_1_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_1_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_1_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_1_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_1_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_1_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_1;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_1.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_1.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_1_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_1_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_1_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_1_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_1_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_1_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_1_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_1_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_1_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_1_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_1;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_1.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_1.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_1_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_1_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_1_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_1_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_1_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_1_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_1_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_1_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_1_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_1_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_1;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_1.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_1.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_1 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_1;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_1;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_1_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_1_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_1_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_1_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_1_inst_4.BIST_SO;
   }

   // Memory block 2
   Instance memory_block_2_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_2_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_2_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_2_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_2_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_2_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_2_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_2_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_2_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_2_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_2;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_2.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_2.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_2_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_2_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_2_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_2_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_2_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_2_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_2_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_2_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_2_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_2_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_2;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_2.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_2.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_2_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_2_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_2_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_2_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_2_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_2_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_2_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_2_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_2_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_2_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_2;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_2.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_2.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_2_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_2_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_2_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_2_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_2_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_2_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_2_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_2_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_2_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_2_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_2;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_2.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_2.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_2_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_2_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_2_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_2_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_2_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_2_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_2_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_2_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_2_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_2_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_2;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_2.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_2.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_2 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_2;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_2;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_2_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_2_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_2_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_2_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_2_inst_4.BIST_SO;
   }

   // Memory block 3
   Instance memory_block_3_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_3_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_3_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_3_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_3_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_3_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_3_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_3_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_3_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_3_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_3;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_3.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_3.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_3_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_3_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_3_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_3_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_3_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_3_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_3_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_3_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_3_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_3_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_3;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_3.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_3.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_3_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_3_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_3_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_3_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_3_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_3_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_3_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_3_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_3_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_3_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_3;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_3.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_3.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_3_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_3_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_3_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_3_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_3_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_3_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_3_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_3_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_3_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_3_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_3;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_3.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_3.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_3_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_3_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_3_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_3_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_3_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_3_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_3_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_3_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_3_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_3_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_3;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_3.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_3.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_3 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_3;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_3;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_3_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_3_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_3_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_3_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_3_inst_4.BIST_SO;
   }

   // Memory block 4
   Instance memory_block_4_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_4_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_4_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_4_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_4_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_4_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_4_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_4_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_4_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_4_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_4;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_4.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_4.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_4_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_4_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_4_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_4_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_4_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_4_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_4_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_4_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_4_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_4_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_4;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_4.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_4.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_4_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_4_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_4_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_4_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_4_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_4_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_4_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_4_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_4_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_4_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_4;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_4.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_4.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_4_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_4_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_4_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_4_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_4_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_4_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_4_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_4_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_4_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_4_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_4;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_4.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_4.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_4_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_4_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_4_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_4_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_4_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_4_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_4_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_4_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_4_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_4_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_4;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_4.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_4.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_4 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_4;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_4;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_4_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_4_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_4_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_4_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_4_inst_4.BIST_SO;
   }

   // Memory block 5
   Instance memory_block_5_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_5_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_5_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_5_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_5_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_5_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_5_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_5_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_5_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_5_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_5;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_5.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_5.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_5_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_5_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_5_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_5_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_5_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_5_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_5_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_5_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_5_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_5_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_5;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_5.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_5.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_5_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_5_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_5_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_5_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_5_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_5_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_5_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_5_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_5_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_5_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_5;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_5.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_5.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_5_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_5_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_5_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_5_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_5_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_5_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_5_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_5_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_5_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_5_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_5;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_5.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_5.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_5_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_5_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_5_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_5_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_5_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_5_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_5_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_5_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_5_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_5_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_5;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_5.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_5.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_5 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_5;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_5;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_5_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_5_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_5_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_5_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_5_inst_4.BIST_SO;
   }

   // Memory block 6
   Instance memory_block_6_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_6_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_6_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_6_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_6_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_6_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_6_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_6_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_6_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_6_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_6;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_6.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_6.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_6_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_6_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_6_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_6_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_6_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_6_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_6_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_6_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_6_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_6_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_6;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_6.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_6.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_6_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_6_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_6_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_6_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_6_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_6_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_6_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_6_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_6_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_6_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_6;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_6.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_6.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_6_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_6_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_6_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_6_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_6_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_6_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_6_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_6_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_6_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_6_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_6;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_6.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_6.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_6_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_6_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_6_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_6_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_6_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_6_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_6_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_6_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_6_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_6_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_6;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_6.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_6.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_6 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_6;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_6;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_6_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_6_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_6_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_6_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_6_inst_4.BIST_SO;
   }

   // Memory block 7
   Instance memory_block_7_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_7_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_7_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_7_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_7_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_7_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_7_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_7_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_7_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_7_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_7;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_7.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_7.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_7_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_7_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_7_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_7_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_7_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_7_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_7_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_7_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_7_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_7_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_7;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_7.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_7.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_7_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_7_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_7_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_7_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_7_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_7_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_7_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_7_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_7_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_7_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_7;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_7.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_7.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_7_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_7_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_7_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_7_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_7_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_7_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_7_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_7_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_7_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_7_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_7;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_7.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_7.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_7_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_7_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_7_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_7_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_7_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_7_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_7_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_7_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_7_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_7_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_7;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_7.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_7.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_7 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_7;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_7;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_7_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_7_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_7_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_7_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_7_inst_4.BIST_SO;
   }

   // Memory block 8
   Instance memory_block_8_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_8_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_8_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_8_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_8_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_8_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_8_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_8_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_8_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_8_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_8;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_8.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_8.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_8_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_8_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_8_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_8_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_8_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_8_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_8_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_8_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_8_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_8_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_8;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_8.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_8.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_8_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_8_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_8_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_8_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_8_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_8_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_8_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_8_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_8_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_8_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_8;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_8.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_8.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_8_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_8_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_8_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_8_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_8_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_8_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_8_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_8_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_8_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_8_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_8;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_8.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_8.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_8_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_8_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_8_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_8_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_8_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_8_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_8_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_8_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_8_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_8_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_8;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_8.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_8.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_8 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_8;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_8;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_8_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_8_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_8_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_8_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_8_inst_4.BIST_SO;
   }

   // Memory block 9
   Instance memory_block_9_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_9_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_9_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_9_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_9_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_9_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_9_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_9_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_9_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_9_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_9;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_9.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_9.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_9_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_9_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_9_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_9_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_9_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_9_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_9_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_9_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_9_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_9_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_9;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_9.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_9.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_9_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_9_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_9_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_9_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_9_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_9_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_9_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_9_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_9_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_9_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_9;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_9.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_9.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_9_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_9_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_9_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_9_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_9_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_9_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_9_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_9_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_9_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_9_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_9;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_9.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_9.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_9_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_9_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_9_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_9_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_9_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_9_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_9_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_9_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_9_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_9_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_9;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_9.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_9.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_9 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_9;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_9;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_9_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_9_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_9_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_9_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_9_inst_4.BIST_SO;
   }

   // Memory block 10
   Instance memory_block_10_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_10_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_10_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_10_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_10_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_10_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_10_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_10_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_10_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_10_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_10;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_10.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_10.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_10_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_10_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_10_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_10_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_10_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_10_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_10_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_10_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_10_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_10_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_10;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_10.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_10.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_10_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_10_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_10_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_10_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_10_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_10_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_10_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_10_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_10_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_10_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_10;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_10.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_10.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_10_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_10_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_10_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_10_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_10_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_10_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_10_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_10_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_10_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_10_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_10;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_10.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_10.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_10_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_10_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_10_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_10_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_10_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_10_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_10_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_10_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_10_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_10_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_10;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_10.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_10.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_10 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_10;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_10;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_10_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_10_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_10_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_10_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_10_inst_4.BIST_SO;
   }

   // Memory block 11
   Instance memory_block_11_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_11_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_11_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_11_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_11_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_11_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_11_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_11_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_11_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_11_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_11;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_11.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_11.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_11_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_11_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_11_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_11_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_11_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_11_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_11_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_11_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_11_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_11_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_11;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_11.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_11.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_11_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_11_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_11_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_11_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_11_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_11_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_11_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_11_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_11_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_11_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_11;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_11.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_11.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_11_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_11_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_11_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_11_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_11_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_11_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_11_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_11_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_11_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_11_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_11;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_11.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_11.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_11_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_11_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_11_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_11_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_11_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_11_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_11_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_11_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_11_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_11_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_11;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_11.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_11.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_11 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_11;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_11;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_11_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_11_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_11_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_11_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_11_inst_4.BIST_SO;
   }

   // Memory block 12
   Instance memory_block_12_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_12_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_12_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_12_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_12_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_12_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_12_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_12_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_12_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_12_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_12;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_12.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_12.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_12_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_12_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_12_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_12_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_12_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_12_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_12_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_12_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_12_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_12_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_12;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_12.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_12.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_12_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_12_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_12_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_12_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_12_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_12_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_12_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_12_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_12_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_12_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_12;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_12.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_12.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_12_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_12_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_12_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_12_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_12_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_12_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_12_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_12_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_12_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_12_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_12;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_12.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_12.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_12_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_12_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_12_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_12_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_12_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_12_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_12_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_12_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_12_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_12_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_12;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_12.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_12.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_12 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_12;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_12;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_12_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_12_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_12_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_12_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_12_inst_4.BIST_SO;
   }

   // Memory block 13
   Instance memory_block_13_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_13_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_13_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_13_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_13_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_13_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_13_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_13_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_13_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_13_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_13;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_13.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_13.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_13_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_13_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_13_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_13_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_13_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_13_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_13_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_13_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_13_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_13_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_13;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_13.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_13.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_13_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_13_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_13_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_13_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_13_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_13_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_13_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_13_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_13_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_13_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_13;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_13.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_13.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_13_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_13_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_13_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_13_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_13_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_13_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_13_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_13_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_13_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_13_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_13;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_13.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_13.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_13_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_13_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_13_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_13_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_13_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_13_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_13_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_13_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_13_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_13_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_13;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_13.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_13.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_13 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_13;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_13;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_13_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_13_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_13_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_13_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_13_inst_4.BIST_SO;
   }

   // Memory block 14
   Instance memory_block_14_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_14_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_14_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_14_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_14_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_14_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_14_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_14_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_14_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_14_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_14;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_14.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_14.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_14_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_14_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_14_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_14_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_14_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_14_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_14_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_14_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_14_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_14_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_14;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_14.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_14.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_14_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_14_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_14_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_14_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_14_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_14_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_14_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_14_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_14_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_14_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_14;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_14.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_14.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_14_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_14_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_14_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_14_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_14_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_14_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_14_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_14_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_14_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_14_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_14;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_14.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_14.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_14_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_14_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_14_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_14_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_14_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_14_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_14_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_14_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_14_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_14_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_14;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_14.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_14.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_14 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_14;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_14;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_14_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_14_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_14_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_14_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_14_inst_4.BIST_SO;
   }

   // Memory block 15
   Instance memory_block_15_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_15_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_15_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_15_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_15_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_15_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_15_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_15_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_15_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_15_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_15;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_15.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_15.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_15_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_15_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_15_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_15_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_15_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_15_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_15_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_15_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_15_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_15_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_15;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_15.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_15.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_15_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_15_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_15_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_15_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_15_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_15_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_15_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_15_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_15_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_15_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_15;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_15.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_15.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_15_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_15_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_15_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_15_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_15_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_15_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_15_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_15_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_15_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_15_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_15;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_15.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_15.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_15_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_15_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_15_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_15_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_15_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_15_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_15_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_15_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_15_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_15_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_15;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_15.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_15.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_15 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_15;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_15;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_15_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_15_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_15_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_15_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_15_inst_4.BIST_SO;
   }

   // Memory block 16
   Instance memory_block_16_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_16_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_16_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_16_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_16_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_16_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_16_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_16_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_16_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_16_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_16;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_16.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_16.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_16_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_16_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_16_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_16_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_16_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_16_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_16_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_16_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_16_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_16_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_16;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_16.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_16.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_16_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_16_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_16_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_16_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_16_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_16_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_16_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_16_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_16_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_16_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_16;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_16.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_16.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_16_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_16_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_16_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_16_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_16_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_16_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_16_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_16_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_16_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_16_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_16;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_16.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_16.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_16_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_16_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_16_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_16_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_16_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_16_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_16_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_16_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_16_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_16_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_16;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_16.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_16.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_16 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_16;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_16;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_16_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_16_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_16_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_16_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_16_inst_4.BIST_SO;
   }

   // Memory block 17
   Instance memory_block_17_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_17_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_17_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_17_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_17_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_17_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_17_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_17_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_17_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_17_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_17;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_17.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_17.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_17_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_17_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_17_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_17_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_17_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_17_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_17_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_17_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_17_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_17_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_17;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_17.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_17.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_17_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_17_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_17_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_17_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_17_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_17_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_17_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_17_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_17_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_17_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_17;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_17.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_17.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_17_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_17_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_17_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_17_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_17_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_17_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_17_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_17_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_17_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_17_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_17;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_17.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_17.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_17_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_17_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_17_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_17_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_17_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_17_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_17_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_17_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_17_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_17_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_17;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_17.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_17.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_17 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_17;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_17;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_17_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_17_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_17_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_17_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_17_inst_4.BIST_SO;
   }

   // Memory block 18
   Instance memory_block_18_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_18_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_18_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_18_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_18_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_18_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_18_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_18_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_18_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_18_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_18;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_18.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_18.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_18_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_18_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_18_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_18_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_18_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_18_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_18_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_18_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_18_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_18_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_18;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_18.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_18.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_18_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_18_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_18_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_18_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_18_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_18_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_18_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_18_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_18_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_18_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_18;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_18.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_18.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_18_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_18_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_18_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_18_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_18_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_18_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_18_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_18_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_18_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_18_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_18;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_18.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_18.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_18_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_18_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_18_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_18_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_18_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_18_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_18_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_18_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_18_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_18_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_18;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_18.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_18.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_18 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_18;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_18;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_18_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_18_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_18_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_18_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_18_inst_4.BIST_SO;
   }

   // Memory block 19
   Instance memory_block_19_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_19_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_19_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_19_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_19_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_19_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_19_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_19_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_19_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_19_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_19;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_19.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_19.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_19_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_19_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_19_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_19_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_19_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_19_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_19_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_19_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_19_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_19_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_19;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_19.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_19.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_19_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_19_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_19_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_19_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_19_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_19_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_19_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_19_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_19_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_19_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_19;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_19.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_19.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_19_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_19_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_19_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_19_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_19_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_19_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_19_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_19_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_19_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_19_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_19;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_19.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_19.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_19_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_19_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_19_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_19_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_19_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_19_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_19_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_19_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_19_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_19_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn_19;
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_19.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_19.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_19 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn_19;
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist_19;
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_19_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_19_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_19_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_19_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_19_inst_4.BIST_SO;
   }



// One MBIST BAP for the core
   Instance mbist_bap_inst Of mbist_bap {
      InputPort reset              = sib_inst.to_ijtag_reset;
      InputPort ijtag_select       = sib_mbist_inst.ijtag_to_sel;
      InputPort si                 = sib_inst.to_ijtag_si;
      InputPort capture_en         = sib_inst.to_ijtag_ce;
      InputPort shift_en           = sib_inst.to_ijtag_se;
      InputPort update_en          = sib_inst.to_ijtag_ue;
      InputPort tck                = sib_inst.to_ijtag_tck;
      InputPort fromBist_0 = mbist_controller_block_0.MBISTPG_SO;
      InputPort fromBist_1 = mbist_controller_block_1.MBISTPG_SO;
      InputPort fromBist_2 = mbist_controller_block_2.MBISTPG_SO;
      InputPort fromBist_3 = mbist_controller_block_3.MBISTPG_SO;
      InputPort fromBist_4 = mbist_controller_block_4.MBISTPG_SO;
      InputPort fromBist_5 = mbist_controller_block_5.MBISTPG_SO;
      InputPort fromBist_6 = mbist_controller_block_6.MBISTPG_SO;
      InputPort fromBist_7 = mbist_controller_block_7.MBISTPG_SO;
      InputPort fromBist_8 = mbist_controller_block_8.MBISTPG_SO;
      InputPort fromBist_9 = mbist_controller_block_9.MBISTPG_SO;
      InputPort fromBist_10 = mbist_controller_block_10.MBISTPG_SO;
      InputPort fromBist_11 = mbist_controller_block_11.MBISTPG_SO;
      InputPort fromBist_12 = mbist_controller_block_12.MBISTPG_SO;
      InputPort fromBist_13 = mbist_controller_block_13.MBISTPG_SO;
      InputPort fromBist_14 = mbist_controller_block_14.MBISTPG_SO;
      InputPort fromBist_15 = mbist_controller_block_15.MBISTPG_SO;
      InputPort fromBist_16 = mbist_controller_block_16.MBISTPG_SO;
      InputPort fromBist_17 = mbist_controller_block_17.MBISTPG_SO;
      InputPort fromBist_18 = mbist_controller_block_18.MBISTPG_SO;
      InputPort fromBist_19 = mbist_controller_block_19.MBISTPG_SO;
      InputPort MBISTPG_GO_0 = mbist_controller_block_0.MBISTPG_GO;
      InputPort MBISTPG_GO_1 = mbist_controller_block_1.MBISTPG_GO;
      InputPort MBISTPG_GO_2 = mbist_controller_block_2.MBISTPG_GO;
      InputPort MBISTPG_GO_3 = mbist_controller_block_3.MBISTPG_GO;
      InputPort MBISTPG_GO_4 = mbist_controller_block_4.MBISTPG_GO;
      InputPort MBISTPG_GO_5 = mbist_controller_block_5.MBISTPG_GO;
      InputPort MBISTPG_GO_6 = mbist_controller_block_6.MBISTPG_GO;
      InputPort MBISTPG_GO_7 = mbist_controller_block_7.MBISTPG_GO;
      InputPort MBISTPG_GO_8 = mbist_controller_block_8.MBISTPG_GO;
      InputPort MBISTPG_GO_9 = mbist_controller_block_9.MBISTPG_GO;
      InputPort MBISTPG_GO_10 = mbist_controller_block_10.MBISTPG_GO;
      InputPort MBISTPG_GO_11 = mbist_controller_block_11.MBISTPG_GO;
      InputPort MBISTPG_GO_12 = mbist_controller_block_12.MBISTPG_GO;
      InputPort MBISTPG_GO_13 = mbist_controller_block_13.MBISTPG_GO;
      InputPort MBISTPG_GO_14 = mbist_controller_block_14.MBISTPG_GO;
      InputPort MBISTPG_GO_15 = mbist_controller_block_15.MBISTPG_GO;
      InputPort MBISTPG_GO_16 = mbist_controller_block_16.MBISTPG_GO;
      InputPort MBISTPG_GO_17 = mbist_controller_block_17.MBISTPG_GO;
      InputPort MBISTPG_GO_18 = mbist_controller_block_18.MBISTPG_GO;
      InputPort MBISTPG_GO_19 = mbist_controller_block_19.MBISTPG_GO;
      InputPort MBISTPG_DONE_0 = mbist_controller_block_0.MBISTPG_DONE;
      InputPort MBISTPG_DONE_1 = mbist_controller_block_1.MBISTPG_DONE;
      InputPort MBISTPG_DONE_2 = mbist_controller_block_2.MBISTPG_DONE;
      InputPort MBISTPG_DONE_3 = mbist_controller_block_3.MBISTPG_DONE;
      InputPort MBISTPG_DONE_4 = mbist_controller_block_4.MBISTPG_DONE;
      InputPort MBISTPG_DONE_5 = mbist_controller_block_5.MBISTPG_DONE;
      InputPort MBISTPG_DONE_6 = mbist_controller_block_6.MBISTPG_DONE;
      InputPort MBISTPG_DONE_7 = mbist_controller_block_7.MBISTPG_DONE;
      InputPort MBISTPG_DONE_8 = mbist_controller_block_8.MBISTPG_DONE;
      InputPort MBISTPG_DONE_9 = mbist_controller_block_9.MBISTPG_DONE;
      InputPort MBISTPG_DONE_10 = mbist_controller_block_10.MBISTPG_DONE;
      InputPort MBISTPG_DONE_11 = mbist_controller_block_11.MBISTPG_DONE;
      InputPort MBISTPG_DONE_12 = mbist_controller_block_12.MBISTPG_DONE;
      InputPort MBISTPG_DONE_13 = mbist_controller_block_13.MBISTPG_DONE;
      InputPort MBISTPG_DONE_14 = mbist_controller_block_14.MBISTPG_DONE;
      InputPort MBISTPG_DONE_15 = mbist_controller_block_15.MBISTPG_DONE;
      InputPort MBISTPG_DONE_16 = mbist_controller_block_16.MBISTPG_DONE;
      InputPort MBISTPG_DONE_17 = mbist_controller_block_17.MBISTPG_DONE;
      InputPort MBISTPG_DONE_18 = mbist_controller_block_18.MBISTPG_DONE;
      InputPort MBISTPG_DONE_19 = mbist_controller_block_19.MBISTPG_DONE;
   }
   Instance sib_mbist_inst Of sib_1 {
      InputPort ijtag_reset = sib_inst.to_ijtag_reset;
      InputPort ijtag_sel = sib_inst.ijtag_to_sel;
      InputPort ijtag_si = sib_inst.to_ijtag_si;
      InputPort ijtag_ce = sib_inst.to_ijtag_ce;
      InputPort ijtag_se = sib_inst.to_ijtag_se;
      InputPort ijtag_ue = sib_inst.to_ijtag_ue;
      InputPort ijtag_tck = sib_inst.to_ijtag_tck;
      InputPort ijtag_from_so = mbist_bap_inst.so;
      }
   Instance sib_inst Of sib_2 {
      InputPort ijtag_reset = ijtag_reset;
      InputPort ijtag_sel = ijtag_sel;
      InputPort ijtag_si = ijtag_si;
      InputPort ijtag_ce = ijtag_ce;
      InputPort ijtag_se = ijtag_se;
      InputPort ijtag_ue = ijtag_ue;
      InputPort ijtag_tck = ijtag_tck;
      InputPort ijtag_from_so = sib_mbist_inst.ijtag_so;
   }
}



Module chip_tap_fsm {
   TCKPort tck;
   TMSPort tms;
   TRSTPort trst;
   ToIRSelectPort irSel;
   ToResetPort tlr;
}

Module chip_tap {
   TCKPort tck;
   ScanInPort tdi;
   ScanOutPort tdo {
      Source IRMux;
   }
   TMSPort tms;
   TRSTPort trst;
   ToCaptureEnPort capture_dr_en;
   ToShiftEnPort shift_dr_en;
   ToUpdateEnPort update_dr_en;
   ToResetPort test_logic_reset {
      ActivePolarity 0;
   }
   ToSelectPort host_1_to_sel {
      Source host_1_to_sel_int;
   }
   ScanInPort host_1_from_so;
   DataOutPort force_disable {
      Source force_disable_int;
   }
   DataOutPort select_jtag_input {
      Source select_jtag_input_int;
   }
   DataOutPort select_jtag_output {
      Source select_jtag_output_int;
   }
   DataOutPort extest_pulse {
      Source ext_test_pulse_int;
   }
   DataOutPort extest_train {
      Source ext_test_train_int;
   }
   DataOutPort fsm_state[3:0] {
      RefEnum state_encoding;
   }
   ScanInterface tap_client {
      Port tdi;
      Port tdo;
      Port tms;
   }
   ScanInterface host_ijtag_1 {
      Port host_1_from_so;
      Port host_1_to_sel;
   }
   Enum state_encoding {
      test_logic_reset = 4'b1111;
      run_test_idle = 4'b1100;
      select_dr = 4'b0111;
      capture_dr = 4'b0110;
      shift_dr = 4'b0010;
      exit1_dr = 4'b0001;
      pause_dr = 4'b0011;
      exit2_dr = 4'b0000;
      update_dr = 4'b0101;
      select_ir = 4'b0100;
      capture_ir = 4'b1110;
      shift_ir = 4'b1010;
      exit1_ir = 4'b1001;
      pause_ir = 4'b1011;
      exit2_ir = 4'b1000;
      update_ir = 4'b1101;
   }
   Enum instruction_opcodes {
      BYPASS = 4'b1111;
      CLAMP = 4'b0000;
      EXTEST = 4'b0001;
      EXTEST_PULSE = 4'b0010;
      EXTEST_TRAIN = 4'b0011;
      INTEST = 4'b0100;
      SAMPLE = 4'b0101;
      PRELOAD = 4'b0101;
      HIGHZ = 4'b0110;
      HOSTIJTAG_1 = 4'b0111;
   }
   ScanRegister instruction[3:0] {
      ScanInSource tdi;
      CaptureSource 4'b0001;
      ResetValue 4'b1111;
      RefEnum instruction_opcodes;
   }
   ScanRegister bypass {
      ScanInSource tdi;
      CaptureSource 1'b0;
   }
   ScanMux IRMux SelectedBy fsm.irSel {
      1'b0 : DRMux;
      1'b1 : instruction[0];
   }
   ScanMux DRMux SelectedBy instruction {
      4'b1111 : bypass;
      4'b0000 : bypass;
      4'b0001 : bypass;
      4'b0010 : bypass;
      4'b0011 : bypass;
      4'b0100 : bypass;
      4'b0101 : bypass;
      4'b0110 : bypass;
      4'b0111 : host_1_from_so;
   }
   LogicSignal host_1_to_sel_int {
      instruction == HOSTIJTAG_1;
   }
   LogicSignal force_disable_int {
      instruction == HIGHZ;
   }
   LogicSignal select_jtag_input_int {
      instruction == INTEST;
   }
   LogicSignal select_jtag_output_int {
      ((((instruction == EXTEST) || (instruction == EXTEST_PULSE)) ||
          (instruction == EXTEST_TRAIN)) || (instruction == CLAMP)) ||
          (instruction == HIGHZ);
   }
   LogicSignal ext_test_pulse_int {
      instruction == EXTEST_PULSE;
   }
   LogicSignal ext_test_train_int {
      instruction == EXTEST_TRAIN;
   }
   Instance fsm Of chip_tap_fsm {
      InputPort tck = tck;
      InputPort tms = tms;
      InputPort trst = trst;
   }
}


Module chip {
   ClockPort clk;
   TCKPort tck;
   ScanInPort tdi;
   ScanOutPort tdo {
      Source chip_tap_inst.tdo;
   }
   TMSPort tms;
   TRSTPort trst;
   ScanInterface tap {
      Port tck;
      Port tdi;
      Port tdo;
      Port tms;
      Port trst;
   }


   Instance chip_tap_inst Of chip_tap {
      InputPort tck    = tck;
      InputPort tdi    = tdi;
      InputPort tms    = tms;
      InputPort trst   = trst;
      InputPort host_1_from_so = sib_core4.ijtag_so;
   }
   Instance core0 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core0.ijtag_to_sel;
      InputPort ijtag_si    = tdi;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core0 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = tdi;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core0.ijtag_so;
   }
   Instance core1 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core1.ijtag_to_sel;
      InputPort ijtag_si    = sib_core0.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core1 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core0.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core1.ijtag_so;
   }
   Instance core2 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core2.ijtag_to_sel;
      InputPort ijtag_si    = sib_core1.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core2 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core1.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core2.ijtag_so;
   }
   Instance core3 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core3.ijtag_to_sel;
      InputPort ijtag_si    = sib_core2.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core3 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core2.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core3.ijtag_so;
   }
   Instance core4 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core4.ijtag_to_sel;
      InputPort ijtag_si    = sib_core3.ijtag_so;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core4 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = sib_core3.ijtag_so;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core4.ijtag_so;
   }
}
