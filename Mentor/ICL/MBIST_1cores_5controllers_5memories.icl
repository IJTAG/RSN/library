// Copyright 2018: Mentor, a Siemens Business
// Benchmark for a chip containing 1 cores
// Each core contains 5 memory BIST controllers
// Each memory BIST controller tests 5 memories



Module memory_1 {
   DataInPort AR[3:0];
   DataInPort AW[3:0];
   ClockPort CLKR;
   ClockPort CLKW;
}


Module mbist_interface {
   ClockPort BIST_CLK;
   DataInPort BIST_EN;
   DataInPort BIST_ASYNC_RESETN;
   ScanInPort BIST_SI;
   ScanOutPort BIST_SO {
      Source reg3_alias;
   }
   ShiftEnPort BIST_SHIFT_COLLAR;
   DataInPort BIST_CONTROL;
   DataOutPort AR[3:0];
   DataOutPort AW[3:0];

   Alias reg2_alias[0:3] = reg2[0:3];
   Alias reg3_alias = reg3[0];
   ScanRegister reg1[0:0] {
      ScanInSource BIST_SI;
   }
   ScanRegister reg2[0:3] {
      ScanInSource reg1[0];
   }
   ScanRegister reg3[7:0] {
      ScanInSource reg2_alias[3];
   }
}

Module sib_1 {
   ResetPort ijtag_reset {
      ActivePolarity 0;
   }
   SelectPort ijtag_sel;
   ScanInPort ijtag_si;
   CaptureEnPort ijtag_ce;
   ShiftEnPort ijtag_se;
   UpdateEnPort ijtag_ue;
   TCKPort ijtag_tck;
   ScanOutPort ijtag_so {
      Source sib;
   }
   ToSelectPort ijtag_to_sel;
   ScanInPort ijtag_from_so;
   ScanInterface client {
      Port ijtag_si;
      Port ijtag_so;
      Port ijtag_sel;
   }
   ScanInterface host {
      Port ijtag_from_so;
      Port ijtag_to_sel;
   }
   ScanRegister sib {
      ScanInSource scan_in_mux;
      CaptureSource 1'b0;
      ResetValue 1'b0;
   }
   ScanMux scan_in_mux SelectedBy sib {
      1'b0 : ijtag_si;
      1'b1 : ijtag_from_so;
   }
}
Module sib_2 {
   ResetPort ijtag_reset {
      ActivePolarity 0;
   }
   SelectPort ijtag_sel;
   ScanInPort ijtag_si;
   CaptureEnPort ijtag_ce;
   ShiftEnPort ijtag_se;
   UpdateEnPort ijtag_ue;
   TCKPort ijtag_tck;
   
   ToResetPort to_ijtag_reset {
      ActivePolarity 0;
      Source ijtag_reset;
   }
   ScanOutPort to_ijtag_si {
      Source ijtag_si;
   }
   ToCaptureEnPort to_ijtag_ce {
      Source ijtag_ce;
   }
   ToShiftEnPort to_ijtag_se {
      Source ijtag_se;
   }
   ToUpdateEnPort to_ijtag_ue {
      Source ijtag_ue;
   }
   ToTCKPort to_ijtag_tck;
   
   ScanOutPort ijtag_so {
      Source sib;
   }
   ToSelectPort ijtag_to_sel;
   ScanInPort ijtag_from_so;

   ScanInterface client {
      Port ijtag_si;
      Port ijtag_so;
      Port ijtag_sel;
   }
   ScanInterface host {
      Port ijtag_from_so;
      Port ijtag_to_sel;
      Port to_ijtag_si;
   }
   ScanRegister sib {
      ScanInSource scan_in_mux;
      CaptureSource 1'b0;
      ResetValue 1'b0;
   }
   ScanMux scan_in_mux SelectedBy sib {
      1'b0 : ijtag_si;
      1'b1 : ijtag_from_so;
   }
}
Module  mbist_bap {
   ResetPort reset {
      ActivePolarity 0;
   }
   SelectPort ijtag_select;
   ScanInPort si;
   CaptureEnPort capture_en;
   ShiftEnPort shift_en;
   ToShiftEnPort shift_en_R;
   UpdateEnPort update_en;
   TCKPort tck;

   DataOutPort setting1 {
      Source BIST_setting1_tdr;
      RefEnum OnOff;
   }
   DataOutPort setting2 {
      Source BIST_setting2_tdr;
      RefEnum OnOff;
   }
   DataOutPort setting3 {
      Source BIST_setting3_tdr;
      RefEnum OnOff;
   }

   ScanOutPort so {
      Source sib_0;
   }
   
   ScanOutPort toBist[4:0] {
      Source toBist_int;
   }
   ScanInPort fromBist[4:0];
   DataOutPort bistEn[4:0] {
      Source bistEn_int[4:0];
   }
   DataInPort MBISTPG_GO[4:0];
   DataInPort MBISTPG_DONE[4:0];

   DataOutPort TCK_MODE {
      Source TCK_MODE_tdr;
      RefEnum OnOff;
   }
   DataOutPort BIST_ASYNC_RESET {
      Source BIST_ASYNC_RESET_tdr;
      RefEnum OnOff;
   }
   DataOutPort BIST_SETUP[2:0] {
      Source BIST_SETUP_tdr[2:0];
   }
   ScanInterface client {
      Port si;
      Port so;
      Port ijtag_select;
   }
   
   ScanInterface host_0 {
      Port toBist[0];
      Port fromBist[0];
      Port shift_en_R;
   }
   ScanInterface host_1 {
      Port toBist[1];
      Port fromBist[1];
      Port shift_en_R;
   }
   ScanInterface host_2 {
      Port toBist[2];
      Port fromBist[2];
      Port shift_en_R;
   }
   ScanInterface host_3 {
      Port toBist[3];
      Port fromBist[3];
      Port shift_en_R;
   }
   ScanInterface host_4 {
      Port toBist[4];
      Port fromBist[4];
      Port shift_en_R;
   }
   Alias bistEn_int[4:0] =sib_4, sib_3, sib_2, sib_1, sib_0;
   Alias toBist_int[4:0] = BIST_setting3_tdr, sib_4, sib_3, sib_2, sib_1;
   Enum OnOff {
      ON = 1'b1;
      OFF = 1'b0;
   }
   ScanRegister tdr[0:0] {
      ScanInSource si;
      CaptureSource MBISTPG_DONE[4];
   }
   ScanRegister BIST_SETUP_tdr[2:0] {
      ScanInSource tdr[0];
      CaptureSource MBISTPG_DONE[3], MBISTPG_DONE[2], MBISTPG_DONE[1];
      DefaultLoadValue 3'b000;
      ResetValue 3'b000;
   }
   ScanRegister TCK_MODE_tdr {
      ScanInSource BIST_SETUP_tdr[0];
      CaptureSource MBISTPG_DONE[0];
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister CHAIN_BYPASS_EN_tdr {
      ScanInSource TCK_MODE_tdr;
      CaptureSource MBISTPG_GO[4];
      DefaultLoadValue 1'b1;
      ResetValue 1'b1;
   }
   ScanRegister BIST_ASYNC_RESET_tdr {
      ScanInSource CHAIN_BYPASS_EN_tdr;
      CaptureSource MBISTPG_GO[3];
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister BIST_setting1_tdr {
      ScanInSource BIST_ASYNC_RESET_tdr;
      CaptureSource MBISTPG_GO[2];
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister BIST_setting2_tdr {
      ScanInSource BIST_setting1_tdr;
      CaptureSource MBISTPG_GO[1];
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister BIST_setting3_tdr {
      ScanInSource BIST_setting2_tdr;
      CaptureSource MBISTPG_GO[0];
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_0 {
      ScanInSource fromBistMux_0;
      CaptureSource sib_0;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_1 {
      ScanInSource fromBistMux_1;
      CaptureSource sib_1;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_2 {
      ScanInSource fromBistMux_2;
      CaptureSource sib_2;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_3 {
      ScanInSource fromBistMux_3;
      CaptureSource sib_3;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanRegister sib_4 {
      ScanInSource fromBistMux_4;
      CaptureSource sib_4;
      DefaultLoadValue 1'b0;
      ResetValue 1'b0;
   }
   ScanMux fromBistMux_4 SelectedBy sib_4, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist[4];
      3'bxxx : BIST_setting3_tdr;
   }
   ScanMux fromBistMux_0 SelectedBy sib_0, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist[0];
      3'bxxx : sib_1;
   }
   ScanMux fromBistMux_1 SelectedBy sib_1, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist[1];
      3'bxxx : sib_2;
   }
   ScanMux fromBistMux_2 SelectedBy sib_2, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist[2];
      3'bxxx : sib_3;
   }
   ScanMux fromBistMux_3 SelectedBy sib_3, BIST_SETUP_tdr[1], CHAIN_BYPASS_EN_tdr
       {
      3'b100 : fromBist[3];
      3'bxxx : sib_4;
   }
}

Module mbist_controller {
   ClockPort BIST_CLK;
   DataInPort MBISTPG_EN;

   DataInPort MBISTPG_ASYNC_RESETN;

   DataInPort BIST_CONTROL[2:0];
   DataInPort TCK_MODE;
   DataInPort setting1 {
      RefEnum OnOff;
   }
   DataInPort setting2 {
      RefEnum OnOff;
   }
   DataInPort setting3 {
      RefEnum OnOff;
   }
   DataOutPort MBISTPG_GO {
      RefEnum PassFail;
   }
   DataOutPort MBISTPG_DONE {
      RefEnum PassFail;
   }
   TCKPort TCK;
   ScanInPort BIST_SI;
   ScanOutPort MBISTPG_SO {
      Source CONTROLLER_SETUP_CHAIN;
   }
   ShiftEnPort BIST_SHIFT;
   ToShiftEnPort BIST_SHIFT_COLLAR;
   ScanOutPort MEM0_BIST_COLLAR_SI {
      Source MEM0_BIST_COLLAR_SI_INT;
   }
   ScanInPort MEM0_BIST_COLLAR_SO;
   ScanOutPort MEM1_BIST_COLLAR_SI {
      Source MEM1_BIST_COLLAR_SI_INT;
   }
   ScanInPort MEM1_BIST_COLLAR_SO;
   ScanOutPort MEM2_BIST_COLLAR_SI {
      Source MEM2_BIST_COLLAR_SI_INT;
   }
   ScanInPort MEM2_BIST_COLLAR_SO;
   ScanOutPort MEM3_BIST_COLLAR_SI {
      Source MEM3_BIST_COLLAR_SI_INT;
   }
   ScanInPort MEM3_BIST_COLLAR_SO;
   ScanOutPort MEM4_BIST_COLLAR_SI {
      Source MEM4_BIST_COLLAR_SI_INT;
   }
   ScanInPort MEM4_BIST_COLLAR_SO;

   ScanInterface Client {
      Port BIST_SI;
      Port MBISTPG_SO;
      Port BIST_SHIFT;
   }

   ScanInterface MEM0_INTERFACE {
      Port BIST_SHIFT_COLLAR;
      Port MEM0_BIST_COLLAR_SI;
      Port MEM0_BIST_COLLAR_SO;
   }
   ScanInterface MEM1_INTERFACE {
      Port BIST_SHIFT_COLLAR;
      Port MEM1_BIST_COLLAR_SI;
      Port MEM1_BIST_COLLAR_SO;
   }
   ScanInterface MEM2_INTERFACE {
      Port BIST_SHIFT_COLLAR;
      Port MEM2_BIST_COLLAR_SI;
      Port MEM2_BIST_COLLAR_SO;
   }
   ScanInterface MEM3_INTERFACE {
      Port BIST_SHIFT_COLLAR;
      Port MEM3_BIST_COLLAR_SI;
      Port MEM3_BIST_COLLAR_SO;
   }
   ScanInterface MEM4_INTERFACE {
      Port BIST_SHIFT_COLLAR;
      Port MEM4_BIST_COLLAR_SI;
      Port MEM4_BIST_COLLAR_SO;
   }

   Alias MEM0_BIST_COLLAR_SI_INT = MEM1_BIST_COLLAR_SO;
   Alias MEM1_BIST_COLLAR_SI_INT = MEM2_BIST_COLLAR_SO;
   Alias MEM2_BIST_COLLAR_SI_INT = MEM3_BIST_COLLAR_SO;
   Alias MEM3_BIST_COLLAR_SI_INT = MEM4_BIST_COLLAR_SO;
   Alias MEM4_BIST_COLLAR_SI_INT = BIST_TO_COLLAR_SO_MUX;



   Enum PassFail {
      Pass = 1'b1;
      Fail = 1'b0;
      Ignore = 1'bx;
   }
   Enum OnOff {
      ON  = 1'b1;
      OFF = 1'b0;
   }

   ScanRegister MBIST_SETTINGS1[9:0] {
      ScanInSource BIST_SI;
   }
   ScanRegister MBIST_SETTINGS2[9:0] {
      ScanInSource MBIST_SETTINGS1[0];
   }
   ScanRegister MBIST_SETTINGS3[9:0] {
      ScanInSource MBIST_SETTINGS2[0];
   }

   ScanRegister MBIST_RESULT[9:0] {
      ScanInSource MEM0_BIST_COLLAR_SO;
   }

   ScanMux BIST_TO_COLLAR_SO_MUX SelectedBy mode1, mode2 {
      2'b01 : MBIST_SETTINGS2[0];
      2'b10 : MBIST_SETTINGS3[0];
   }
   ScanMux CONTROLLER_SETUP_CHAIN SelectedBy MBISTPG_EN, BIST_CONTROL {
      1'b1, 3'b00x : MBIST_RESULT[0];
   }
   LogicSignal mode1 {
      MBISTPG_EN, BIST_CONTROL == 1'b1, 3'b001;
   }
   LogicSignal mode2 {
      MBISTPG_EN, BIST_CONTROL == 1'b1, 3'b000;
   }
}


Module core {
   ClockPort clk;
   CaptureEnPort ijtag_ce;
   ResetPort ijtag_reset {
      ActivePolarity 0;
   }
   ShiftEnPort ijtag_se;
   SelectPort ijtag_sel;
   ScanInPort ijtag_si;
   ScanOutPort ijtag_so {
      Source sib_inst.ijtag_so;
   }
   TCKPort ijtag_tck;
   UpdateEnPort ijtag_ue;
   ScanInterface ijtag {
      Port ijtag_ce;
      Port ijtag_reset;
      Port ijtag_se;
      Port ijtag_sel;
      Port ijtag_si;
      Port ijtag_so;
      Port ijtag_tck;
      Port ijtag_ue;
   }
   

   // Memory block 0
   Instance memory_block_0_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_0_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_0_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_0_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_0_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_0_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_0_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_0_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_0_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_0_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[0];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_0.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_0.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_0_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_0_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_0_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_0_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_0_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_0_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_0_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_0_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_0_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_0_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[0];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_0.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_0.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_0_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_0_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_0_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_0_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_0_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_0_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_0_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_0_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_0_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_0_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[0];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_0.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_0.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_0_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_0_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_0_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_0_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_0_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_0_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_0_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_0_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_0_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_0_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[0];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_0.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_0.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_0_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_0_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_0_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_0_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_0_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_0_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_0_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_0_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_0_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_0_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[0];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_0.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_0.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_0 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn[0];
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist[0];
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_0_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_0_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_0_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_0_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_0_inst_4.BIST_SO;
   }

   // Memory block 1
   Instance memory_block_1_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_1_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_1_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_1_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_1_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_1_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_1_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_1_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_1_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_1_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[1];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_1.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_1.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_1_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_1_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_1_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_1_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_1_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_1_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_1_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_1_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_1_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_1_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[1];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_1.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_1.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_1_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_1_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_1_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_1_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_1_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_1_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_1_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_1_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_1_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_1_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[1];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_1.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_1.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_1_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_1_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_1_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_1_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_1_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_1_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_1_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_1_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_1_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_1_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[1];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_1.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_1.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_1_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_1_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_1_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_1_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_1_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_1_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_1_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_1_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_1_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_1_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[1];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_1.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_1.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_1 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn[1];
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist[1];
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_1_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_1_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_1_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_1_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_1_inst_4.BIST_SO;
   }

   // Memory block 2
   Instance memory_block_2_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_2_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_2_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_2_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_2_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_2_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_2_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_2_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_2_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_2_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[2];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_2.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_2.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_2_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_2_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_2_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_2_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_2_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_2_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_2_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_2_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_2_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_2_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[2];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_2.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_2.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_2_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_2_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_2_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_2_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_2_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_2_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_2_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_2_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_2_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_2_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[2];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_2.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_2.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_2_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_2_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_2_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_2_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_2_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_2_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_2_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_2_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_2_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_2_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[2];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_2.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_2.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_2_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_2_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_2_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_2_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_2_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_2_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_2_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_2_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_2_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_2_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[2];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_2.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_2.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_2 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn[2];
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist[2];
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_2_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_2_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_2_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_2_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_2_inst_4.BIST_SO;
   }

   // Memory block 3
   Instance memory_block_3_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_3_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_3_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_3_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_3_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_3_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_3_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_3_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_3_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_3_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[3];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_3.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_3.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_3_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_3_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_3_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_3_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_3_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_3_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_3_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_3_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_3_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_3_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[3];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_3.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_3.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_3_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_3_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_3_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_3_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_3_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_3_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_3_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_3_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_3_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_3_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[3];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_3.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_3.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_3_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_3_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_3_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_3_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_3_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_3_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_3_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_3_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_3_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_3_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[3];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_3.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_3.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_3_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_3_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_3_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_3_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_3_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_3_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_3_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_3_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_3_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_3_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[3];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_3.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_3.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_3 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn[3];
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist[3];
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_3_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_3_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_3_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_3_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_3_inst_4.BIST_SO;
   }

   // Memory block 4
   Instance memory_block_4_inst_0 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_4_inst_0.AR[3];
      InputPort AR[2] = mbist_interface_block_4_inst_0.AR[2];
      InputPort AR[1] = mbist_interface_block_4_inst_0.AR[1];
      InputPort AR[0] = mbist_interface_block_4_inst_0.AR[0];
      InputPort AW[3] = mbist_interface_block_4_inst_0.AW[3];
      InputPort AW[2] = mbist_interface_block_4_inst_0.AW[2];
      InputPort AW[1] = mbist_interface_block_4_inst_0.AW[1];
      InputPort AW[0] = mbist_interface_block_4_inst_0.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_4_inst_0 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[4];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_4.MEM0_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_4.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_4_inst_1 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_4_inst_1.AR[3];
      InputPort AR[2] = mbist_interface_block_4_inst_1.AR[2];
      InputPort AR[1] = mbist_interface_block_4_inst_1.AR[1];
      InputPort AR[0] = mbist_interface_block_4_inst_1.AR[0];
      InputPort AW[3] = mbist_interface_block_4_inst_1.AW[3];
      InputPort AW[2] = mbist_interface_block_4_inst_1.AW[2];
      InputPort AW[1] = mbist_interface_block_4_inst_1.AW[1];
      InputPort AW[0] = mbist_interface_block_4_inst_1.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_4_inst_1 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[4];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_4.MEM1_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_4.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_4_inst_2 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_4_inst_2.AR[3];
      InputPort AR[2] = mbist_interface_block_4_inst_2.AR[2];
      InputPort AR[1] = mbist_interface_block_4_inst_2.AR[1];
      InputPort AR[0] = mbist_interface_block_4_inst_2.AR[0];
      InputPort AW[3] = mbist_interface_block_4_inst_2.AW[3];
      InputPort AW[2] = mbist_interface_block_4_inst_2.AW[2];
      InputPort AW[1] = mbist_interface_block_4_inst_2.AW[1];
      InputPort AW[0] = mbist_interface_block_4_inst_2.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_4_inst_2 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[4];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_4.MEM2_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_4.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_4_inst_3 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_4_inst_3.AR[3];
      InputPort AR[2] = mbist_interface_block_4_inst_3.AR[2];
      InputPort AR[1] = mbist_interface_block_4_inst_3.AR[1];
      InputPort AR[0] = mbist_interface_block_4_inst_3.AR[0];
      InputPort AW[3] = mbist_interface_block_4_inst_3.AW[3];
      InputPort AW[2] = mbist_interface_block_4_inst_3.AW[2];
      InputPort AW[1] = mbist_interface_block_4_inst_3.AW[1];
      InputPort AW[0] = mbist_interface_block_4_inst_3.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_4_inst_3 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[4];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_4.MEM3_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_4.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance memory_block_4_inst_4 Of memory_1 {
      InputPort AR[3] = mbist_interface_block_4_inst_4.AR[3];
      InputPort AR[2] = mbist_interface_block_4_inst_4.AR[2];
      InputPort AR[1] = mbist_interface_block_4_inst_4.AR[1];
      InputPort AR[0] = mbist_interface_block_4_inst_4.AR[0];
      InputPort AW[3] = mbist_interface_block_4_inst_4.AW[3];
      InputPort AW[2] = mbist_interface_block_4_inst_4.AW[2];
      InputPort AW[1] = mbist_interface_block_4_inst_4.AW[1];
      InputPort AW[0] = mbist_interface_block_4_inst_4.AW[0];
      InputPort CLKR = clk;
      InputPort CLKW = clk;
   }
   Instance mbist_interface_block_4_inst_4 Of mbist_interface {
      InputPort BIST_CLK          = clk;
      InputPort BIST_EN           = mbist_bap_inst.bistEn[4];
      InputPort BIST_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_SI           = mbist_controller_block_4.MEM4_BIST_COLLAR_SI;
      InputPort BIST_SHIFT_COLLAR = mbist_controller_block_4.BIST_SHIFT_COLLAR;
      InputPort BIST_CONTROL      = mbist_bap_inst.BIST_SETUP[2];
   }
   Instance mbist_controller_block_4 Of mbist_controller {
      InputPort BIST_CLK             = clk;
      InputPort MBISTPG_EN           = mbist_bap_inst.bistEn[4];
      InputPort MBISTPG_ASYNC_RESETN = mbist_bap_inst.BIST_ASYNC_RESET;
      InputPort BIST_CONTROL[2:0]    = mbist_bap_inst.BIST_SETUP[2:0];
      InputPort TCK_MODE             = mbist_bap_inst.TCK_MODE;
      InputPort setting1             = mbist_bap_inst.setting1;
      InputPort setting2             = mbist_bap_inst.setting2;
      InputPort setting3             = mbist_bap_inst.setting3;
      InputPort TCK                  = sib_inst.to_ijtag_tck;
      InputPort BIST_SI              = mbist_bap_inst.toBist[4];
      InputPort BIST_SHIFT           = mbist_bap_inst.shift_en_R;
      InputPort MEM0_BIST_COLLAR_SO  = mbist_interface_block_4_inst_0.BIST_SO;
      InputPort MEM1_BIST_COLLAR_SO  = mbist_interface_block_4_inst_1.BIST_SO;
      InputPort MEM2_BIST_COLLAR_SO  = mbist_interface_block_4_inst_2.BIST_SO;
      InputPort MEM3_BIST_COLLAR_SO  = mbist_interface_block_4_inst_3.BIST_SO;
      InputPort MEM4_BIST_COLLAR_SO  = mbist_interface_block_4_inst_4.BIST_SO;
   }



// One MBIST BAP for the core
   Instance mbist_bap_inst Of mbist_bap {
      InputPort reset              = sib_inst.to_ijtag_reset;
      InputPort ijtag_select       = sib_mbist_inst.ijtag_to_sel;
      InputPort si                 = sib_inst.to_ijtag_si;
      InputPort capture_en         = sib_inst.to_ijtag_ce;
      InputPort shift_en           = sib_inst.to_ijtag_se;
      InputPort update_en          = sib_inst.to_ijtag_ue;
      InputPort tck                = sib_inst.to_ijtag_tck;
      InputPort fromBist[0] = mbist_controller_block_0.MBISTPG_SO;
      InputPort fromBist[1] = mbist_controller_block_1.MBISTPG_SO;
      InputPort fromBist[2] = mbist_controller_block_2.MBISTPG_SO;
      InputPort fromBist[3] = mbist_controller_block_3.MBISTPG_SO;
      InputPort fromBist[4] = mbist_controller_block_4.MBISTPG_SO;
      InputPort MBISTPG_GO[0] = mbist_controller_block_0.MBISTPG_GO;
      InputPort MBISTPG_GO[1] = mbist_controller_block_1.MBISTPG_GO;
      InputPort MBISTPG_GO[2] = mbist_controller_block_2.MBISTPG_GO;
      InputPort MBISTPG_GO[3] = mbist_controller_block_3.MBISTPG_GO;
      InputPort MBISTPG_GO[4] = mbist_controller_block_4.MBISTPG_GO;
      InputPort MBISTPG_DONE[0] = mbist_controller_block_0.MBISTPG_DONE;
      InputPort MBISTPG_DONE[1] = mbist_controller_block_1.MBISTPG_DONE;
      InputPort MBISTPG_DONE[2] = mbist_controller_block_2.MBISTPG_DONE;
      InputPort MBISTPG_DONE[3] = mbist_controller_block_3.MBISTPG_DONE;
      InputPort MBISTPG_DONE[4] = mbist_controller_block_4.MBISTPG_DONE;
   }
   Instance sib_mbist_inst Of sib_1 {
      InputPort ijtag_reset = sib_inst.to_ijtag_reset;
      InputPort ijtag_sel = sib_inst.ijtag_to_sel;
      InputPort ijtag_si = sib_inst.to_ijtag_si;
      InputPort ijtag_ce = sib_inst.to_ijtag_ce;
      InputPort ijtag_se = sib_inst.to_ijtag_se;
      InputPort ijtag_ue = sib_inst.to_ijtag_ue;
      InputPort ijtag_tck = sib_inst.to_ijtag_tck;
      InputPort ijtag_from_so = mbist_bap_inst.so;
      }
   Instance sib_inst Of sib_2 {
      InputPort ijtag_reset = ijtag_reset;
      InputPort ijtag_sel = ijtag_sel;
      InputPort ijtag_si = ijtag_si;
      InputPort ijtag_ce = ijtag_ce;
      InputPort ijtag_se = ijtag_se;
      InputPort ijtag_ue = ijtag_ue;
      InputPort ijtag_tck = ijtag_tck;
      InputPort ijtag_from_so = sib_mbist_inst.ijtag_so;
   }
}



Module chip_tap_fsm {
   TCKPort tck;
   TMSPort tms;
   TRSTPort trst;
   ToIRSelectPort irSel;
   ToResetPort tlr;
}

Module chip_tap {
   TCKPort tck;
   ScanInPort tdi;
   ScanOutPort tdo {
      Source IRMux;
   }
   TMSPort tms;
   TRSTPort trst;
   ToCaptureEnPort capture_dr_en;
   ToShiftEnPort shift_dr_en;
   ToUpdateEnPort update_dr_en;
   ToResetPort test_logic_reset {
      ActivePolarity 0;
   }
   ToSelectPort host_1_to_sel {
      Source host_1_to_sel_int;
   }
   ScanInPort host_1_from_so;
   DataOutPort force_disable {
      Source force_disable_int;
   }
   DataOutPort select_jtag_input {
      Source select_jtag_input_int;
   }
   DataOutPort select_jtag_output {
      Source select_jtag_output_int;
   }
   DataOutPort extest_pulse {
      Source ext_test_pulse_int;
   }
   DataOutPort extest_train {
      Source ext_test_train_int;
   }
   DataOutPort fsm_state[3:0] {
      RefEnum state_encoding;
   }
   ScanInterface tap_client {
      Port tdi;
      Port tdo;
      Port tms;
   }
   ScanInterface host_ijtag_1 {
      Port host_1_from_so;
      Port host_1_to_sel;
   }
   Enum state_encoding {
      test_logic_reset = 4'b1111;
      run_test_idle = 4'b1100;
      select_dr = 4'b0111;
      capture_dr = 4'b0110;
      shift_dr = 4'b0010;
      exit1_dr = 4'b0001;
      pause_dr = 4'b0011;
      exit2_dr = 4'b0000;
      update_dr = 4'b0101;
      select_ir = 4'b0100;
      capture_ir = 4'b1110;
      shift_ir = 4'b1010;
      exit1_ir = 4'b1001;
      pause_ir = 4'b1011;
      exit2_ir = 4'b1000;
      update_ir = 4'b1101;
   }
   Enum instruction_opcodes {
      BYPASS = 4'b1111;
      CLAMP = 4'b0000;
      EXTEST = 4'b0001;
      EXTEST_PULSE = 4'b0010;
      EXTEST_TRAIN = 4'b0011;
      INTEST = 4'b0100;
      SAMPLE = 4'b0101;
      PRELOAD = 4'b0101;
      HIGHZ = 4'b0110;
      HOSTIJTAG_1 = 4'b0111;
   }
   ScanRegister instruction[3:0] {
      ScanInSource tdi;
      CaptureSource 4'b0001;
      ResetValue 4'b1111;
      RefEnum instruction_opcodes;
   }
   ScanRegister bypass {
      ScanInSource tdi;
      CaptureSource 1'b0;
   }
   ScanMux IRMux SelectedBy fsm.irSel {
      1'b0 : DRMux;
      1'b1 : instruction[0];
   }
   ScanMux DRMux SelectedBy instruction {
      4'b1111 : bypass;
      4'b0000 : bypass;
      4'b0001 : bypass;
      4'b0010 : bypass;
      4'b0011 : bypass;
      4'b0100 : bypass;
      4'b0101 : bypass;
      4'b0110 : bypass;
      4'b0111 : host_1_from_so;
   }
   LogicSignal host_1_to_sel_int {
      instruction == HOSTIJTAG_1;
   }
   LogicSignal force_disable_int {
      instruction == HIGHZ;
   }
   LogicSignal select_jtag_input_int {
      instruction == INTEST;
   }
   LogicSignal select_jtag_output_int {
      ((((instruction == EXTEST) || (instruction == EXTEST_PULSE)) ||
          (instruction == EXTEST_TRAIN)) || (instruction == CLAMP)) ||
          (instruction == HIGHZ);
   }
   LogicSignal ext_test_pulse_int {
      instruction == EXTEST_PULSE;
   }
   LogicSignal ext_test_train_int {
      instruction == EXTEST_TRAIN;
   }
   Instance fsm Of chip_tap_fsm {
      InputPort tck = tck;
      InputPort tms = tms;
      InputPort trst = trst;
   }
}


Module chip {
   ClockPort clk;
   TCKPort tck;
   ScanInPort tdi;
   ScanOutPort tdo {
      Source chip_tap_inst.tdo;
   }
   TMSPort tms;
   TRSTPort trst;
   ScanInterface tap {
      Port tck;
      Port tdi;
      Port tdo;
      Port tms;
      Port trst;
   }


   Instance chip_tap_inst Of chip_tap {
      InputPort tck    = tck;
      InputPort tdi    = tdi;
      InputPort tms    = tms;
      InputPort trst   = trst;
      InputPort host_1_from_so = sib_core0.ijtag_so;
   }
   Instance core0 Of core {
      InputPort clk         = clk;
      InputPort ijtag_ce    = chip_tap_inst.capture_dr_en;
      InputPort ijtag_reset = chip_tap_inst.test_logic_reset;
      InputPort ijtag_se    = chip_tap_inst.shift_dr_en;
      InputPort ijtag_sel   = sib_core0.ijtag_to_sel;
      InputPort ijtag_si    = tdi;
      InputPort ijtag_tck   = tck;
      InputPort ijtag_ue    = chip_tap_inst.update_dr_en;
   }
   Instance sib_core0 Of sib_1 {
      InputPort ijtag_reset   = chip_tap_inst.test_logic_reset;
      InputPort ijtag_sel     = chip_tap_inst.host_1_to_sel;
      InputPort ijtag_si      = tdi;
      InputPort ijtag_ce      = chip_tap_inst.capture_dr_en;
      InputPort ijtag_se      = chip_tap_inst.shift_dr_en;
      InputPort ijtag_ue      = chip_tap_inst.update_dr_en;
      InputPort ijtag_tck     = tck;
      InputPort ijtag_from_so = core0.ijtag_so;
   }
}
