# Copyright 2018: Mentor, a Siemens Business
# Benchmark for a chip containing 5 cores
# Each core contains 20 memory BIST controllers
# Each memory BIST controller tests 5 memories
iProcsForModule mbist_controller

   iProc setupController_int {
   } {
      iWrite MBISTPG_ASYNC_RESETN 1
      iApply
      
      iWrite BIST_CONTROL[0] 0
      iWrite TCK_MODE 1
      iWrite setting1 1
      iWrite setting2 0
      iWrite setting3 1
      iApply

      iWrite MBIST_SETTINGS1[9] 1
      iWrite MBIST_SETTINGS1[5] 0
      iWrite MBIST_SETTINGS2[3] 1
      iWrite MBIST_SETTINGS3[9:0] 0b0111010001
      iApply
   }
  
   iProc runTest_int {
   } {
      iTake MBISTPG_EN
      
      iWrite MBISTPG_EN 1
      iApply
      
      iRunLoop 1 -tck

      iRead MBISTPG_GO Fail
      iRead MBISTPG_DONE Fail    
      iWrite BIST_CONTROL 0b000
      iApply

      # wait for MBIST to finish
      iRunLoop 10000 -tck

      iRead MBISTPG_GO Pass
      iRead MBISTPG_DONE Pass    
      iWrite BIST_CONTROL[1] 0   
      iApply
      
      iRelease MBISTPG_EN
   }




iProcsForModule core


   iProc compareResult_int {
        {blockNo ""}
   } {
      iWrite mbist_controller_block_${blockNo}.MBIST_SETTINGS1[5] 1
      iWrite mbist_controller_block_${blockNo}.BIST_CONTROL 0b001
      iApply
      iRead mbist_interface_block_${blockNo}_inst_0.reg2[2] 0b0
      iRead mbist_interface_block_${blockNo}_inst_1.reg2[2] 0b0
      iRead mbist_interface_block_${blockNo}_inst_2.reg2[2] 0b0
      iRead mbist_interface_block_${blockNo}_inst_3.reg2[2] 0b0
      iRead mbist_interface_block_${blockNo}_inst_4.reg2[2] 0b0
      iApply
      iWrite mbist_controller_block_${blockNo}.MBISTPG_EN 0

      iApply
      iRunLoop 1 -tck
   }



   iProc perform_MBIST_onBlock {
        {blockNo ""}
   } {
      iTake mbist_controller_block_${blockNo}.BIST_CONTROL[1]
      iCall mbist_controller_block_${blockNo}.setupController_int;
      iCall mbist_controller_block_${blockNo}.runTest_int;
      iCall compareResult_int ${blockNo};
      iApply
      iRelease mbist_controller_block_${blockNo}.BIST_CONTROL[1]
   }

   iProc test_seq {
   } { 
      iCall perform_MBIST_onBlock 0;
      iApply
      iCall perform_MBIST_onBlock 1;
      iApply
      iCall perform_MBIST_onBlock 2;
      iApply
      iCall perform_MBIST_onBlock 3;
      iApply
      iCall perform_MBIST_onBlock 4;
      iApply
      iCall perform_MBIST_onBlock 5;
      iApply
      iCall perform_MBIST_onBlock 6;
      iApply
      iCall perform_MBIST_onBlock 7;
      iApply
      iCall perform_MBIST_onBlock 8;
      iApply
      iCall perform_MBIST_onBlock 9;
      iApply
      iCall perform_MBIST_onBlock 10;
      iApply
      iCall perform_MBIST_onBlock 11;
      iApply
      iCall perform_MBIST_onBlock 12;
      iApply
      iCall perform_MBIST_onBlock 13;
      iApply
      iCall perform_MBIST_onBlock 14;
      iApply
      iCall perform_MBIST_onBlock 15;
      iApply
      iCall perform_MBIST_onBlock 16;
      iApply
      iCall perform_MBIST_onBlock 17;
      iApply
      iCall perform_MBIST_onBlock 18;
      iApply
      iCall perform_MBIST_onBlock 19;
      iApply
   }

   iProc test_parallel {
   } {
      iMerge -begin
   
      iCall perform_MBIST_onBlock 0;
      iCall perform_MBIST_onBlock 1;
      iCall perform_MBIST_onBlock 2;
      iCall perform_MBIST_onBlock 3;
      iCall perform_MBIST_onBlock 4;
      iCall perform_MBIST_onBlock 5;
      iCall perform_MBIST_onBlock 6;
      iCall perform_MBIST_onBlock 7;
      iCall perform_MBIST_onBlock 8;
      iCall perform_MBIST_onBlock 9;
      iCall perform_MBIST_onBlock 10;
      iCall perform_MBIST_onBlock 11;
      iCall perform_MBIST_onBlock 12;
      iCall perform_MBIST_onBlock 13;
      iCall perform_MBIST_onBlock 14;
      iCall perform_MBIST_onBlock 15;
      iCall perform_MBIST_onBlock 16;
      iCall perform_MBIST_onBlock 17;
      iCall perform_MBIST_onBlock 18;
      iCall perform_MBIST_onBlock 19;
      iMerge -end
   }




iProcsForModule chip

   iProc chip_seq {
   } { 
      iCall core0.test_seq;
      iApply
      iCall core1.test_seq;
      iApply
      iCall core2.test_seq;
      iApply
      iCall core3.test_seq;
      iApply
      iCall core4.test_seq;
      iApply
   }

   iProc chip_parallel {
   } {
      iMerge -begin
      iCall core0.test_parallel;
      iCall core1.test_parallel;
      iCall core2.test_parallel;
      iCall core3.test_parallel;
      iCall core4.test_parallel;
      iMerge -end
   }
