#!/bin/bash
homedir="."
currentdir="unset"
echo "home dir $homedir"
# Script to parse, link and compile ICL files 

# collect sub directories
find_subdirs () {
    while IFS= read -d $'\0' -r file ; do
        v_subDirs=("${v_subDirs[@]}" "$file")
    done < <(find "$1" -maxdepth 1 ! -path "$1" -type d -print0)  
}
#collect ICL files
find_iclfiles () {
    while IFS= read -d $'\0' -r file ; do
        v_iclFiles=("${v_iclFiles[@]}" "$file")
    done < <(find "$1" -maxdepth 1 -type f -name '*.icl' -print0)
}

elaborate_icls () {
    declare -a iclsArr=("${!2}")
    local lv_icls=()
    local lv_dirs=()
    v_iclFiles=()
    v_subDirs=()
    find_iclfiles "$1"
    lv_icls=("${v_iclFiles[@]}")
    lv_icls=("${lv_icls[@]}" "${iclsArr[@]}")
    v_subDirs=()
    find_subdirs "$1"
    lv_dirs=("${v_subDirs[@]}")
    if ((${#lv_dirs[@]} < 1)); then
        topmodule=""
        if [ -f "$1/top.module" ]; then
            topmodule="$(< $1/top.module)"
            echo "Processing files:"
            printf "%s\n" "${lv_icls[@]}"
            java -jar bin/ICL-tools-CLI-1.09.18.jar -c "${lv_icls[@]}" -b -t $topmodule -s "$1"/stat.txt  -l "$1"/debugOut >> "$1"/result.txt
            echo "End..."
        else
            if ((${#v_iclFiles[@]} > 0)); then
               echo "Processing files:"
               printf "%s\n" "${lv_icls[@]}"
               java -jar bin/ICL-tools-CLI-1.09.18.jar -c "${lv_icls[@]}" -l "$1"/clilog >> "$1"/result.txt
               echo "End..."
            fi
        fi 
    else 
        for d in ${lv_dirs[@]} ; do
            elaborate_icls "$d" lv_icls[@]
        done
    fi
}

v_iclFiles=()
v_subDirs=()
dirs=()
icls=()
find_subdirs "$homedir"
dirs=("${v_subDirs[@]}")
find_iclfiles "$homedir"
icls=("${v_iclFiles[@]}")
for d in ${dirs[@]} ; do
    elaborate_icls "$d" icls[@]
done
