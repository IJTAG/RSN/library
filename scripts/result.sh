#!/bin/bash
homedir="."
currentdir="unset"
failsNo=0
echo "base directory: $homedir"
red="\e[91m"
green="\e[92m"
reset="\e[0m"
# collect sub directories
find_subdirs () {
    while IFS= read -d $'\0' -r file ; do
        v_subDirs=("${v_subDirs[@]}" "$file")
    done < <(find "$1" -maxdepth 1 ! -path "$1" -type d -print0)  
}
find_outfiles () {
    while IFS= read -d $'\0' -r file ; do
        v_outFiles=("${v_outFiles[@]}" "$file")
    done < <(find "$1" -maxdepth 1 -type f -name 'result.txt' -print0)
}

elaborate_outs () {
    declare -a outsArr=("${!2}")
    local lv_outs=()
    local lv_dirs=()
    v_outFiles=()
    v_subDirs=()
    find_outfiles "$1"
    lv_outs=("${v_outFiles[@]}")
    v_subDirs=()
    find_subdirs "$1"
    lv_dirs=("${v_subDirs[@]}")
    if ((${#lv_dirs[@]} < 1)); then
        if ((${#v_outFiles[@]} > 0)); then
            #printf "%s\n" "${lv_outs[@]}"
            if (! grep -r -q 'SUCCESS' "${lv_outs[@]}") ; then
                echo -e "${red}FAILURE${reset} in "${lv_outs[@]}""
                failsNo=$((failsNo + 1))
            else 
                echo -e "${green}SUCCESS${reset} in "${lv_outs[@]}""
            fi
        fi
    else 
        for d in ${lv_dirs[@]} ; do
            elaborate_outs "$d" lv_outs[@]
        done
    fi
}

v_outFiles=()
v_subDirs=()
dirs=()
outs=()

find_subdirs "$homedir"
dirs=("${v_subDirs[@]}")

find_outfiles "$homedir"
outs=("${v_outFiles[@]}")

for d in ${dirs[@]} ; do
    elaborate_outs "$d" outs[@]
done

if (( $failsNo > 1 )); then
    echo "+---------------------------------------------------------------------+"
    echo "|  SUMMARY: $failsNo networks contain problems causing compilation FAILURES! |"
    echo "+---------------------------------------------------------------------+"
    exit 1
else 
    if (( $failsNo > 0 )); then
        echo "+---------------------------------------------------------------------+"
    echo "|  SUMMARY: $failsNo network contains problems causing compilation FAILURES! |"
    echo "+---------------------------------------------------------------------+"
        exit 1
    fi 
fi
